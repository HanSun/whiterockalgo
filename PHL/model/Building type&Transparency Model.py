import os
import glob
from tensorflow import keras
import tensorflow as tf
from PIL import Image
import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import pyplot as plt

class TypeTransModel:
    def __init__(self):

        # The directories of three different data set
        self.brick_1 = '/content/drive/My Drive/color/google_image/Merged Data&Model/Brick/'
        self.glass_1 = '/content/drive/My Drive/color/google_image/Merged Data&Model/Glass/'
        self.lime_1 = '/content/drive/My Drive/color/google_image/Merged Data&Model/Lime/'
        self.brick_2 = '/content/drive/My Drive/color/Pictures - Jordan/Test/Brick/'
        self.glass_2 = '/content/drive/My Drive/color/Pictures - Jordan/Test/Glass/'
        self.lime_2 = '/content/drive/My Drive/color/Pictures - Jordan/Test/Lime/'
        self.brick_3 = '/content/drive/My Drive/color/Pictures - Jordan.v2/brick building/'
        self.glass_3 = '/content/drive/My Drive/color/Pictures - Jordan.v2/glass building/'
        self.lime_3 = '/content/drive/My Drive/color/Pictures - Jordan.v2/limestone building/'

        # Global variables for training
        self.data_dir = ['train.tfrecord']
        self.save_path = 'Model_July_2.h5'
        self.LENGTH = 160
        self.WIDTH = 160
        self.CHANNELS = 3
        self.CLASS = 3
        self.BATCH_SIZE = 50
        self.EPOCHS = 20

    def _int64_feature(self, value):
        return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

    def _bytes_feature(self, value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    def write_record(self, images:list, labels:list, shape:tuple, file_name:str):
        assert len(images) == len(labels)
        writer = tf.python_io.TFRecordWriter(file_name)
        for i, (image, label) in enumerate(zip(images, labels),1):
            img = Image.open(image)
            img = img.convert("RGB")
            img = np.array(img.resize(shape))
            img = img/255
            feature = {'label': self._int64_feature(label),  'image': self._bytes_feature(img.tostring())}
            example = tf.train.Example(features=tf.train.Features(feature=feature))
            writer.write(example.SerializeToString())
            if i % 100 == 0:
                print("Record {} images of all".format(i))
        writer.close()
        print("Record {} images in total.".format(i))

    def get_files_list(self, *data_dir:tuple):
        # input order: (brick_1, glass_1, limestone_1), (brick_2, glass_2, limestone_2), ...
        features = []
        labels = []
        order = 0
        for material in zip(*data_dir):
            for item in material:
                data = glob.glob(item + '*.jpg')
                if not len(data) == 0:
                    features += data
                    labels += [order]*len(data)
                else:
                    raise ValueError('Empty folder!')
            order += 1
        assert order == 3
        assert len(features) == len(labels)
        return features, labels

    def parse_tfrecord(self, raw_record, img_shape:list):
        features_dict = {'image': tf.FixedLenFeature([], tf.string),
                         'label': tf.FixedLenFeature([], tf.int64)}
        parsed = tf.parse_single_example(raw_record, features_dict)
        image_raw = parsed['image']
        image = tf.decode_raw(image_raw, tf.float64)
        image = tf.reshape(image, img_shape)
        label = tf.cast(parsed['label'], tf.int32)
        label = tf.one_hot(label,self.CLASS)
        return image, label

    def input_fn(self, filenames:list, img_shape:list, buffer_size=1062, batch_size=50):
        dataset = tf.data.TFRecordDataset(filenames)
        dataset = dataset.map(lambda x: self.parse_tfrecord(x, img_shape))
        dataset = dataset.shuffle(buffer_size)
        dataset = dataset.batch(batch_size)
        dataset = dataset.repeat()
        iterator = dataset.make_one_shot_iterator()
        return iterator

    def my_model(self):
        # The architecture of my convolutional neural network
        model = keras.models.Sequential()
        model.add(keras.layers.Conv2D(16, (5,5), input_shape=(self.LENGTH, self.WIDTH, self.CHANNELS), activation='relu', padding='same'))
        model.add(keras.layers.Conv2D(16, (5,5), activation='relu', padding='same'))
        model.add(keras.layers.MaxPooling2D(pool_size=(2,2)))
        model.add(keras.layers.Conv2D(32, (5,5), activation='relu', padding='same'))
        model.add(keras.layers.Conv2D(32, (5,5), activation='relu', padding='same'))
        model.add(keras.layers.MaxPooling2D(pool_size=(2,2)))
        model.add(keras.layers.Conv2D(64, (5,5), activation='relu', padding='same'))
        model.add(keras.layers.Conv2D(64, (5,5), activation='relu', padding='same'))
        model.add(keras.layers.MaxPooling2D(pool_size=(2,2)))
        model.add(keras.layers.Flatten())
        model.add(keras.layers.Dense(units=1024, activation='relu'))
        model.add(keras.layers.Dropout(0.25))
       # Dense layer2: 1024 -> 256
        model.add(keras.layers.Dense(units=256, activation='relu'))
        model.add(keras.layers.Dropout(0.25))
       # Dense layer3: 256 -> 64
        model.add(keras.layers.Dense(units=64, activation='relu'))
        model.add(keras.layers.Dropout(0.25))
        # Output layer: 128 -> 3
        model.add(keras.layers.Dense(units=self.CLASS, activation='softmax'))
        return model

    def crop(self, img, left, right, top, bottom):
        x, y = img.size
        area = (int(x*left), int(y*top), int(x*right), int(y*bottom))
        return img.crop(area)

    def transparency(self, img, material):
        L, W = img.size
        length = int(300*L/W)
        img = img.resize((length,300))
        win = 0
        img_array = np.array(img)
        for i in range(300//9):
            array = img_array[i*9:(i+1)*9, :]
            array_dis = np.linalg.norm(array-np.array(material),axis=2)
            dummy_array_dis = np.where(array_dis<100,0,1)
            array_mean = array.mean(axis=0)
            mean_dis = np.linalg.norm(array_mean-np.array(material),axis=1)
            dummy_mean_dis = np.where(mean_dis<100,0,1)
            dis_count = dummy_array_dis * dummy_mean_dis[np.newaxis,:]
            win += dis_count.sum()
        return win/(300*length)

    def multi_crop(self, img, material):
        res = {}
        for i in range(100,301,2):
            i /= 1000
            for j in range(100,301,2):
                j /= 1000
                cropped = self.crop(img,i,1-i,j,1-j)
                score = self.transparency(cropped, material)
                res.setdefault((i,j),score)
        return res

    def compute(self, dic,method='mean'):
        scores = list(dic.values())
        if method == 'mean':
            return np.array(scores).mean()
        if method == 'median':
            return np.median(np.array(scores))
        raise ValueError('Indistinguishable method {}'.format(method))

    def run(self):
        features, labels = self.get_files_list((self.brick_1, self.glass_1, self.lime_1), (self.brick_2, self.glass_2, self.lime_2), (self.brick_3, self.glass_3, self.lime_3))
        self.write_record(features, labels, (160,160), 'train.tfrecord')
        dataset = self.input_fn(self.data_dir, [self.LENGTH, self.WIDTH, self.CHANNELS], 1062, self.BATCH_SIZE)
        model = self.my_model()
        model.compile(optimizer='adam',loss='categorical_crossentropy', metrics=['accuracy'])
        history = model.fit(dataset, epochs=self.EPOCHS, steps_per_epoch=22, verbose=1)
        model.save(self.save_path)


