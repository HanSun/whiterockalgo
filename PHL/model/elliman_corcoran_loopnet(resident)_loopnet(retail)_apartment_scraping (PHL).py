import requests
from bs4 import BeautifulSoup
import re
import pandas as pd
import numpy as np
import urllib
import os
import csv
import urllib.request
import html.parser
from requests.exceptions import HTTPError
from socket import error as SocketError
from http.cookiejar import CookieJar
import NY_Residential_Building_Off_Market
import datetime
import time
import random
from fake_useragent import UserAgent
import sys
import warnings
from Compass_scraping import get_compass
from rent_com_scraping_PHL import get_rent_com
from remax_com_scraping_PHL import get_remax_df
if not sys.warnoptions:
    warnings.simplefilter('ignore')
import lycleaner
cleaner = lycleaner.Address_cleaner()
ua = UserAgent()
ua.update()
try:
    ua = UserAgent()
except FakeUserAgentError:
    print(ua.random)

zip_headers={
    'User-Agent': "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
    "Accept": "application/json, text/javascript, */*; q=0.01",
    "Accept-Encoding": "gzip, deflate, br",
    'Accept-Language': 'zh-cn',
    "X-Requested-With": "XMLHttpRequest",
    "Referer": "https://www.mapdevelopers.com/what-is-my-zip-code.php",
    'Connection': 'keep-alive',
    "Host": "www.mapdevelopers.com"
}
zip_url="https://www.mapdevelopers.com/data.php?operation=geocode"

# create csv file to save results for data scraping
def create_csv(base_path,file_name,metrics,base_url):
    with open(os.path.join(base_path,file_name),'w') as f:
        writer = csv.DictWriter(f,metrics)
        writer.writeheader()
        
def create_csv_2(borough):
    metrics = [['ID', 'Address', 'Neighborhood', 'Price', 'Type', 'Info', 'Link']]
    with open(borough+'.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows(metrics)

# Download pictures of broker websites       
def download_pictures(url, page_folder, folder_name):
    """
    Given the quickview url download all the pictures.
    Url: the url get from get_house_info function
    Folder_name: we need to create an independent folder for storing each apartment's pictures. recommend
    to use the id from get_house_info
    """
    save_path = os.path.join(base_path, page_folder, folder_name)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    
    # This url contains no pictures we need, only contains another url on which we can download target pictures
    images_response = requests.get(url)
    if not images_response.status_code == 200:
        raise ValueError('Can not get into original url!')
    images_result = BeautifulSoup(images_response.content, 'lxml')
    images_link = images_result.find('body').get_text()
    ind = images_link.index('https')
    images_link = images_link[ind:-2]
    images_link = images_link.replace('\\','')
    
    # Retrive pictures now
    pictures_response = requests.get(images_link)
    if pictures_response.status_code == 200:
        pictures_results = BeautifulSoup(pictures_response.content,'lxml')
        pictures_pages = pictures_results.find_all('img')
        for picture_page in pictures_pages:
            picture_link = picture_page.get('src')
            if re.search('jpg',picture_link):
                with open(save_path+'/'+os.path.basename(picture_link),'wb') as f:
                    response = requests.get(picture_link)
                    if response.status_code == 200:
                        f.write(response.content)
                    

# go through pages to scrape data                    
def gothrough_page(post_url,request_url,payload,metrics,page_folder, download_pic, base_path, base_url, csv_file_name):
    """
    input: the pages url where we will retrive information
    
    get the information about each apartment listed in the page and write them into a csv file
    and download pictures by calling download_pictures function.
    
    return: next_page: the url of next page to scrape
    """
    #post_url = 'https://www.elliman.com/search/for-rent?sid=42627284#'
    '''
    base_url = 'https://www.elliman.com'
    payload = {'email_address':'13191001861@163.com', 
                 'password':'Gwd7341-676'}
    post_url = "https://www.elliman.com/login"
    request_url = 'https://www.elliman.com/search/for-rent/search-341?sdid=1&sid=42628490&sk=1'    
    '''     
    #try:
    with requests.Session() as session:
        post = session.post(post_url, data=payload)
        response = session.get(request_url)
        if not response.status_code == 200:
            raise ValueError('Can not open page')
        
    # Gather informations of 30 items in one page
    result_page = BeautifulSoup(response.content,'lxml')
    heads = result_page.find('div',class_='w_description').find_all('strong')
    heads = [head.get_text() for head in heads]
    try:
        first_info = result_page.find('div',class_='w_listitem _listview first').find('div',class_='w_listitem_description').find('ul')
    except:
        first_info = ''
    try:
        last_info = result_page.find('div',class_='w_listitem _listview last').find('div',class_='w_listitem_description').find('ul')
    except:
        last_info = ''
    
    infos = result_page.find_all('div',class_='w_listitem _listview')
    try:
        first_images = result_page.find('div',class_='w_listitem _listview first').find('a',class_='w_listitem_quickview').get('href').replace('#', '')
    except:
        first_images = ''
    try:
        last_images = result_page.find('div',class_='w_listitem _listview last').find('a',class_='w_listitem_quickview').get('href').replace('#', '')
    except:
        last_images = ''
    
    images=[]
    for image in infos:
        #print(image.find('a',class_='w_listitem_quickview').get('href').replace('#', ''))
        print('')
        try:
            images.append(image.find('a',class_='w_listitem_quickview').get('href').replace('#', ''))
        except:
            images.append('')
# images = [image.find('a',class_='w_listitem_quickview').get('href').replace('#', '') for image in infos]
    infos = [info.find('div',class_='w_listitem_description').find('ul') for info in infos]
    images.insert(0,first_images)
    images.append(last_images)
    infos.insert(0,first_info)
    infos.append(last_info)
    infos = zip(infos,images)
    
    # Write informations into csv file and download images
    print('Processing {} to {} items of {}'.format(*heads))
    for info, image in infos:
        try:
            info_dict={}
            info_hash = set(info.find_all('li'))
        except:
            print('Some problem with {}'.format(request_url))
        # write informations here
        try:
            address_ = info.find('li',class_='listing_address first')
            info_hash.remove(address_)
            address_ = address_.find('a')
            info_dict.update({'item_link':base_url+address_.get('href')})
            text = address_.get_text()
            ind = text.index(' - ')
            address = text[:ind].strip()
            neighbourhood = text[ind+1:].strip()
            info_dict.update({'Address':address, 'Neighbourhood':neighbourhood})

            price_ = info.find('li',class_='listing_price')
            info_hash.remove(price_)
            price_ = price_.get_text()
            info_dict.update({'Price': ''.join(x for x in price_ if x.isnumeric())})

            features_ = info.find('li',class_='listing_features')
            info_hash.remove(features_)
            features_ = features_.get_text()
            info_dict.update({'Features': features_})

            id_ = info.find('li',class_='last listing_id')
            info_hash.remove(id_)
            id_ = id_.get_text()
            info_dict.update({'ID':''.join(x for x in id_ if x.isnumeric())})

            try:
                area_ = info.find_all('li')[4]
                if area_ in info_hash:
                    area_ = area_.get_text()
                    area_ = area_.split('|')[0]
                    info_dict.update({'Area(Sq. Feet)': ''.join(x for x in area_ if x.isnumeric())})
            except:
                info_dict.update({'Area(Sq. Feet)': ''})

            with open(os.path.join(base_path,csv_file_name),'a') as f:
                writer = csv.DictWriter(f,metrics)
                writer.writerow(info_dict)
                
            # download images here
            if download_pic == True:
                if not image == '':
                    images_link = base_url + image
                    download_pictures(images_link, page_folder, ''.join(x for x in id_ if x.isnumeric()))
        except:
            continue
        
        
    next_ = result_page.find('span',class_='iui_paginator_next').find('a')
    print('Done with this page')
    return next_

def go_through_page_2(link, csv_file, page_num):
    print('Begin with page {}'.format(page_num))
    headers = {'User-Agent':USER, 'Cookie':COOKIE}
    response = requests.get(link, headers=headers)
    
    if not response.status_code == 200:
        return None
    page = BeautifulSoup(response.content, 'lxml')
    search_results = page.find('div', class_='Search-Results-Block').find_all('div', class_='row')
    
    results = []
    for search_result in search_results:
        results += search_result.find_all('div', class_='col-xs-12 col-sm-6 col-md-4 col-lg-3 listing NudgeDown')
    if results == []:
        raise ValueError('End up with page {}'.format(page_num))
    for item in results:
        try:
            #item = results[0]
            id_ = item.find('div', class_='listing').get('data-listingid')
            information = item.find('div', class_='info-wrapper info')
            address = information.find('a', class_='address').get_text().strip()
            link = BASE + information.find('a', class_='address').get('href')
            others = information.find_all('div')
        except:
            id_ = ''
            information = ''
            address = ''
            link = ''
            other = ''
        try:
            price = others[0].get_text().strip()
            infos = others[1].get_text().split('/')
            type_ = infos[-1].strip()
            info = '; '.join(i.strip() for i in infos[:-1])
            neighborhood = others[2].find('a').get_text().strip()

        except:
            price = ''
            infos = ''
            type_ = ''
            info = ''
            neighborhood = ''
        row = [id_, address, neighborhood, price, type_, info, link]
        with open(csv_file, 'a') as f:
            writer = csv.writer(f)
            writer.writerows([row])

    
    next_page = page.find('div', class_='PageControls').find('a', title='Go to next page')
    print('>...')
    print('\n')
    return next_page.get('href')

def skip_page(post_url,request_url,payload):
    with requests.Session() as session:
        post = session.post(post_url, data=payload)
        response = session.get(request_url)
        if not response.status_code == 200:
            raise ValueError('Can not open page')
        result_page = BeautifulSoup(response.content,'lxml')
        next_ = result_page.find('span',class_='iui_paginator_next').find('a')
    return next_

def retrive_all_data(first_page,post_url,payload,metrics, download_pic, base_path, base_url, csv_file_name, start_page=1):
    page = 1
    next_page_tag = first_page
    while page < start_page:
        if page == 1:
            next_page_url = first_page
        else:
            next_page_url = base_url + next_page_tag.get('href')
        next_page_tag = skip_page(post_url,next_page_url,payload)
        page += 1

    while next_page_tag is not None:
        if page == 1:
            next_page_url = first_page
        else:
            next_page_url = base_url + next_page_tag.get('href')
        page_folder = f'Page{page}'
        if not os.path.exists(os.path.join(base_path,page_folder)):
            os.makedirs(os.path.join(base_path,page_folder))
        next_page_tag = gothrough_page(post_url,
                                       next_page_url,
                                       payload,
                                       metrics,
                                       page_folder,
                                       download_pic,
                                       base_path,
                                       base_url,
                                       csv_file_name)
        page += 1
    print('Done!')

################################# Loopnet Retail ##############################
def get_retail_urls():
    """
    Get Retail URL
    
    Returns
    -------
    keep : list
        a list of Retail URL
    """    
    for i in range(1,26):
        try:
            if i==1:
                #url = r'https://www.loopnet.com/for-lease/{}/?sk=0dc60e26260a56004b2aae26bf2e4ad4&bb=7i4js73uxH-x-u9lS'.format(i)
                url = r'https://www.loopnet.com/for-lease/philadelphia-pa/generalretail/{}/?sk=a9736bce3cbeef296fb09583dcbdb151&bb=q97-l_vjgIt_u7rx4P'.format(i)
                headers ={'User-Agent': ua.random}
                res = requests.get(url, headers=headers)
                page_content = BeautifulSoup(res.content,'lxml')
                urls = page_content.findAll('a', href=re.compile(r'https://www.loopnet.com/Listing/'))
                retail_urls = list(set([url['href'] for url in urls]))
                print('We have got {} retail url'.format(len(retail_urls)))
            else:
                time.sleep(30)
                url = r'https://www.loopnet.com/for-lease/philadelphia-pa/generalretail/{}/?sk=a9736bce3cbeef296fb09583dcbdb151&bb=q97-l_vjgIt_u7rx4P'.format(i)
                headers ={'User-Agent': ua.random}
                res = requests.get(url, headers=headers)
                page_content = BeautifulSoup(res.content,'lxml')
                urls = page_content.findAll('a', href=re.compile(r'https://www.loopnet.com/Listing/'))
                temp_urls = list(set([url['href'] for url in urls]))
                retail_urls.extend(temp_urls)
                print('We have got {} retail url'.format(len(retail_urls)))
        except requests.exceptions.ConnectionError:
            #Most free proxies will often get connection errors. You will have retry the entire request using another proxy to work. 
            #We will just skip retries as its beyond the scope of this tutorial and we are only downloading a single url 
            print("Skipping. Connnection error")
    
    return retail_urls


def get_df(page_content1):
    """
    Get a retail building's information from URL
    ----------
    page_content1 : BeautifulSoup
       
    Returns
    -------
    keep : DataFrame
        a table of a retail building's information
    """    
    Address = ['Address', page_content1.findAll('span',class_='inline-block wrap-padding')[0].text]
    borough_zip = page_content1.findAll('span',class_='inline-block remove-comma-wrap')[0].text.replace('\n','').replace('\r','').strip(',').split(',')
    borough_zip = [i.strip() for i in borough_zip]
    Borough = ['Borough', borough_zip[0]]
    Zip = ['Zip', borough_zip[1]]

    info = page_content1.select('#top > section > main > section > div.module.profile-wrapper > div.row.profile-content-wrapper > div > div > div.column-08.column-ex-large-09.column-large-09.column-medium-09.profile-main-info > section.property-details > ul')[0].text
    info = info.strip('\n').replace('\n',',').split(',')
    info = pd.DataFrame([i.split(':') for i in info])
    
    info = info.append(pd.DataFrame([Address, Borough, Zip]))
    tables = page_content1.select('table',class_='property-type featured-grid')
    global df_list
    df_list = []
    
    # get Info Table
    for i, table in enumerate(tables):
        if i== 0:
            df_list.append(pd.read_html(table.prettify())[0])
        elif 'Rental Rate' == pd.read_html(table.prettify())[0].iloc[0,0]:
            df_list.append(pd.read_html(table.prettify())[0])
    
    info = info.append(df_list[0].iloc[:,0:2].append(pd.DataFrame(df_list[0].iloc[:,2:].values), ignore_index=True), ignore_index=True)
    if info[info.iloc[:,0]=='Rental Rate'].index.to_list() != []:
        info = info.drop(index=info[info.iloc[:,0]=='Rental Rate'].index[0])
        
    df = pd.DataFrame()

    for i in range(1,len(df_list)):    
        if i == 1:
            df = info.append(pd.DataFrame(df_list[i].iloc[:,0:2].values), ignore_index=True)
            df = df.append(pd.DataFrame(df_list[i].iloc[:,2:].values), ignore_index=True)
            df.columns = ['0','1']
            df.set_index('0', inplace = True)
            df.dropna(inplace = True)
        else:
            temp_df = info.append(pd.DataFrame(df_list[i].iloc[:,0:2].values))
            temp_df = temp_df.append(pd.DataFrame(df_list[i].iloc[:,2:].values))        
            temp_df.columns = ['0','1']
            temp_df.set_index('0', inplace = True)
            temp_df.dropna(inplace = True)


            df = df.merge(temp_df, left_index = True, right_index=True, how = 'outer')
    
    df.columns = df.loc['Listing ID'].to_list()
    return df

def get_all_retail(retail_urls):  
    """
    Get all retail buildings' information from URL
    ----------
    retail_urls : list
       
    Returns
    -------
    keep : DataFrame
        a table of all retail buildings' information
    """    
    global loopnet_retail_scrape_error
    loopnet_retail_scrape_error = []
    df = pd.DataFrame()
    temp= pd.DataFrame()
    for i in range(len(retail_urls)):
        try:
            if i == 0:
                print(i)
                headers = {'User-Agent': ua.random}
                res1 = requests.get(retail_urls[i], headers=headers)
                page_content1 = BeautifulSoup(res1.content,'lxml')
                #get address
                df = get_df(page_content1)
                df.loc['Link'] = retail_urls[i]
            else:
                time.sleep(15)
                print(i)
                headers ={'User-Agent': ua.random}
                res1 = requests.get(retail_urls[i], headers=headers)
                page_content1 = BeautifulSoup(res1.content,'lxml')
                #get address
                temp = get_df(page_content1)
                temp.loc['Link'] = retail_urls[i]

                df = df.merge(temp, left_index = True, right_index=True, how = 'outer')
        except BaseException as e:
            print('An error occurs: ', e)
            loopnet_retail_scrape_error.append(retail_urls[i])
            pass
        
    return df

def loopnet_retail_cleaner(loopnet_retail_df):
    loopnet_retail_df = loopnet_retail_df[['Address', 'Borough', 'Build Out As','Date Available','Date Created', 
                                           'Floor','Last Updated','Lease Term', 'Link','Listing ID','Property Sub-type', 'Property Type',
                                           'Rental Rate','Space Available','Space Type', 'Year Built','Zip']]
    #loopnet_retail_df.set_index('0',inplace = True)
    loopnet_retail_df['Rental Rate'] = loopnet_retail_df['Rental Rate'].apply(lambda x: re.findall(re.compile(r'Upon Request$'),x)[-1] if re.findall(re.compile(r'Upon Request$'),x)!=[] else x)
    loopnet_retail_df['Rental Rate'] = loopnet_retail_df['Rental Rate'].apply(lambda x: x.replace('/Yr','/Yr;').replace('/Mo','/Mo;').strip(';'))
    loopnet_retail_df['Rental Rate'] = loopnet_retail_df['Rental Rate'].apply(lambda x: x.split(';')).apply(lambda x: x[-1].strip() if len(x) == 1 else x[-2].strip())
    loopnet_retail_df[['State','Zip']] = loopnet_retail_df['Zip'].str.split(' ', n=1, expand=True)
    loopnet_retail_df.rename(columns={'Space Available':'Retail SF','Gross Leasable Area':'Retail SF','Building Size':'GSF',
                       'Rental Rate':'Rent','Lease Term':'Term','Date Available': 'Available','Borough':'City', 'Year Built':'Year built',
                       'Build Out As':'Build Out','Property Sub-type':'Sub-type','Last Updated':'Last updated'}, inplace = True)
    loopnet_retail_df['Sub-type'] = loopnet_retail_df['Sub-type'].replace(regex = {'  More...  ':'/'})
    loopnet_retail_df['Floor'] = np.where((loopnet_retail_df['Floor'] == 'Concourse')|(loopnet_retail_df['Floor'] == 'Lobby')|(loopnet_retail_df['Floor'] == 'Unknown Floor'),np.nan,loopnet_retail_df['Floor']) 
    loopnet_retail_df['Retail Basement'] = np.where((loopnet_retail_df['Floor'] == 'Lower Level')|(loopnet_retail_df['Floor'] == 'Basement'),'1','0') 
    loopnet_retail_df['Retail Ground'] = np.where((loopnet_retail_df['Floor'] == 'Ground') | (loopnet_retail_df['Floor'] == 'Mezzanine')|(loopnet_retail_df['Floor'] == '1st Floor'),'1','0') 
    loopnet_retail_df['Retail High floor'] = np.where((loopnet_retail_df['Floor'] != 'Ground')&(loopnet_retail_df['Floor'] != 'Mezzanine')&(loopnet_retail_df['Floor'] != '1st Floor')&(loopnet_retail_df['Floor'] != 'Lower Level')&(loopnet_retail_df['Floor'] != 'Basement'),'1','0') 
    loopnet_retail_df.drop('Floor',1,inplace = True)
    '''
    loopnet_retail_df.to_csv('/home/ubuntu/Area/NYC/loopnet/Loopnet_Master_table.csv')
    loopnet_retail_df = pd.read_csv('Loopnet_Retail_Master_File.csv', index_col=0)
    '''
    loopnet_retail_df['Address'] = cleaner.easy_clean(loopnet_retail_df['Address'].str.upper())
    return loopnet_retail_df

################################# Loopnet Residential ##############################
def get_loopnet_id():    
    """
    Get Residential URL
    
    Returns
    -------
    keep : list
        a list of Residential URL
    """    
    for i in range(1,21):
        url = r'https://www.loopnet.com/pennsylvania/philadelphia-county_multifamily-properties-for-sale/'+str(i)+'/'
        headers ={'User-Agent': ua.random}
        res = requests.get(url, headers=headers)
        page_content = BeautifulSoup(res.content,'lxml')
        if i == 1:
            print(i)
            ID = [l['href'] for l in page_content.findAll('a',href=re.compile(r'/Listing/.+/\d{8}/'))]

        else:
            print(i)
            temp = [l['href'] for l in page_content.findAll('a',href=re.compile(r'/Listing/.+/\d{8}/'))]

            ID.extend(temp)
        time.sleep(15)
    return ID

def unique(items):
    """
    Get unique items from the input
    ----------
    item : list
        Building Parcel ID
    Returns
    -------
    keep : list
        a list of unique Parcel ID
    """    
    found = set(['15215378'])
    keep = []

    for item in items:
        if item not in found:
            found.add(item)
            keep.append(item)

    return keep

def get_info(id_):
    """
    Get building information with the ID
    ----------
    id_ : str
        Building Listing ID
    Returns
    -------
    final_table1, final_table2 : DataFrame
        two tables for a building
    """    
    url = 'https://www.loopnet.com'+id_
    #id_= '/Listing/2288-Mott-Ave-Far-Rockaway-NY/16765551/'
    headers ={'User-Agent': ua.random}
    req = requests.get(url=url,headers=headers)
    page_content = BeautifulSoup(req.text, 'lxml')
    
    def get_address():
        '''
        tag = page_content.find_all('h2')[-1]
        address = tag.get_text().replace('ABOUT','').replace('\r','').replace('\n','').split(',')
        df = pd.DataFrame([['Address',address[0].strip()], ['Borough',address[1]], ['Zip',address[2].strip()]])
        '''
        tag = page_content.find_all('h2')[-1]
        tag1 = page_content.findAll('h1', class_='profile-hero-title')
        address = tag.get_text().replace('ABOUT','').replace('\r','').replace('\n','').split(',')
        title = [t.text for t in tag1]
        #df = pd.DataFrame([['Address',address[0].strip()], ['Borough',address[1]], ['Zip',address[2].strip()], ['Title',title[0]]])
        df = pd.DataFrame([['Address',' - '.join(address)], ['Title',title[0]]])

        return df
    
    def get_tables():   
        tables = page_content.select('table')
        df_list = []
        
        for table in tables:
            df_list.append(pd.concat(pd.read_html(table.prettify())))
        
        if 'Financial Summary' in str(df_list[2].columns[0]):
            table1, table2 = df_list[0], df_list[2]
            
        else:
            table1, table2 = df_list[0], pd.DataFrame()
        
        table_new  = table1.iloc[:,:2].append(pd.DataFrame(table1.iloc[:,2:].values))
        
        if not table2.empty:
            table2_new = table2.iloc[:,:-1]
            table2_new.columns = [0,1]
            table_new = pd.concat([table_new, table2_new],ignore_index = True)
            
        return table_new.dropna()
    
    def get_timestamp():
        t = page_content.findAll('ul', class_='property-timestamp')
        results = [tt.text for tt in t][0]
        time = results.strip('\n').split('\n')
        time = pd.Series(time)
        time = pd.DataFrame(time.str.split(': ', n=1, expand=True))
        return time

    def get_link():
        link = pd.DataFrame(['Link',url]).T
        return link
    
    def get_salesnote():
        try:
            notes = page_content.find('section', class_='description include-in-page').get_text()
            return pd.DataFrame([notes.replace('\n','').strip().split('\r')[0], notes.replace('\n','').strip().split('\r')[-1]]).T
        except AttributeError:
            print('There is not Sales Notes')
            return pd.DataFrame()
    
    Address = get_address()
    Table = get_tables()
    Time = get_timestamp()
    Link = get_link()
    Sale_note = get_salesnote()

    final_table1 = pd.concat([Address, Table, Time, Link, Sale_note],ignore_index = True).set_index([0]).T
    if 'Total Expenses' in final_table1.columns:
        final_table1 = final_table1.drop(['Other Income', 'Total Expenses'],1)
    #final_table2 = pd.concat([Address, Sale_note, Link],ignore_index = True).set_index([0]).T
    
    return final_table1

def get_tables(ID):   
    """
    Get all building information with the ID
    ----------
    ID : list
        a list of Building Listing ID
    Returns
    -------
    final_table1, final_table2 : DataFrame
        two tables for all buildings
    """    
    global loopnet_residential_scrape_error
    loopnet_residential_scrape_error = []
    table1 = pd.DataFrame()        
    for i in range(len(ID)):
        try:
            if i == 0:
                print(i, ID[i])
                temp1 = get_info(ID[i])
                if 'Total Building Size' in temp1.columns:
                    temp1.rename(columns={'Total Building Size':'Building Size'},inplace = True)
                    temp1.rename(columns={'Total Land Area':'Lot Size'},inplace = True)
        
                table1 = pd.concat([table1,temp1],ignore_index = True)
                #table2 = pd.concat([table2,temp2],ignore_index = True)
                time.sleep(2)
                
            else:
                print(i, ID[i])
                temp1 = get_info(ID[i])
                if 'Total Building Size' in temp1.columns:
                    temp1.rename(columns={'Total Building Size':'Building Size'},inplace = True)
                    temp1.rename(columns={'Total Land Area':'Lot Size'},inplace = True)
        
        
                table1 = pd.concat([table1,temp1],ignore_index = True)
                #table2 = pd.concat([table2,temp2],ignore_index = True)
                time.sleep(2)
        except:
            loopnet_residential_scrape_error.append(ID[i])
            pass
    
    #final_table1 = table1[table1['Address'].apply(lambda x: not bool(re.search('\$', x)))]
    #final_table2 = table2
    
    return table1

def loopnet_residential_cleaner(loopnet_residential_df):
    loopnet_residential_df['Address'] = loopnet_residential_df['Address'].apply(lambda x: x.strip())
    loopnet_residential_df[['Address','Zip']]= loopnet_residential_df['Address'].str.split(r" PA ",expand = True)
    loopnet_residential_df['Cap Rate'].fillna(loopnet_residential_df['Address'].apply(lambda x: re.findall(r'[-\d.]+\d%',x)[0] if re.findall(r'[-\d.]+\d%',x) != [] else np.nan),inplace = True)
    loopnet_residential_df['Price'].fillna(loopnet_residential_df['Address'].apply(lambda x: re.findall(r"\$\d+ - \d+ - \d+|\$\d+ - \d+", x)[0].replace(' - ',',') if re.findall(r"\$\d+ - \d+ - \d+|\$\d+ - \d+", x) != [] else np.nan),inplace=True)
    loopnet_residential_df[['Building Size']] = loopnet_residential_df[['Building Size']].replace(regex = {' SF':'',',':''})
    loopnet_residential_df[['Unit Size']] = loopnet_residential_df[['Unit Size']].replace(regex = {' SF':'',',':''})
    loopnet_residential_df[['Building Size']] = loopnet_residential_df[['Building Size']].replace(regex = {' SF':'',',':''})
    try:
        loopnet_residential_df[['Unit Size']] = loopnet_residential_df[['Unit Size']].replace(regex = {' SF':'',',':''})
    except:
        pass
    
    try:
        loopnet_residential_df[['Effective Gross Income']] = loopnet_residential_df[['Effective Gross Income']].replace(regex = {'\$':'',',':''})
    except:
        loopnet_residential_df['Effective Gross Income'] = np.nan
    try:
        loopnet_residential_df[['Gross Rental Income']] = loopnet_residential_df[['Gross Rental Income']].replace(regex = {'\$':'',',':''})
    except:
        loopnet_residential_df['Gross Rental Income'] = np.nan
    try:
        loopnet_residential_df[['Net Operating Income']] = loopnet_residential_df[['Net Operating Income']].replace(regex = {'\$':'',',':''})
    except:
        loopnet_residential_df['Net Operating Income'] = np.nan
    try:
        loopnet_residential_df[['Operating Expenses']] = loopnet_residential_df[['Operating Expenses']].replace(regex = {'\$':'',',':''})
    except:
        loopnet_residential_df['Operating Expenses'] = np.nan
    try:
        loopnet_residential_df[['Price']] = loopnet_residential_df[['Price']].replace(regex = {'\$':'',',':''})
    except:
        loopnet_residential_df['Price'] = np.nan
    try:
        loopnet_residential_df[['Taxes']] = loopnet_residential_df[['Taxes']].replace(regex = {'\$':'',',':''})
    except:
        loopnet_residential_df['Taxes'] = np.nan
    try:
        loopnet_residential_df[['Vacancy Loss']] = loopnet_residential_df[['Vacancy Loss']].replace(regex = {'\$':'',',':''})
    except:
        loopnet_residential_df['Vacancy Loss'] = np.nan
    #loopnet_residential_df[['Effective Gross Income','Gross Rental Income','Net Operating Income','Operating Expenses','Price','Taxes','Vacancy Loss']] = loopnet_residential_df[['Effective Gross Income','Gross Rental Income','Net Operating Income','Operating Expenses','Price','Taxes','Vacancy Loss']].replace(regex = {'\$':'',',':''})
    loopnet_residential_df['Price'] = loopnet_residential_df['Price'].apply(lambda x: x.replace('Upon Request','0')).apply(lambda x: x.split(' - ')[-1] if '-' in x else x)
    #loopnet_residential_df[['Effective Gross Income','Gross Rental Income','Net Operating Income','Operating Expenses','Price','Taxes','Vacancy Loss']] =     loopnet_residential_df[['Effective Gross Income','Gross Rental Income','Net Operating Income','Operating Expenses','Price','Taxes','Vacancy Loss']].replace(regex = {'-':'0'})
    try:
        loopnet_residential_df[['Effective Gross Income']] = loopnet_residential_df[['Effective Gross Income']].replace(regex = {'-':'0'})
    except:
        loopnet_residential_df['Effective Gross Income'] = np.nan
    try:
        loopnet_residential_df[['Gross Rental Income']] = loopnet_residential_df[['Gross Rental Income']].replace(regex = {'-':'0'})
    except:
        loopnet_residential_df['Gross Rental Income'] = np.nan
    try:
        loopnet_residential_df[['Net Operating Income']] = loopnet_residential_df[['Net Operating Income']].replace(regex = {'-':'0'})
    except:
        loopnet_residential_df['Net Operating Income'] = np.nan
    try:
        loopnet_residential_df[['Operating Expenses']] = loopnet_residential_df[['Operating Expenses']].replace(regex = {'-':'0'})
    except:
        loopnet_residential_df['Operating Expenses'] = np.nan
    try:
        loopnet_residential_df[['Price']] = loopnet_residential_df[['Price']].replace(regex = {'-':'0'})
    except:
        loopnet_residential_df['Price'] = np.nan
    try:
        loopnet_residential_df[['Taxes']] = loopnet_residential_df[['Taxes']].replace(regex = {'-':'0'})
    except:
        loopnet_residential_df['Taxes'] = np.nan
    try:
        loopnet_residential_df[['Vacancy Loss']] = loopnet_residential_df[['Vacancy Loss']].replace(regex = {'-':'0'})
    except:
        loopnet_residential_df['Vacancy Loss'] = np.nan
        
    loopnet_residential_df[['Effective Gross Income','Gross Rental Income','Net Operating Income','Operating Expenses','Price','Taxes','Vacancy Loss']] = loopnet_residential_df[['Effective Gross Income','Gross Rental Income','Net Operating Income','Operating Expenses','Price','Taxes','Vacancy Loss']].astype('float')
   
    loopnet_residential_df['Address'] = loopnet_residential_df['Address'].str.split(r" - .+",expand = True)[0]
    loopnet_residential_df['Title'] = loopnet_residential_df['Title'].str.split(r" - .+",expand = True)[0]
    loopnet_residential_df['Address'] = loopnet_residential_df['Address'].apply(lambda x: np.nan if 'Unit' in str(x) or '$' in str(x) or 'Properties' else x)
    loopnet_residential_df['Address'] = loopnet_residential_df['Address'].fillna(loopnet_residential_df['Title'])
    loopnet_residential_df['Address'] = loopnet_residential_df['Address'].replace(regex={' Portfolio':''})
    loopnet_residential_df = loopnet_residential_df[loopnet_residential_df['Address'].apply(lambda x: x[0].isnumeric())]
    loopnet_residential_df['State'] = 'PA'
    loopnet_residential_df = loopnet_residential_df[loopnet_residential_df['Link'].apply(lambda x: 'portfolio' not in x)]
    loopnet_residential_df.dropna(subset = ['Address'], inplace = True)
    loopnet_residential_df['Address'] = cleaner.easy_clean(loopnet_residential_df['Address'].str.upper())
    loopnet_residential_df = loopnet_residential_df.reset_index()
    for i in range(len(loopnet_residential_df.index)):
        try:
            loopnet_residential_df.loc[i,'Zip'] = loopnet_residential_df.loc[i,'Zip'][:5]
        except:
            tmp_data={"address":loopnet_residential_df.loc[i,'Address']+", PA, USA "}
            z = requests.post(zip_url,data=tmp_data,headers=zip_headers)
            loopnet_residential_df.loc[i,'Zip'] = z.json()["data"]["zip"]
    #loopnet_residential_df['Zip'] = loopnet_residential_df['Zip'].apply(lambda x:x[:5])
    return loopnet_residential_df
################################ Get the apartment.com URL ##############################
def get_apartment_url():
    import traceback
    """
    Get individual url for building from Apartment.com

    Returns
    -------
    apt_url : List
        A list of URL for building
    """  
    
    global apartment_url_error
    apartment_url_error = []
    
    page_number = [1]
    for i in page_number:
        try:
            if i == 1:
                url = r'https://www.apartments.com/philadelphia-pa/{}'.format(i)
                ip_list = ['119.6.136.122','114.106.77.14']
                proxies = {"http": random.choice(ip_list)}
                headers ={'User-Agent': ua.random}
                res = requests.get(url, headers=headers, proxies = proxies)
                print('Start to scrape!')
                page_content = BeautifulSoup(res.content,'lxml')    
                last_page = [n.text for n in page_content.findAll('a',href=re.compile(r'https://www.apartments.com/philadelphia-pa/[0-9]+/'))][-2]
                print(int(last_page))
                page_number.extend(range(2,int(last_page)+1))
                urls = page_content.findAll('a',href=re.compile(r'https://www.apartments.com/.*-philadelphia-pa/.*/'))
                apt_url = [url['href'] for url in urls]
                print('We got {} apt url now.'.format(len(apt_url)))
                
            else:
                time.sleep(5)
                url = r'https://www.apartments.com/philadelphia-pa/{}'.format(i)
                ip_list = ['119.6.136.122','114.106.77.14']
                proxies = {"http": random.choice(ip_list)}
                headers ={'User-Agent': ua.random}
                res = requests.get(url, headers=headers, proxies = proxies)
                print('Start to scrape!')
                page_content = BeautifulSoup(res.content,'lxml')    
                urls = page_content.findAll('a',href=re.compile(r'https://www.apartments.com/.*-philadelphia-pa/.*/'))
                temp_url = [url['href'] for url in urls]
                apt_url.extend(temp_url)
                print('We got {} apt url now.'.format(len(apt_url)))
        except Exception as e:
            print('An exception occurred: {}'.format(e))
            traceback.print_exc()
            apartment_url_error.append(i)
            pass        
    apt_url_df = pd.DataFrame(np.array(apt_url))
    #apt_url_df.to_csv('Apartment_url.csv')   
        
    return apt_url
        

################################ Use the apartment URL to get apartment information ##############################
def get_apartment_com_table(apt_url):
    import traceback
    """
    Get building information with the APT URL
    ----------
    apt_url : list
        APT URL
    Returns
    -------
    units_table : DataFrame
        tables for building
    """    
    global apartment_scrape_error
    
    apartment_scrape_error = []
    for i,url in enumerate(apt_url):
        try:
            if i == 0:
                ip_list = ['119.6.136.122','114.106.77.14']
                proxies = {"http": random.choice(ip_list)}
                headers ={'User-Agent': ua.random}
                #url = 'https://www.apartments.com/greybarn-amityville-10-greybarn-lane-amityville-ny/ngb7zfq/'
                res1 = requests.get(url, headers=headers, proxies = proxies)
                print('Start to scrape!')
                page_content1 = BeautifulSoup(res1.content,'lxml')    
                tables = page_content1.select('table',class_= "availabilityTable  ")
                df_list = []
                
                for table in tables:
                    df_list.append(pd.read_html(table.prettify()))
                units_table = df_list[0][0]
                Info = page_content1.find('div', class_= 'propertyAddress').text.replace('\r','').replace('\n','').split(',')
                Info = [i.strip() for i in Info]
                if len(Info)==2:
                    addr = page_content1.find('div', class_= 'propertyName').text.replace('\r','').replace('\n','').replace('|','').split(',')
                    Info.insert(0,addr[0].strip())
                Address = Info[0]
                City = Info[1]
                State = Info[2][:2]
                Zip = Info[2][2:7]
                Pro_name = page_content1.find('h1', class_='propertyName')
                units_table['Address'] = np.array([Address]*units_table.shape[0])
                units_table['City'] = np.array([City]*units_table.shape[0])
                units_table['State'] = np.array([State]*units_table.shape[0])
                units_table['Zip'] = np.array([Zip]*units_table.shape[0])
                units_table['Property Name'] = np.array([page_content1.find('h1', class_='propertyName').text.strip('\r\n').strip()]*units_table.shape[0])
                Washer_Dryer = bool(page_content1.findAll(text=re.compile(r'Washer/Dryer')))
                #Pet_policy = bool(sum([not bool(re.search('NO',a.text.strip(), re.IGNORECASE)) for a in page_content1.findAll("div", {"class": "petPolicyDetails"})]))
                Pet_policy =[a.text for a in page_content1.findAll("div", {"class": "petPolicyDetails"})]
                Pet_policy = ';'.join(Pet_policy)
                Concierge = bool(re.search(re.compile(r'(Doorman|Concierge)',re.IGNORECASE),str(page_content1)))
                units_table['Washer/Dryer'] = np.array([Washer_Dryer]*units_table.shape[0])
                units_table['Pet policy'] = np.array([Pet_policy]*units_table.shape[0])
                units_table['Concierge'] = np.array([Concierge]*units_table.shape[0])
                units_table['Link'] = np.array([url]*units_table.shape[0])
                
            else:
                time.sleep(5)
                ip_list = ['119.6.136.122','114.106.77.14']
                proxies = {"http": random.choice(ip_list)}
                headers ={'User-Agent': ua.random}
                res1 = requests.get(url, headers=headers, proxies = proxies)
                print('Start to scrape!')
                page_content1 = BeautifulSoup(res1.content,'lxml')    
                tables = page_content1.select('table',class_= "availabilityTable  ")
                df_list = []
                
                for table in tables:
                    df_list.append(pd.read_html(table.prettify()))
                temp_table = df_list[0][0]
                Info = page_content1.find('div', class_= 'propertyAddress').text.replace('\r','').replace('\n','').split(',')
                Info = [i.strip() for i in Info]
                if len(Info)==2:
                    addr = page_content1.find('div', class_= 'propertyName').text.replace('\r','').replace('\n','').replace('|','').split(',')
                    Info.insert(0,addr[0].strip())
                Address = Info[0]
                City = Info[1]
                State = Info[2][:2]
                Zip = Info[2][2:7]
                temp_table['Address'] = np.array([Address]*temp_table.shape[0])
                temp_table['City'] = np.array([City]*temp_table.shape[0])
                temp_table['State'] = np.array([State]*temp_table.shape[0])
                temp_table['Zip'] = np.array([Zip]*temp_table.shape[0])
                temp_table['Property Name'] = np.array([page_content1.find('h1', class_='propertyName').text.strip('\r\n').strip()]*temp_table.shape[0])
                Washer_Dryer = bool(page_content1.findAll(text=re.compile(r'Washer/Dryer')))
                #Pet_policy = bool(sum([not bool(re.search('NO',a.text.strip(), re.IGNORECASE)) for a in page_content1.findAll("div", {"class": "petPolicyDetails"})]))
                Pet_policy =[a.text for a in page_content1.findAll("div", {"class": "petPolicyDetails"})]
                Pet_policy = ';'.join(Pet_policy)
                Concierge = bool(re.search(re.compile(r'(Doorman|Concierge)',re.IGNORECASE),str(page_content1)))
                temp_table['Washer/Dryer'] = np.array([Washer_Dryer]*temp_table.shape[0])
                temp_table['Pet policy'] = np.array([Pet_policy]*temp_table.shape[0])
                temp_table['Concierge'] = np.array([Concierge]*temp_table.shape[0])        
                temp_table['Link'] = np.array([url]*temp_table.shape[0])
                units_table = units_table.append(temp_table,ignore_index = True)
        except Exception as e:
            print('An exception occurred: {}'.format(e))
            traceback.print_exc()
            apartment_scrape_error.append(url)
            pass 
        
    #units_table.to_csv('master_apartment_com.csv')
    return units_table

# apartment.com cleaning
def apartment_com_cleaner(apartment_com_df):
    
    apartment_com_df.columns = [str(c) for c in apartment_com_df.columns]
    apartment_com_df['4'].fillna(apartment_com_df['7'], inplace = True)   
    col_2_replace = [('0','Beds'),('1','Baths'),('2','Rent'),('3','Deposit'),('4','APT#'),('5','Sq Ft'),('6','Lease Length'),('8','Available')]
    for x,y in col_2_replace:
        try:
            apartment_com_df[x].fillna(apartment_com_df[y], inplace = True)
            apartment_com_df.drop(y,1,inplace = True)
        except:
            pass
        apartment_com_df.rename(columns = {x:y}, inplace = True)       
    apartment_com_df.rename(columns = {'Pet_policy':'Pet policy'}, inplace = True)
    apartment_com_df.rename(columns = {'Sq Ft':'Unit SF'},inplace = True)
    apartment_com_df = apartment_com_df[['Address', 'Property Name','City', 'State', 'APT#','Zip', 'Washer/Dryer','Pet policy', 'Concierge', 'Beds', 'Baths', 'Rent', 'Unit SF', 'Lease Length','Link']]
    apartment_com_df['Beds'] = apartment_com_df['Beds'].apply(lambda x: 0 if 'S' == str(x)[0] else float(str(x)[0]))
    apartment_com_df['Baths'][apartment_com_df['Baths'].isnull()] = 0
    apartment_com_df['Baths'] = apartment_com_df['Baths'].apply(lambda x: float(str(x)[0]+'.5') if '½' in str(x) else float(str(x)[0]))
    apartment_com_df['Lease Length'][apartment_com_df['Lease Length'].isnull()] = 0
    apartment_com_df['Lease Length'] = apartment_com_df['Lease Length'].replace(regex = {'\d{1,2} - ':''}).apply(lambda x:str(x)[0:2]).astype('float')
    apartment_com_df['Pet policy'] = apartment_com_df['Pet policy'].apply(lambda x: 0 if bool(re.search(r'(?:No|Not).+Allowed', str(x), re.IGNORECASE)) else 1)
    #apartment_com_df[['Washer/Dryer', 'Pet policy','Concierge']] = apartment_com_df[['Washer/Dryer', 'Pet policy','Concierge']].astype(int)
    
    apartment_com_df['Rent'].replace(regex = {'(\$|¥)': '', ',': ''}, inplace = True)
    apartment_com_df['Rent'] = apartment_com_df['Rent'].replace(regex = {'Call for Rent': 0,'  / Person':''})
    apartment_com_df['Rent'] = apartment_com_df['Rent'].apply(lambda x: np.mean([float(i) for i in x.split(' - ')]) if '-' in str(x) else str(x))
    apartment_com_df['Rent'] = apartment_com_df['Rent'].apply(lambda x: float(x) if str(x).isnumeric() else x)
    apartment_com_df['Unit SF'].replace(regex = {'Sq Ft': '', ',':''}, inplace = True)
    apartment_com_df['Unit SF'] = apartment_com_df['Unit SF'].apply(lambda x: [float(i) for i in x.split(' - ')][-1] if '-' in str(x) else float(x))
    apartment_com_df['Address'].replace(regex = {'One':'1'},inplace=True)
    
    apartment_com_df['Address'] = apartment_com_df['Address'].apply(lambda x: np.nan if not str(x)[0].isnumeric() else x)
    apartment_com_df['Address'] = apartment_com_df['Address'].fillna(apartment_com_df['Property Name'].str.split(' - ',n=1, expand = True)[1])
    apartment_com_df.drop('Property Name',1,inplace = True)
    apartment_com_df.dropna(subset=['Address'], inplace=True)
    apartment_com_df['Address'] = cleaner.easy_clean(apartment_com_df['Address'].str.upper())
    
    apartment_com_df['Zip'] = apartment_com_df['Zip'].astype('float')
    apartment_com_df.columns = [c.upper() for c in apartment_com_df.columns]
    apartment_com_df=apartment_com_df.rename(columns={'BATHS':'BATH','BEDS':'BED'})
    apartment_com_df = apartment_com_df[['ADDRESS', 'STATE', 'ZIP', 'WASHER/DRYER', 'PET POLICY','APT#',
                                        'CONCIERGE', 'BED', 'BATH', 'RENT', 'UNIT SF', 'LEASE LENGTH', 'LINK']]
    
    apartment_com_df['PROPERTY TYPE'] = ['Rental']*apartment_com_df.shape[0]
    apartment_com_df = apartment_com_df[apartment_com_df['ADDRESS'].apply(lambda x: x[0].isnumeric())]
    return apartment_com_df

def merge_loopnet_retail_residential(loopnet_retail_df,loopnet_residential_df):
    loopnet_residential_df.columns = [c.capitalize() for c in loopnet_residential_df.columns]
    loopnet_retail_df.columns = [c.capitalize() for c in loopnet_retail_df.columns]
    loopnet_residential_df['Last updated'], loopnet_retail_df['Last updated'] = np.nan, np.nan
    # Merge Loopnet Residential and Loopnet Retail
    for c in list(set(loopnet_residential_df.columns)&set(loopnet_retail_df.columns)):
        print(c, loopnet_residential_df[c].dtypes,loopnet_retail_df[c].dtypes)
        if loopnet_residential_df[c].dtypes == 'float64':
            loopnet_retail_df[c] = loopnet_retail_df[c].astype('float')
          
    loopnet_rsd_rt_df = loopnet_retail_df.merge(loopnet_residential_df, on = list(set(loopnet_retail_df.columns)&set(loopnet_residential_df.columns)), how = 'outer')
    loopnet_rsd_rt_df['Rent'] = loopnet_rsd_rt_df['Rent'].replace(regex = {'Upon Request': 0,' /Mo':'','\$':'',',':''})
    loopnet_rsd_rt_df['Rent'] = loopnet_rsd_rt_df['Rent'].apply(lambda x: np.mean([float(i) for i in x.split(' - ')]) if '-' in str(x) else float(x))
    loopnet_rsd_rt_df['Lot size'] = loopnet_rsd_rt_df['Lot size'].replace(regex={' AC':''}).apply(lambda x: float(x)*43560)
    loopnet_rsd_rt_df['Retail sf'] = loopnet_rsd_rt_df['Retail sf'].replace(regex = {' SF':'',',':''})
    loopnet_rsd_rt_df.rename(columns = {'Building class':'A B C CLASS','Operating expense':'OP EXPENSES','Taxes':'RE TAXES',
                                        'Vacancy loss':'VACANCY','Effective Gross Income':'EGI','Gross Rental Income':'PGI',
                                        'Floors':'# FLOORS','Net Operating Income':'NOI','Price':'Asking Price','No. units':'# UNITS',
                                        'Price / sf':'PRICE PSF','Lot size':'LAND SF','Sub-type':'Condo use','Building size':'GSF',
                                        'Service Type':'Lease Type','Average occupancy':'OCCUP','GROSS RENT MULTIPLIER':'GRM'},inplace = True)
    
    loopnet_rsd_rt_df.drop(['Term', 'Available'],1,inplace = True) 
    loopnet_rsd_rt_df.columns = [c.upper() for c in loopnet_rsd_rt_df.columns]
    loopnet_rsd_rt_df['SALE NOTES'].fillna(loopnet_rsd_rt_df['INVESTMENT SUMMARY'],inplace = True)
    loopnet_rsd_rt_df.drop(['INVESTMENT SUMMARY','CONDO USE','SPACE TYPE'],1,inplace = True)
    loopnet_rsd_rt_df = loopnet_rsd_rt_df[loopnet_rsd_rt_df['STATE'] != 'NJ']
    
    # create a dataframe template
    df = pd.DataFrame(columns = ['ADDRESS', 'APT#', 'BATH', 'BED', 'RENT', 'RENT PSF', 'UNIT SF','GSF','# UNITS', 
                                 '# FLOORS', 'ZIP', 'LAND SF','RETAIL SF', 'COMM'])
    
    
    for c in list(set(df.columns)&set(loopnet_rsd_rt_df.columns)):
        if df[c].dtypes == 'float64':
            loopnet_rsd_rt_df[c] = loopnet_rsd_rt_df[c].astype('float')
        if df[c].dtypes == 'int64':
            loopnet_rsd_rt_df[c] = loopnet_rsd_rt_df[c].astype('float')
            df[c] = loopnet_rsd_rt_df[c].astype('float')
          
    df = df.merge(loopnet_rsd_rt_df, on=list(set(df.columns) & set(loopnet_rsd_rt_df.columns)), how = 'outer')
    return df

def co_de_cleaner(co_df,de_df):
    co_df[['Address','APT']] = lycleaner.split_apt_num(co_df['Address'],', ')
    co_df['APT'] = co_df['APT'].apply(lycleaner.clean_apt_num)
    co_df['Address'] = cleaner.easy_clean(co_df['Address'].str.upper())
    co_df = co_df.dropna(subset=['Address'])
    co_df['Info'] = co_df['Info'].apply(str)
    co_df['Beds'] = co_df['Info'].apply(lycleaner.num_bed)
    co_df['Baths'] = co_df['Info'].apply(lambda x: re.findall(r'[0-9](?= Bath[s]{0,1})',x))
    co_df['Baths'] = co_df['Baths'].apply(lambda x: ''.join(x))
    co_df['Baths'] = co_df['Baths'].apply(lambda x: float(str(x)) if x != '' else np.nan)
    co_df['Price'] = co_df['Price'].replace(regex = {'\$':'',',':''}).apply(lambda x: float(x))
    co_df['Unit SF'] = co_df['Info'].apply(lambda x: x.split(';')[-1] if ' Sq. Ft' in str(x) else np.nan)
    co_df['Unit SF'] = co_df['Unit SF'].apply(lambda x: float(str(x).strip().replace(' Sq. Ft.','').replace(',','')) if ' Sq. Ft.' in str(x) else np.nan)
    co_df.drop('Info',1,inplace = True)
    
    de_df['Address'].replace(regex = {'. #':', '},inplace = True)
    de_df[['Address','APT']] = lycleaner.split_apt_num(de_df['Address'],', ')
    de_df['Address'] = de_df['Address'].apply(lambda x: x.split(', ')[-1])
    de_df['Address'] = cleaner.easy_clean(de_df['Address'].str.upper())
    de_df['Address'] = de_df['Address'].apply(lambda x: x[:np.argmax([i.isalpha() for i in x])]+' '+ x[np.argmax([i.isalpha() for i in x]):] if x[np.argmax([i.isalpha() for i in x])-1] != ' ' else x)
    de_df['Address'] = de_df['Address'].apply(lambda x: x.strip().strip(','))
    de_df['Address'].replace(regex = {' TH ':' '},inplace = True)
    de_df[["Borough","State"]] = lycleaner.split_apt_num(de_df['Neighbourhood'].apply(lambda x: str(x).strip(' - ')),',')
    de_df['State'].replace(regex={' New York':'NY'},inplace=True)
    de_df.drop('Neighbourhood',1,inplace = True)
    de_df['Baths'] = de_df['Features'].apply(lycleaner.remove_num_bed)
    de_df['Beds'] = de_df['Features'].apply(lycleaner.remove_num_bath)
    de_df.rename(columns = {'Area(Sq. Feet)':'Unit SF'}, inplace = True)
    de_df.drop('Features', 1, inplace = True)
    return co_df, de_df

def compass_cleaner(compass):
    compass['ADDRESS'] = np.nan
    compass['ZIP'] = np.nan
    compass['RENT'] = np.nan
    compass['UNIT SF'] = np.nan
    compass['YEAR BUILT'] = np.nan
    compass['RENT PSF'] = np.nan
    
    compass['price'].replace(regex = {'$':''},inplace = True)
    compass['price'].replace(regex = {',':''},inplace = True)
    compass['sqft'].replace(regex = {',':''},inplace = True)
    compass['RENT'] = compass['price']
    compass['UNIT SF'] = compass['sqft']
    
    for i in range(len(compass.index)):
        try:    
            compass.loc[i,'ADDRESS'] = str(compass.loc[i,'address_with_zipcode']).split(',')[0]
        except:
            pass
        
        try:    
            compass.loc[i,'ZIP'] = str(compass.loc[i,'address_with_zipcode']).split('PA ')[-1]
        except:
            pass
            
        try:
            compass.loc[i,'RENT'] = int(compass.loc[i,'price'])
        except:
            compass.loc[i,'RENT'] = int(str(compass.loc[i,'price'])[1:])
        
        try:
            compass.loc[i,'UNIT SF'] = int(compass.loc[i,'UNIT SF'])
        except:
            compass.loc[i,'UNIT SF'] = np.nan
        
        try:
            compass.loc[i,'YEAR BUILT'] = int(compass.loc[i,'year_built'])
        except:
            pass
        
        try:
            compass.loc[i,'RENT PSF'] = int(compass.loc[i,'RENT'])/int(compass.loc[i,'UNIT SF'])*12
        except:
            pass
        
    return compass[['ADDRESS','ZIP','RENT','UNIT SF','YEAR BUILT','RENT PSF']]

def rent_com_cleaner(rent_com):
    rent_com['ADDRESS'] = cleaner.easy_clean(rent_com['ADDRESS'].str.upper())
    rent_com['RENT PSF'] = np.nan
    
    for i in range(len(rent_com.index)):
        try:
            rent_com.loc[i,'RENT PSF'] = int(rent_com.loc[i,'RENT'])/int(rent_com.loc[i,'UNIT SF'])*12
        except:
            pass
        
    return rent_com[['ADDRESS','STATE','ZIP','APT#','RENT','BED','BATH','UNIT SF','RENT PSF']]

def merge_tables(loopnet_retail_df, loopnet_residential_df, apartment_com_df, compass, rent_com):
    loopnet_residential_df.columns = [c.capitalize() for c in loopnet_residential_df.columns]
    loopnet_retail_df.columns = [c.capitalize() for c in loopnet_retail_df.columns]
    loopnet_residential_df['Last updated'], loopnet_retail_df['Last updated'] = np.nan, np.nan
    # Merge Loopnet Residential and Loopnet Retail
    loopnet_retail_df['Zip'] = loopnet_retail_df['Zip'].astype('str')      
    for c in list(set(loopnet_residential_df.columns)&set(loopnet_retail_df.columns)):
        if loopnet_residential_df[c].dtypes == 'float64':
            loopnet_retail_df[c] = loopnet_retail_df[c].astype('float')
    loopnet_rsd_rt_df = loopnet_retail_df.merge(loopnet_residential_df, on = list(set(loopnet_retail_df.columns)&set(loopnet_residential_df.columns)), how = 'outer')
    loopnet_rsd_rt_df['Rent'] = loopnet_rsd_rt_df['Rent'].replace(regex = {'Upon Request': 0,' /Mo':'','\$':'',',':''})
    loopnet_rsd_rt_df['Rent'] = loopnet_rsd_rt_df['Rent'].apply(lambda x: np.mean([float(i) for i in x.split(' - ')]) if '-' in str(x) else float(x))
    loopnet_rsd_rt_df['Lot size'] = loopnet_rsd_rt_df['Lot size'].replace(regex={' AC':''}).apply(lambda x: float(x)*43560)
    loopnet_rsd_rt_df['Retail sf'] = loopnet_rsd_rt_df['Retail sf'].replace(regex = {' SF':'',',':''})
    loopnet_rsd_rt_df.rename(columns = {'Building class':'A B C CLASS','Operating expense':'OP EXPENSES','Taxes':'RE TAXES',
                                        'Vacancy loss':'VACANCY','Effective Gross Income':'EGI','Gross Rental Income':'PGI',
                                        'Floors':'# FLOORS','Net Operating Income':'NOI','Price':'Asking Price','No. units':'# UNITS',
                                        'Price / sf':'PRICE PSF','Lot size':'LAND SF','Sub-type':'Condo use','Building size':'GSF',
                                        'Service Type':'Lease Type','Average occupancy':'OCCUP','GROSS RENT MULTIPLIER':'GRM'},inplace = True)
    
    loopnet_rsd_rt_df.drop(['Term', 'Available'],1,inplace = True) 
    loopnet_rsd_rt_df.columns = [c.upper() for c in loopnet_rsd_rt_df.columns]
    loopnet_rsd_rt_df['SALE NOTES'].fillna(loopnet_rsd_rt_df['INVESTMENT SUMMARY'],inplace = True)
    loopnet_rsd_rt_df.drop(['INVESTMENT SUMMARY','CONDO USE','SPACE TYPE'],1,inplace = True)
    loopnet_rsd_rt_df = loopnet_rsd_rt_df[loopnet_rsd_rt_df['STATE'] != 'NJ']
    
    # create a dataframe template
    df = pd.DataFrame(columns = ['ADDRESS', 'APT#', 'BATH', 'BED', 'RENT', 'RENT PSF', 'UNIT SF','GSF','# UNITS', 
                                 '# FLOORS', 'ZIP', 'LAND SF','RETAIL SF', 'COMM'])
    
    
    for c in list(set(df.columns)&set(loopnet_rsd_rt_df.columns)):
        if df[c].dtypes == 'float64':
            loopnet_rsd_rt_df[c] = loopnet_rsd_rt_df[c].astype('float')
        if df[c].dtypes == 'int64':
            loopnet_rsd_rt_df[c] = loopnet_rsd_rt_df[c].astype('float')
            df[c] = loopnet_rsd_rt_df[c].astype('float')
          
    df = df.merge(loopnet_rsd_rt_df, on=list(set(df.columns) & set(loopnet_rsd_rt_df.columns)), how = 'outer')
    
    
    for c in list(set(df.columns)&set(apartment_com_df.columns)):
        if apartment_com_df[c].dtypes == 'float64':
            print(c)
            df[c] = df[c].astype('float')
        if apartment_com_df[c].dtypes == 'int64':
            print(c)
            df[c] = df[c].astype('float')
            apartment_com_df[c] = apartment_com_df[c].astype('float')
    
    df = df.merge(apartment_com_df, on = list(set(apartment_com_df.columns)&set(df.columns)), how = 'outer')
    #df = df.merge(compass, on = list(set(compass.columns)&set(df.columns)), how = 'outer')
    df = pd.concat([df,compass],axis=0)
    df = pd.concat([df,rent_com],axis=0)
    # merge
    #co_de_df = co_df.merge(de_df, on = list(set(de_df.columns)&set(co_df.columns)), how = 'outer')
    #co_de_df['Link'].fillna(co_de_df['item_link'],inplace = True)
    #co_de_df.drop('item_link',1,inplace = True)
    #co_de_df.rename(columns = {'Price':'Rent','Area(Sq. Feet)':'Unit SF','Baths':'Bath','Beds':'Bed','ID':'Listing id'}, inplace = True)
    #co_de_df.columns = [c.upper() for c in co_de_df.columns]
    #co_de_df['LISTING ID'] = co_de_df['LISTING ID'].astype('str')
    #co_de_df['PROPERTY TYPE'] = ['Rental']*co_de_df.shape[0]
   
    #for c in list(set(co_de_df.columns)&set(df.columns)):
    #    if df[c].dtypes == 'float64':
    #        co_de_df[c] = co_de_df[c].astype('float')
    
    #ny_master = df.merge(co_de_df, on = list(set(co_de_df.columns)&set(df.columns)), how = 'outer')
    import copy
    ny_master = copy.deepcopy(df)
    #print(ny_master.columns)
    #ny_master['BOROUGH'].fillna(ny_master['CITY'], inplace = True)
    ny_master.drop('CITY',1,inplace = True)
    ny_master['STATE'].replace(regex = {' New York':'NY'},inplace = True)
    #ny_master['APT#'].fillna(ny_master['APT'],inplace = True)
    #ny_master.drop('APT',1,inplace = True)
    ny_master['APT#'] = ny_master['APT#'].apply(lambda x: str(x).strip())
    ny_master.rename(columns = {'SERVICE TYPE':'LEASE TYPE'},inplace = True)
    ny_master['COMM'] = np.where(ny_master['RETAIL SF'].isna(),0,1)
    ny_master['RETAIL SF'] = ny_master['RETAIL SF'].apply(lambda x: 0 if '-' in str(x) and float(str(x).split('-')[-1]) - float(str(x).split('-')[0])>=1000 else x)
    ny_master['RETAIL SF'] = ny_master['RETAIL SF'].apply(lambda x: float(str(x).split(' - ')[-1]))
    ny_master = ny_master[ny_master['RENT']!=0]
    ny_master = ny_master[ny_master['UNIT SF']!=0]
    ny_master = ny_master[ny_master['RETAIL SF']!=0]
    ny_master['RENT PSF'] = ny_master['RENT'].astype('float')*12/ny_master['RETAIL SF'].fillna(ny_master['UNIT SF']).astype('float')
    ny_master['PRICE PSF'] = ny_master['ASKING PRICE'].apply(lambda x: np.nan if x == '' else x).astype('float')/ny_master['GSF'].astype('float')
    ny_master['# FLOORS'].fillna(ny_master['NO. STORIES'],inplace = True)
    ny_master.drop('NO. STORIES',1,inplace = True)
    ny_master['LAST UPDATED'] = [datetime.date.today()]*ny_master.shape[0]
    
    # Delete no number address row'# FLOORS'
    ny_master = ny_master[ny_master['ADDRESS'].apply(lambda x: str(x)[0].isnumeric())]
    # Delete condo building roe=w
    ny_master = ny_master[ny_master['PROPERTY TYPE'].apply(lambda x: 'Condo' not in str(x))]
    ny_master = ny_master[ny_master['PROPERTY TYPE']!='Industrial']
    ny_master = ny_master[ny_master['PROPERTY TYPE']!='Land']
    
    ny_master['PRICE CHANGE'] = np.nan
    ny_master['SCRAPED DATE'] = np.nan
    ny_master.drop(['UNIT SIZE', 'SALE NOTES','LEASE LENGTH'], 1,inplace = True)
    ny_master.rename(columns = {'PET_POLICY':'PET POLICY'}, inplace = True)
    ny_master = ny_master[['APT#','ADDRESS', 'STATE', 'ZIP',  'BATH', 'BED', 'RENT', 'RETAIL SF', 'UNIT SF', 'RENT PSF', 'ASKING PRICE', 
                           'PRICE PSF','COMM','BUILD OUT', 'A B C CLASS', 'LINK', 'LISTING ID','PROPERTY TYPE', 'YEAR BUILT',
                           'RETAIL BASEMENT', 'RETAIL GROUND', 'RETAIL HIGH FLOOR', 'APARTMENT STYLE', 'OCCUP', 'GSF', 'CAP RATE', 
                           'EFFECTIVE GROSS INCOME','# FLOORS', 'GROSS RENT MULTIPLIER', 'GROSS RENTAL INCOME', 'LAND SF',
                           'NET OPERATING INCOME', '# UNITS', 'OPERATING EXPENSES', 'RE TAXES', 'VACANCY','WASHER/DRYER', 'PET POLICY', 
                           'CONCIERGE', 'DATE CREATED','LAST UPDATED', 'PRICE CHANGE', 'SCRAPED DATE']]
    
    ny_master['STATE'].fillna('NY',inplace = True)
    ny_master['APT#'] = ny_master['APT#'].apply(lambda x: np.nan if 'Bathroom' in str(x) else x)
    ny_master['ADDRESS'] = ny_master['ADDRESS'].replace(regex = {' DR':'DRIVE'})
    ny_master.to_csv('NY_super_file {}.csv'.format(datetime.date.today()))
    return ny_master

def de_scraping(base_path, download_pic):
    csv_file_name = 'ApartmentInfo.csv'
    metrics = ['ID','Address','Price','Features','Area(Sq. Feet)','item_link','Neighbourhood']
    base_url = 'https://www.elliman.com'
    payload = {'email_address':'13191001861@163.com', 
                 'password':'Gwd7341-676'}
    post_url = "https://www.elliman.com/login"
    first_page = 'https://www.elliman.com/search/for-rent?sid=42628490'    
        
    create_csv(base_path,csv_file_name,metrics,base_url)
    try:
        retrive_all_data(first_page,post_url,payload,metrics, download_pic, base_path, base_url, csv_file_name)
        de_df = pd.read_csv('C:/Users/jorda/OneDrive/Desktop/Douglas Elliman/ApartmentInfo.csv')
        de_df = de_df.drop_duplicates()
        return de_df
    except:
        
        de_df = pd.read_csv('C:/Users/jorda/OneDrive/Desktop/Douglas Elliman/ApartmentInfo.csv')
        de_df = de_df.drop_duplicates()
        return de_df

def co_scraping():

    create_csv_2('manhattan')
    i = 1
    link = MANHATTAN
    while i < 270:
        print(link)
        try:
            link = BASE + go_through_page_2(link, 'manhattan.csv', i)
            if int(link[link.index('Page=')+5:]) == i:
                i += 1
            else:
                break
        except:
            i+=1
    co_df = pd.read_csv('./manhattan.csv')
    co_df = co_df.drop_duplicates()
    return co_df

################################################## Tables Scraping ##################################################
'''
#******************************************************
os.chdir("/home/ubuntu/Area/NYC/loopnet/")
s3_resource = boto3.resource('s3')
bucket = s3_resource.Bucket('gsinvest')
filecsv_table_list = []
#******************************************************

for obj in bucket.objects.filter(Prefix='NYC/loopnet'):
    filecsv_table_list.append(obj.key)

if 'Loopnet_retail_urls.csv' in filecsv_table_list:
    retail_urls_df = pd.read_csv('Loopnet_retail_urls.csv')
    
    retail_urls = retail_urls_df['url'].to_list()
else:
    retail_urls = get_retail_urls()
    retail_urls_df = pd.DataFrame(retail_urls)
    retail_urls_df.columns = ['url']
    retail_urls_df.to_csv('/home/ubuntu/Area/NYC/loopnet/Loopnet_retail_urls.csv')
'''   
''' 
loopnet_retail_df = get_all_retail(retail_urls)
loopnet_residential_df1, loopnet_residential_df2 = get_tables(ID)
apartment_com_df = get_apartment_com_table(apt_url)
'''
def scrape_tables(loopnet_retail, loopnet_residential, apartment_com, cocoran, douglas, download_pic):
    if loopnet_retail:
        # loopnet retail scraping   
        print('Starting scraping Loopnet Retail Tables...')
        global loopnet_retail_df
        retail_urls = get_retail_urls()
        retail_urls = list(set(retail_urls))
        loopnet_retail_df = get_all_retail(retail_urls)
        loopnet_retail_df = loopnet_retail_df.T
        #loopnet_retail_df.to_csv('NY loopnet_retail {}.csv'.format(datetime.date.today()))
        print('Loopnet Retail Tables Done')
        
    if loopnet_residential:
        # loopnet resident scraping
        print('Starting scraping Loopnet Residential Tables...')
        global loopnet_residential_df
        ID = get_loopnet_id()
        ID = unique(ID)
        loopnet_residential_df = get_tables(ID)
        #loopnet_residential_df1, loopnet_residential_df2 = get_tables(ID)
        #loopnet_residential_df = loopnet_residential_df1.merge(loopnet_residential_df2, on = list(set(loopnet_residential_df1.columns)&set(loopnet_residential_df2.columns)), how = 'outer')
        #loopnet_residential_df.to_csv('NY loopnet_residential {}.csv'.format(datetime.date.today()))
        print('Loopnet Residential Tables Done')
 
    if apartment_com:
        # apartment.com scraping
        print('Starting scraping apartment.com Tables...')
        global apartment_com_df
        apt_url = get_apartment_url()
        apt_url = list(set(apt_url))
        apartment_com_df = get_apartment_com_table(apt_url)
        #apartment_com_df.to_csv('NY apartment_com {}.csv'.format(datetime.date.today()))
        print('Apartment.com Tables Done')

    if cocoran:
        global BASE
        global USER
        global COOKIE
        global MANHATTAN
        MANHATTAN = 'https://www.corcoran.com/nyc-real-estate/for-rent/search/manhattan?SaleType=Rent&Count=24&SearchingFrom=%2Fnyc-real-estate%2Ffor-rent%2Fsearch%2Fmanhattan&IsNewDev=false&OnlyExclusives=False&Price.Minimum=0&Price.Maximum=7000&Bedrooms.SearchValue=-0.5&Bathrooms.SearchValue=0.5&Borough=Manhattan&NeighborhoodID=5&NeighborhoodID=8&NeighborhoodID=94&NeighborhoodID=21&NeighborhoodID=22&NeighborhoodID=23&NeighborhoodID=24&NeighborhoodID=34&NeighborhoodID=36&NeighborhoodID=37&NeighborhoodID=39&NeighborhoodID=44&NeighborhoodID=47&NeighborhoodID=49&NeighborhoodID=50&NeighborhoodID=51&NeighborhoodID=523&NeighborhoodID=57&NeighborhoodID=62&NeighborhoodID=63&NeighborhoodID=66&NeighborhoodID=67&NeighborhoodID=524&NeighborhoodID=73&NeighborhoodID=76&NeighborhoodID=83&NeighborhoodID=85&NeighborhoodID=86&NeighborhoodID=87&NeighborhoodID=89&NeighborhoodID=91&HouseStyles=Co-ops&HouseStyles=Condos&HouseStyles=Rental+Buildings&NewSearchName=&SearchName=&TypeOfHome=homes+for+sale&SortBySimplified=Recommended&Page=0'
        BASE = 'https://www.corcoran.com'
        USER = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15'
        COOKIE = 'A4rBlOT06LJ4qVw_1Ww5hM_ZZDqgPQVc_szRPnWOAkr5e0U_Gn6nlAepZZTWJCwlTYzGcGNuFE7NwDAsQ02ncjBHRBLEB6TtXMtf2gHLjSIP2TanRndkw58wwicmGXDy48bbsYpUlGZk33ztEn3tJ0SMQ1E385f_hCDJ3OUltlJziZim8-VcUmc5jK98wYlh1HpZia_qcvyv4Mb9HX2jEj49bGck1tRaCMIWPBfEaSN_2E3Lvtd5MLOl_rNvNJwrYvRe8yOoUWeWFt9pWuF2lNTWNM2-oOnTheghrFkbIUYuSXuZ8s2NVpmM2q321w6i15IRNZDxN-qRObPe_rk_l5wh1Fq-MAZ9x7ephhaffvq_jnD1iirNxc7xVBF0BmcbVZCssEg7wwSJmlzo8jHXsx30oISUqBtnZiQaPmBxZE4UuE11EU4G15styVEDHV38Y-bmt9SMJuG4lbsSyQcxm5FPY5BKjuRllQZkYaVzx7unGrM0zdfV-Vv-7iUmLtt5ttzrB_koLQ9GHha7nrW6nRL3OiCRSR-Sz6yswULhBmb4vnRjClLSNCfV7qYR2G1FqR5fcFT3qMh84hFi_AxOJVmA3omRlDDqI7rk_Zuj05-O-zbovsQB4dGt6Ql2DjcQa8PnQ6Ji7NQB297uzMvylB2AszmX5msnMT96drA6eQNB7v25yfTR1lqKn9Z_PBLL2rGvvBKhtZ7qig8NSsLEYiA7CYIZtBYuUn0V6TbxCPGJ7wamnkj5oX1IWGZlJUYnNJl027m_zItfwf4VweDSSncZ3QqKyQJaJ4in6xoQwZuHZrlIEjKscfeI6YvnF60bXL89S2nIbbbH0ShdIgGLrBm4soCFe2efzg0q7M6NqB1wQuVH8vXLJlkWwt5ZSSS1A1PgzrIgGpqBt6mYhAWPgKfjtodCXVAsALnFjHkwTlJ3dLh3NYskMJvnGV6FXjU5W0-rw-X0bbQelHWRozXKEEjNE5ZuSwC8o7Iv8H-fTI6RQvekJcHhcSxCQB0XMJpxEv0JU5uvN58p2AQnmFEBZsFH8M1LNTNpwZHKeUoqR96kaw7cJA_QttRRtCyVd5xtgOtAkW_Syt-Mz8LUmo71njhIk_k4fjB-gE3B5i_InqCjqUHNPzDAaqmDbdCW71msxHYkTPKgLgmn5EbFU5yeDz2mQ_bN1jSY7cYIv66xCYKK--Jw-CDWYewHUV7-id_O-YqUE-gTnWPJ57lTQupMfcxq_S6DmIYdpQtSnfJ3LSseVOLJ5dGZiVCIC3kZXNKSr7tOwMWopalZBaeVxMwpDnXeJp-9lXoOQ1vvUPf_15cSnjw1gJ_u3ppz5eTru8r8AQ_gfuLtnJrDA9KXYlX10KT853FQJR1Hp_5hp8ZimO4xsvIS8Nyeh0h9A160b4DSqlzuIEwGZ_faFoU2G4wz0IkdUZA6zVQXQQfCRm_qVTxYBZddJSgwEFTBhSNmANpWbqinjdOpJxx-9OlAknKAD3hEvGTDvl9P-74FaLlhZtvEu2cNd9iTdVkYPIt9ySbDrROHfPf3RnVMYKY8dnZsiBXBOw8um2nVImTtnLswcuYVCAvwLzF2Hdb1vrLEm2rhh4ct3ltDl-Z6sFICIv866WDIrL0TzM8ZVcUJT7VhWGQ9ZlaTh_J2-OqAFp4mwUNrM-Cv9yGTQ5zJAosh0jeulrQK9Os; _ga=GA1.2.1718380482.1557598409; _gat_UA-34769542-1=1; _gid=GA1.2.1468713736.1557598409; _fbp=fb.1.1557598408791.76971027; _dc_gtm_UA-34769542-1=1; ai_session=PZg5T|1557598407968|1557601900219; SE=C=24&A=False&SR=nyc&OR=&ST=Rent&T=&HO=False&N=False&F=False&R=False&RS=False&V=False&OD=&SPN=-79228162514264337593543950335&SPX=79228162514264337593543950335&RPN=0&RPX=0&PN=0&PX=7000&Bd=-0.5&Ba=0.5&U=0&BI=0&NI=5,8,94,21,22,23,24,34,36,37,39,44,47,49,50,51,523,57,62,63,66,67,524,73,76,83,85,86,87,89,91&Br=Manhattan&ZO=&S=&BO=None&I=&SN=-1&SX=0&NID=0&ND=False&HC=False&AI=0&P=0&K=&NB=False&Vi=0&TP=0&VT=Image&SB=recommended&H=Co-ops,Condos,Rental Buildings,Condops&F0=&F1=&F2=&F3=&f4=&f5=&f6=&IA=True&B=False&IC=False&Z=&L=0&G=0&RP=&Lon=0&Lat=0&MS=False&OE=False&SF=/nyc-real-estate/for-rent/search/manhattan; nycPV=I1=//mediarouting.vestahub.com/Media/105847633/box/90x120&L1=5746951&R1=nyc&A1=188 East 64th Street,Apt. 2104&N1=Upper East Side&ID1=86&D1=True&P1=$3,995&Z1=0&I0=//mediarouting.vestahub.com/Media/105803634/box/90x120&L0=5746255&R0=nyc&A0=121 East 23rd Street,Apt. 3H&N0=Gramercy&ID0=44&D0=True&P0=$3,250&Z0=0; cto_lwid=85f2710a-dadb-4935-9b90-149387c94f62; __qca=P0-280563343-1557598409012; _sctr=1|1557547200000; _gcl_au=1.1.1922667422.1557598409; _scid=f4dffb8f-507a-4141-84da-9743954d3690; gwcc=%7B%22fallback%22%3A%222123553550%22%2C%22clabel%22%3A%223xVsCP-a65UBEIfF2M8D%22%2C%22backoff%22%3A86400%2C%22backoff_expires%22%3A1557684808%7D; ai_user=T0bTm|2019-05-11T18:13:27.782Z'
        
        # corcoran scraping
        print('Starting scraping Cocoran Tables...')
        global co_df
        co_df = co_scraping()
        print('Cocoran Tables Done')
    
    
    if douglas:
        #if not os.path.exists('C:/Users/Administrator/Desktop/Douglas Elliman'):
        #    os.makedirs('C:/Users/Administrator/Desktop/Douglas Elliman')
            
        if not os.path.exists('C:/Users/jorda/OneDrive/Desktop/Douglas Elliman'):
            os.makedirs('C:/Users/jorda/OneDrive/Desktop/Douglas Elliman')
        
        #base_path = os.path.join('C:/Users/Administrator/Desktop/Douglas Elliman')
        base_path = os.path.join('C:/Users/jorda/OneDrive/Desktop/Douglas Elliman')
        # Douglas Elliman scraping
        print('Starting scraping Douglas Elliman Tables...')
        global de_df
        de_df = de_scraping(base_path, download_pic)
        print('Douglas Elliman Tables Done')
        
        

#%%        
# set True or False to scrape each table
#scrape_tables(loopnet_retail=False, loopnet_residential=True, apartment_com=False, cocoran=False, douglas=False, download_pic= False)
compass = get_compass()
compass = compass_cleaner(compass)
rent_com = get_rent_com()
rent_com = rent_com_cleaner(rent_com)
remax = get_remax_df()
scrape_tables(loopnet_retail=True, loopnet_residential=True, apartment_com=True, cocoran=False, douglas=False, download_pic=False)

'''
loopnet_retail_df = pd.read_csv('NY loopnet_retail 2019-07-31.csv', index_col = 0)
loopnet_residential_df = pd.read_csv('NY loopnet_residential 2019-07-31.csv', index_col = 0)
apartment_com_df = pd.read_csv('NY apartment_com 2019-08-01.csv',index_col = 0)
co_df = pd.read_csv('./manhattan.csv')
co_df = co_df.drop_duplicates()
de_df = pd.read_csv('./Douglas Elliman/ApartmentInfo.csv')
de_df = de_df.drop_duplicates()
'''
################################################## Tables Cleaning ##################################################
def check_from_pluto(pluto, address):
    starts = ''
    ends = ''
    if not '-' in address:
        starts = address.split(' ')[0]+'-'
        ends = ' '.join(address.split(' ')[1:])
    else:
        starts = address.split('-')[0]+'-'
        ends = ' '.join(('-'.join(address.split('-')[1:])).split(' ')[1:])
        
    if len(pluto[ pluto['ADDRESS'] == address ].values) > 0:
        return address
    elif len(pluto[ pluto['ADDRESS'].str.startswith(starts) & pluto['ADDRESS'].str.endswith(ends) ]) > 0:
        return pluto[ pluto['ADDRESS'].str.startswith(starts) & pluto['ADDRESS'].str.endswith(ends) ]['ADDRESS'].values[0]
    else:
        return 0
    
def data_processing(data):
    #data_copy = copy.deepcopy(data)
    data = data.fillna(-1)

    # process with date and yaer
    data[['ADDRESS']] = data[['ADDRESS']].astype(str)
    data[['ZIP']] = data[['ZIP']].astype(int)
    data[['# BATH']] = data[['# BATH']].astype(int)
    data[['# BED']] = data[['# BED']].astype(int)
    data[['VIEW']] = data[['VIEW']].astype(str)
    data[['BLDG CODE']] = data[['BLDG CODE']].astype(str)
    data[['RENT']] = data[['RENT']].astype(int)
    data[['RENT PER BED']] = data[['RENT PER BED']].astype(float)
    data[['RENT PSF']] = data[['RENT PSF']].astype(float)
    data[['SF']] = data[['SF']].astype(int)
    data[['GSF']] = data[['GSF']].astype(int)
    data[['# UNITS']] = data[['# UNITS']].astype(int)
    data[['# FLOORS']] = data[['# FLOORS']].astype(int)
    data[['YEAR BUILT']] = data[['YEAR BUILT']].astype(int)
    data[['LAND SF']] = data[['LAND SF']].astype(int)
    data[['RETAIL SF']] = data[['RETAIL SF']].astype(int)
    data[['LATITUDE']] = data[['LATITUDE']].astype(float)
    data[['LONGITUDE']] = data[['LONGITUDE']].astype(float)
    data[['TOT ASSD $']] = data[['TOT ASSD $']].astype(int)
    data[['# FIREPLACE']] = data[['# FIREPLACE']].astype(int)
    data[['GARAGE']] = data[['GARAGE']].astype(int)
    data[['LOT FRONTAGE']] = data[['LOT FRONTAGE']].astype(float)
    
    data = data.replace('-1', np.nan)
    data = data.replace(-1, np.nan)
    data = data.replace(-1.0, np.nan)
    
    return data

def searching_from_pluto(pluto, address, target):
    return pluto[pluto['ADDRESS'] == address][target].values[0]
    
def get_elevator(pluto, address):
    bldg_class = pluto[pluto['ADDRESS'] == address]['BLDG CLASS NOW'].values[0]
    num_floors = pluto[pluto['ADDRESS'] == address]['# FLOORS'].values[0]
    
    if num_floors >= 5:
        return 1
    else:
        return 0
    
def get_tax_class(pluto, address):
    num_units = pluto[pluto['ADDRESS'] == address]['# UNITS'].values[0]
    
    if num_units > 3:
        return 2
    else:
        return 1
    
def generate_whrk_class(units_num, elevator_num, comm_num, formatting):
    whrk_class_dict = {}
    
    if units_num < 6:
        whrk_class_dict['< 5 UNITS'] = 1
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif units_num < 11:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 1
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif units_num < 21:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 1
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif units_num < 51:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 1
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif units_num < 101:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 1
        whrk_class_dict['> 100 UNITS'] = 0
    else:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 1
        
    if elevator_num == 1:
        whrk_class_dict['ELEVATOR'] = 1
        whrk_class_dict['WALK UP'] = 0
    else:
        whrk_class_dict['ELEVATOR'] = 0
        whrk_class_dict['WALK UP'] = 1
    
    if comm_num > 0:
        whrk_class_dict['MIXED USE'] = 1
    else:
        whrk_class_dict['MIXED USE'] = 0
        
    index_ = []
    for j in ['< 5 UNITS','6 - 10 UNITS','11 - 20 UNITS','21 - 50 UNITS','51 - 100 UNITS','> 100 UNITS','ELEVATOR','WALK UP','MIXED USE']:
        index_.append(whrk_class_dict[j])
    index_ = tuple(index_)
    class_ = 'W'+str(formatting.loc[index_,:].values[0])
    whrk_class_dict['WHRK CLASS'] = class_
    
    return whrk_class_dict

def com_with_dis(img, material, criterion=100, dis=10):
    L, W = img.size
    length = int(680*L/W)
    img = img.resize((length,680))
    img_array = np.array(img)
    
    sampling = lambda: (random.randint(0,55), random.randint(0,39))
    i = 0
    score = []
    ii = 0
    while i < 100:
        sample_1 = sampling()
        sample_2 = sampling()
        if np.linalg.norm(np.array(sample_1)-np.array(sample_2)) >= dis:
            arr_1 = img_array[sample_1[0]*12:(sample_1[0]+1)*12, sample_1[1]*12:(sample_1[1]+1)*12, :].astype(np.int64)
            arr_2 = img_array[sample_2[0]*12:(sample_2[0]+1)*12, sample_2[1]*12:(sample_2[1]+1)*12, :].astype(np.int64)
            if arr_1.shape == (12,12,3) and arr_2.shape == (12,12,3):
                if np.linalg.norm(arr_1-np.array(material),axis=2).mean() <= criterion:
                    if np.linalg.norm(arr_2-np.array(material),axis=2).mean() <= criterion:
                        score.append(np.linalg.norm(arr_1.mean(axis=(0,1)) - arr_2.mean(axis=(0,1))))
                        i += 1
                        
        ii += 1
        
        if ii > 1000000 and i < 100:
            break;
    return np.array(score).mean()

def crop(img, left, right, top, bottom):
    x, y = img.size
    area = (int(x*left), int(y*top), int(x*right), int(y*bottom))
    return img.crop(area)

def transparency(img, material):    
    L, W = img.size
    length = int(300*L/W)
    img = img.resize((length,300))
    win = 0
    img_array = np.array(img)
    for i in range(300//9):
        array = img_array[i*9:(i+1)*9, :]
        array_dis = np.linalg.norm(array-np.array(material),axis=2)
        dummy_array_dis = np.where(array_dis<100,0,1)
        array_mean = array.mean(axis=0)
        mean_dis = np.linalg.norm(array_mean-np.array(material),axis=1)
        dummy_mean_dis = np.where(mean_dis<100,0,1)
        dis_count = dummy_array_dis * dummy_mean_dis[np.newaxis,:]
        win += dis_count.sum()
    return win/(300*length)

def multi_crop(img, material):
    res = {}
    for i in range(100,301,5):
        i /= 1000
        for j in range(100,301,5):
            j /= 1000
            cropped = crop(img,i,1-i,j,1-j)
            score = transparency(cropped, material)
            res.setdefault((i,j),score)
    return res

def compute(dic,method='mean'):
    scores = list(dic.values())
    if method == 'mean':
        return np.array(scores).mean()
    if method == 'median':
        return np.median(np.array(scores))
    raise ValueError('Indistinguishable method {}'.format(method))

def prediction(model, cropped_img, shape: tuple):
	"""
	Before prediction, the test image is recommended to be cropped. Use the function crop in part one
	params:
	cropped_image: raw image data, PIL.Image.Image object
	shape: tuple, (length, width)
	"""
	L, W = shape
	img = np.array(cropped_img.resize(shape))
	img = img/255
	img = img.reshape(1,L,W,3)
	pred = model.predict(img).argmax()
	if pred == 0:
		return 'Brick'
	if pred == 1:
		return 'Glass'
	if pred == 2:
		return 'Limestone'
if len(loopnet_retail_df.index) > 0:
    loopnet_retail_df = loopnet_retail_cleaner(loopnet_retail_df)
else:
    loopnet_retail_df = pd.DataFrame(columns = ['Address', 'City', 'Build out', 'Building class', 'Available',
       'Date created', 'Last updated', 'Term', 'Link', 'Listing id',
       'Sub-type', 'Property type', 'Rent', 'Retail sf', 'Space type',
       'Year built', 'Zip', 'State', 'Retail basement', 'Retail ground',
       'Retail high floor'])
loopnet_residential_df = loopnet_residential_cleaner(loopnet_residential_df)
loopnet_residential_df = loopnet_residential_df[list(loopnet_residential_df.columns)[1:]]
for i in range(len(loopnet_residential_df.index)):
    try:
        loopnet_residential_df.loc[i,'Zip'] = str(int(loopnet_residential_df.loc[i,'Zip']))[:5]
    except:
        loopnet_residential_df.loc[i,'Zip'] = np.nan
apartment_com_df = apartment_com_cleaner(apartment_com_df)

#co_df, de_df = co_de_cleaner(co_df, de_df)
# get final table
ny_master = merge_tables(loopnet_retail_df, loopnet_residential_df, apartment_com_df, compass, rent_com)
ny_master['ADDRESS'] = ny_master['ADDRESS'].replace(regex = {' DR':'DRIVE'})

path = 'C:/Users/Administrator/Desktop/data_need_clean/'
path = 'D:/PHL/master_data/'

ny_master.to_csv(path+'PHL super file {}.{}'.format(datetime.date.today(),'csv'))

old_super = pd.read_csv(path+'PHL-000 Super_Master_File WHRK.csv', index_col = 0)
now_super = pd.read_csv(path+'PHL super file {}.{}'.format(datetime.date.today(),'csv'), index_col = 0)
total_super = pd.concat([now_super,old_super],axis=0)
total_super.to_csv(path+'PHL-000 Super_Master_File WHRK.csv')
# Import necessary files to do the cleansing and merging

#%% Off Market Sale
#sale_off_market_table = NY_Residential_Building_Off_Market.get_sale_off_market_main()

property_for_sale = ny_master.dropna(subset=['ASKING PRICE'])
property_for_sale = property_for_sale[property_for_sale['ASKING PRICE'] > 0]
property_for_sale = property_for_sale.reset_index(drop=True)
property_for_sale = pd.concat([property_for_sale, remax], axis=0)
property_for_rent = ny_master.dropna(subset=['RENT PSF'])
property_for_rent = property_for_rent[property_for_rent['RENT PSF'] > 5]
property_for_rent = property_for_rent[property_for_rent['RENT PSF'] < 500]
property_for_rent = property_for_rent.reset_index(drop=True)


pluto = pd.read_csv(path+'PHLPL-001 All_Properties [byaddress;location] PLUTO.csv')
rent_master = pd.read_csv(path+'PHL-001 Rent_Master [bylocation;addresses].csv',index_col = 0)
#sale_master = pd.read_csv(path+'NMA-002 Resi_Sales_Master [bylocation;addresses].csv',index_col = 0)
rent_master = data_processing(rent_master)
formatting = pd.read_excel(path+'Building_Categories_Format.xlsx')
formatting = formatting.set_index(list(formatting.columns[1:]))

string_cols = ['ADDRESS', 'APT#', 'NEIGHBORHOOD', 'BLDG CODE','VIEW']
int_cols = ['ADDRESS','# BATH', '# BED','RENT','RENT PER BED', 'RENT PSF', 'SF', 'GSF', '# UNITS', '# FLOORS','YEAR BUILT', 'ZIP', 'LAND SF',
            'RETAIL SF','LATITUDE','LONGITUDE','TOT ASSD $','# FIREPLACE','LOT FRONTAGE','GARAGE']
            
init_columns = string_cols+int_cols[1:]
missing_address = []

rent_to_add = pd.DataFrame()
for i in range(len(property_for_rent.index)):
    #tmp = pd.DataFrame(columns = rent_master.columns)
    tmp = pd.DataFrame(columns = init_columns)
    address_ = property_for_rent.loc[i,'ADDRESS']
    if check_from_pluto(pluto, address_) == 0:
        missing_address.append(address_)
        pass
    else:
        cols_pluto = ['GSF','# UNITS','# FLOORS','YEAR BUILT','ZIP','LAND SF','LATITUDE','LONGITUDE','# BATH','# BED','VIEW',
                      'TOT ASSD $','# FIREPLACE','LOT FRONTAGE','GARAGE','BLDG CODE']
        address_ = check_from_pluto(pluto, address_)
        pluto_dict = pluto[pluto['ADDRESS'] == address_][cols_pluto].to_dict()
        #tmp.loc[0,'ADDRESS'] = property_for_rent.loc[i,'ADDRESS']
        tmp.loc[0,'ADDRESS'] = address_
        tmp.loc[0,'APT#'] = property_for_rent.loc[i,'APT#']
        tmp.loc[0,'# BATH'] = list(pluto_dict['# BATH'].values())[0]
        tmp.loc[0,'# BED'] = list(pluto_dict['# BED'].values())[0]
        tmp.loc[0,'NEIGHBORHOOD'] = np.nan
        tmp.loc[0,'RENT'] = property_for_rent.loc[i,'RENT']
        tmp.loc[0,'RENT PER BED'] = property_for_rent.loc[i,'RENT']/list(pluto_dict['# BED'].values())[0]
        tmp.loc[0,'RENT PSF'] = property_for_rent.loc[i,'RENT PSF']
        tmp.loc[0,'SF'] = property_for_rent.loc[i,'UNIT SF']
        tmp.loc[0,'GSF'] = list(pluto_dict['GSF'].values())[0]
        tmp.loc[0,'# UNITS'] = list(pluto_dict['# UNITS'].values())[0]
        tmp.loc[0,'# FLOORS'] = list(pluto_dict['# FLOORS'].values())[0]
        tmp.loc[0,'YEAR BUILT'] = list(pluto_dict['YEAR BUILT'].values())[0]
        tmp.loc[0,'ZIP'] = list(pluto_dict['ZIP'].values())[0]
        tmp.loc[0,'LAND SF'] = list(pluto_dict['LAND SF'].values())[0]
        tmp.loc[0,'RETAIL SF'] = property_for_rent.loc[i,'RETAIL SF']
        
        tmp.loc[0,'BLDG CODE'] = list(pluto_dict['BLDG CODE'].values())[0]
        tmp.loc[0,'LATITUDE'] = list(pluto_dict['LATITUDE'].values())[0]
        tmp.loc[0,'LONGITUDE'] = list(pluto_dict['LONGITUDE'].values())[0]
        tmp.loc[0,'VIEW'] = list(pluto_dict['VIEW'].values())[0]
        tmp.loc[0,'# FIREPLACE'] = list(pluto_dict['# FIREPLACE'].values())[0]
        tmp.loc[0,'TOT ASSD $'] = list(pluto_dict['TOT ASSD $'].values())[0]
        tmp.loc[0,'LOT FRONTAGE'] = list(pluto_dict['LOT FRONTAGE'].values())[0]
        tmp.loc[0,'GARAGE'] = list(pluto_dict['GARAGE'].values())[0]
        
        rent_to_add = pd.concat([rent_to_add, tmp])
        
    print('\rcleaning：{0}{1}%'.format('▉'*int(10*i/len(property_for_rent.index)),round((i/len(property_for_rent.index)*100),2)), end='')

print('\n')
print('Add new units in NY: '+str(len(rent_to_add.index)))

old_len = len(rent_master.groupby('ADDRESS').agg('last').index)
rent_master = pd.concat([rent_master, rent_to_add])
rent_master = rent_master.reset_index(drop=True)
rent_master = data_processing(rent_master)

rent_master.to_csv(path+'PHL-001 Rent_Master [bylocation;addresses].csv')

rent_master_str = rent_master[string_cols].groupby('ADDRESS').agg('last')
rent_index = rent_master_str.index
rent_master_str = rent_master_str.reset_index(drop=True)
rent_master_int = rent_master[int_cols].groupby('ADDRESS').agg('mean')
rent_master_int = rent_master_int.reset_index(drop=True)
rent_master = pd.concat([rent_master_str,rent_master_int],axis=1)
rent_master['ADDRESS'] = list(rent_index)
rent_master = rent_master.reset_index(drop=True)
rent_master = rent_master[['ADDRESS']+list(rent_master)[:-1]]

print('Add new buildings in NY: '+str(len(rent_master.index)-old_len))

date_ = ''.join(str(datetime.date.today()).split('-'))

rent_master_unique_old = pd.read_csv(path+'PHL-001 Rent_Master_Unique [bylocation;addresses].csv', index_col = 0)

from PIL import Image, ImageEnhance
from IPython.display import Image as Im
from IPython.display import display
import os
import glob
import random
from tensorflow import keras
import tensorflow as tf
from PIL import Image
import boto3
import urllib

class PicDownloader:
    def __init__(self):
        self.key  = "AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE"
        
    def download(self, url, name):
        urllib.request.urlretrieve(url.replace(" ", "%20"),"%s.jpg" % name)

    def gen_url(self, geom, fov=100, heading=0, pitch=30, size=(500, 500)):
        # fov控制镜头缩进，数值越小图片越大，最大为120
        # heading控制朝向，0为北，90为东，180为南
        # pitch控制向上的仰视的角度
        x, y = size
        lat, lng = geom
        return "https://maps.googleapis.com/maps/api/streetview?size=%sx%s&location=%s,%s&fov=%s&heading=%s&pitch=%s&key=AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE" % (x, y, lat, lng, fov, heading, pitch)

    def gen_url_by_string(self, address, fov=100, pitch=43, size=(400, 400)):
        x, y = size
        return "https://maps.googleapis.com/maps/api/streetview?size=%sx%s&location=%s&fov=%s&pitch=%s&key=AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE" % (x, y, address, fov, pitch)

PD = PicDownloader()

BUCKET_NAME = 'gsinvest'
#KEY = 'NYC/External_Picture/picture_name.jpg'
s3_resource = boto3.resource('s3')
s3_client = boto3.client('s3')

cnn_model = keras.models.load_model(path+'cnn/Model_July_2.h5')

with open(path+'cnn/features_from_brick.txt','r') as f:
    line = f.readline()
Brick = [float(x) for x in line.split()]

with open(path+'cnn/features_from_lime.txt','r') as f:
    line = f.readline()
Lime = [float(x) for x in line.split()]

rent_master['MATERIAL'] = np.nan
rent_master['TRANSPARENCY'] = np.nan
rent_master['DIRTINESS'] = np.nan

for i in range(len(rent_master.index)):
    try:
        rent_master.loc[i,'MATERIAL'] = rent_master_unique_old[rent_master_unique_old['ADDRESS'] == rent_master.loc[i,'ADDRESS']]['MATERIAL'].values[0]
        rent_master.loc[i,'TRANSPARENCY'] = rent_master_unique_old[rent_master_unique_old['ADDRESS'] == rent_master.loc[i,'ADDRESS']]['TRANSPARENCY'].values[0]
        rent_master.loc[i,'DIRTINESS'] = rent_master_unique_old[rent_master_unique_old['ADDRESS'] == rent_master.loc[i,'ADDRESS']]['DIRTINESS'].values[0]
    except:
        tmp_address = rent_master.loc[i,'ADDRESS']
        
        url = PD.gen_url_by_string(tmp_address+', NYC')
        PD.download(url, path+'tmp')
       
        pic = Image.open(path+'tmp.jpg')
        print('  fetch pic')
        material_label = prediction(cnn_model,pic,(160,160))
        print('  '+material_label)
        if os.path.getsize(path+'tmp.jpg') > 5120:
            #print(material_label)
            
            if material_label == 'Brick':
                res_1 = multi_crop(pic, Brick)
                transparency_rate = compute(res_1,'mean')
                print('  '+str(transparency_rate))
                pic_1 = pic.convert("RGB")
                #display(Im(filename=image))
                contrast = ImageEnhance.Contrast(pic_1)
                contrast = contrast.enhance(1.0)
                clean_score = com_with_dis(contrast, Brick, criterion=80)
                print('  '+str(clean_score))
            elif material_label == 'Limestone':
                res_1 = multi_crop(pic, Lime)
                transparency_rate = compute(res_1,'mean')
                print('  '+str(transparency_rate))
                pic_1 = pic.convert("RGB")
                #display(Im(filename=image))
                contrast = ImageEnhance.Contrast(pic_1)
                contrast = contrast.enhance(1.0)
                clean_score = com_with_dis(contrast, Lime, criterion=80)
                print('  '+str(clean_score))
            else:
                transparency_rate = 1
                clean_score = 0
            
            #print(transparency_rate)
            #print(clean_score)
            
            rent_master.loc[i,'MATERIAL'] = material_label
            rent_master.loc[i,'TRANSPARENCY'] = transparency_rate
            rent_master.loc[i,'DIRTINESS'] = clean_score

rent_master.to_csv(path+'PHL-001 Rent_Master_Unique [bylocation;addresses].csv')
property_for_sale.to_csv(path+'property_to_estimate_PHL_'+date_+'.csv')

########################################################

# TO DO: Please cross the blank ceil in the following columns
# ny_master['ZIP']
# ny_master['RETAIL SF']
# ny_master['COMM']
# ny_master['YEAR BUILT']
# ny_master['GSF']
# ny_master['# UNITS']

# merge with previous file to update new building and new price
# if we have an old file

'''
ny_master_old = pd.read_csv('')
ny_master_old.rename(columns = {'LAST UPDATED':'SCRAPED DATE'})
ny_master_new.rename(columns = {'ASKING PRICE':'PRICE CHANGE'}, inplace = True)
ny_master_old['ASKING PRICE'].fillna(ny_master_new['PRICE CHANGE'],inplace = True)
ny_master_new = ny_master_new.merge(ny_master_old, on = list(set(ny_master_new.columns)&set(ny_master_old,.columns)), how = 'outer')
'''

# TO DO： merge ny_master with pluto, xls, up_df 


'''
pluto = pd.read_csv('./PLUTO.csv')
xls = pd.ExcelFile('./Building_Categories_Format.xlsx')
up_df = pd.read_csv('./NMA-001 NY_Rent_Master [bylocation_address].csv')
'''
########################################################


'''
loopnet_residential_df_copy = loopnet_residential_df.copy()
loopnet_residential_df = loopnet_residential_df_copy.copy()
loopnet_residential_df['Address'] = loopnet_residential_df['Address'].apply(lambda x: x.strip())
loopnet_residential_df[['Address','Zip']]= loopnet_residential_df['Address'].str.split(r" NY ",expand = True)
loopnet_residential_df['Cap Rate'].fillna(loopnet_residential_df['Address'].apply(lambda x: re.findall(r'[-\d.]+\d%',x)[0] if re.findall(r'[-\d.]+\d%',x) != [] else np.nan),inplace = True)
loopnet_residential_df['Price'].fillna(loopnet_residential_df['Address'].apply(lambda x: re.findall(r"\$\d+ - \d+ - \d+|\$\d+ - \d+", x)[0].replace(' - ',',') if re.findall(r"\$\d+ - \d+ - \d+|\$\d+ - \d+", x) != [] else np.nan),inplace=True)



loopnet_residential_df['Address'] = loopnet_residential_df['Address'].str.split(r" - .+",expand = True)[0]
loopnet_residential_df['Title'] = loopnet_residential_df['Title'].str.split(r" - .+",expand = True)[0]
loopnet_residential_df['Address'] = loopnet_residential_df['Address'].apply(lambda x: np.nan if 'Unit' in str(x) or '$' in str(x) or 'Properties' else x)
loopnet_residential_df['Address'] = loopnet_residential_df['Address'].fillna(loopnet_residential_df['Title'])
loopnet_residential_df['Address'] = loopnet_residential_df['Address'].replace(regex={' Portfolio':''})
loopnet_residential_df = loopnet_residential_df[loopnet_residential_df['Address'].apply(lambda x: x[0].isnumeric())]
loopnet_residential_df['State'] = 'NY'
loopnet_residential_df = loopnet_residential_df[loopnet_residential_df['Link'].apply(lambda x: 'portfolio' not in x)]
loopnet_residential_df.dropna(subset = ['Address'], inplace = True)


link_df = loopnet_residential_df['Link'][loopnet_residential_df['Property Type'].isna()]
'''