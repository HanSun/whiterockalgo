#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 20:21:58 2019

@author: zhangzhenhong
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime
import os
import sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

################################### Data Loadinging ###################################
# Fred symbol
symbol = ['MORTGAGE30US','CUUR0100SA0', 'PERMITNE', 'MSPNE', 'RRVRNEQ156N', 'CNERUR']  
# Rename the Fred symbol
var = ['MR', 'CPI', 'HUP', 'MSP','RV','UR'] 

'''
CPI: Consumer Price Index for All Urban Consumers: All items in Northeast
HUP: New Private Housing Units Authorized by Building Permits in the Northeast Census Region
MSP: Median Sales Price of Houses Sold for the Northeast Census Region
RV: Rental Vacancy Rate in the Northeast Census Region
UR: Unemployment Rate in Northeast Census Region
MR: 30-Year Fixed Rate Mortgage Average in the United States
'''        

def data_scrapping(symbol, var):
    """
    Get the data from Fred and preprocess it
    
    Parameters
    ----------
    symbel : target variable and predictors
    var: abbreviation of predictors
    
    Returns
    -------
    data : DateFrame
        a pandas DateFrame containing data from Fred and has been preprocessed
    """
    data = pd.DataFrame()
    from fredapi import Fred
    fred = Fred(api_key='29bf18708bf2fc4f394d7e591275cf5b')
    for i in range(len(symbol)):
        if i == 0:
           data = pd.DataFrame(fred.get_series(symbol[i]),columns = [var[i]])
           data = data.resample('QS',how = 'mean')        
           data.dropna(inplace = True)
        else:
            temp_df = pd.DataFrame(fred.get_series(symbol[i]),columns = [var[i]])
            #temp_df.resample('QS',how = 'mean')
            temp_df.dropna(inplace = True)
            data = pd.merge(data, temp_df, left_index=True, right_index=True,how = 'inner')

    data_4 = data.copy()
    data_4[['MR', 'CPI', 'HUP', 'MSP','RV','UR']] = data_4[['MR', 'CPI', 'HUP', 'MSP','RV','UR']].shift(4)     
    data_4.dropna(inplace = True)
    #data_4.to_csv('/Users/zhangzhenhong/Box/Zhenhong/Release Code/SVR+ARIMA_Rent_Growth/data/dataframe/dataframe.csv')
    return data_4



################################### Model Training ###################################
def train_model(data, start, prediction_periods,label):
    """
    Get the prediction result (SVR + ARIMA, LIN + ARIMA, LASSO + ARIMA) and store the result in a dictionary by the date
    
    Parameters
    ----------
    data : DateFrame
    start: the first prediction date
    label: the macroeconomic symbol to predict
    res_dict: a dictionary to store the predictive result
    
    Returns
    -------
    res_dict : DateFrame
        a pandas DateFrame containing result after training
    """
    data.index = pd.DatetimeIndex(data.index.values, freq='infer')
    start = datetime.datetime.strptime(start, '%Y-%m-%d')

    from dateutil.relativedelta import relativedelta
    def years_delta(years, from_date):
        return from_date + relativedelta(years=years)
    
    def months_delta(months, from_date):
        return from_date + relativedelta(months=months)  
   
    data_start_date = years_delta(years=-10, from_date = start)
    data_end_date = years_delta(years=prediction_periods, from_date = start)
    # training set start date
    training_start = data_start_date
    # training set end date    
    training_end = months_delta(months=-1, from_date = start)
    # testing set start date    
    testing_start = start
    # testing set end date    
    testing_end = months_delta(months=11,from_date = testing_start)
    # ARIMA start date
    arima_start = years_delta(years=1, from_date = start)
    # ARIMA end date
    arima_end = years_delta(years=4, from_date = start)
 
    # Linear Regression
    def linear_regression(X_train_s, y_train_s, X_test_s, y_test_s):
        """
        return: result of testing set
        """
        from sklearn.linear_model import LinearRegression
        global reg
        reg = LinearRegression().fit(X_train_s, y_train_s)
        pred_reg = reg.predict(X_test_s)
        y_hat_lin = y_scaler.inverse_transform(pred_reg)
        y_hat_lin.round(decimals=4) 
        return y_hat_lin

    # Lasso Regression
    def lasso_regression(X_train_s, y_train_s, X_test_s, y_test_s):
        """
        return: result of testing set
        """
        from sklearn.linear_model import Lasso
        from sklearn.linear_model import LassoCV    
        lassocv = LassoCV(fit_intercept = False)
        lassocv.fit(X_train_s, y_train_s)
        alpha = lassocv.alpha_
        global lasso
        lasso = Lasso(alpha,fit_intercept = False)
        lasso.fit(X_train_s, y_train_s)
        pred_reg = lasso.predict(X_test_s)
        y_hat_lasso = y_scaler.inverse_transform(pd.DataFrame(pred_reg))
        y_hat_lasso.round(decimals=4) 
        return y_hat_lasso

    # Support Vector Regression
    # Select the best parameter
    def svc_param_selection(X, y):
        """
        return: the best parameter for SVR
        """
        from sklearn.svm import SVR    
        from sklearn.model_selection import GridSearchCV
        Cs = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10]
        kernels = ['rbf','linear']
        gammas = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10]
        param_grid = {'C': Cs, 'kernel':kernels, 'gamma': gammas}
        grid_search = GridSearchCV(SVR(), param_grid, scoring = 'neg_mean_absolute_error')
        grid_search.fit(X, y)
        grid_search.best_params_
        return grid_search.best_params_
    
    
    def SVR(X_train_s, y_train_s, X_test_s, y_test_s):
        """
        return: result of testing set
        """        
        from sklearn.svm import SVR         
        C, gamma, kernel = svc_param_selection(X_train_s, y_train_s.ravel()).values()
        global svr_reg
        svr_reg = SVR(C=C, gamma=gamma, kernel=kernel).fit(X_train_s, y_train_s.ravel())
        pred_reg = svr_reg.predict(X_test_s)
        y_hat_svr = y_scaler.inverse_transform([pred_reg]).T
        y_hat_svr.round(decimals=4) 
        return y_hat_svr
    
    # ARIMA
    def arima(ts,periods):
        """
        return: result of ARIMA
        """
        from pmdarima.arima import auto_arima
        stepwise_model = auto_arima(ts, start_p=1, start_q=1,
                               max_p=10, max_q=10, m=12,
                               start_P=0, seasonal=True,
                               d=1, D=1, trace=True,
                               error_action='ignore',  
                               suppress_warnings=True, 
                               stepwise=True)
    
        return stepwise_model.predict(periods)
    
    # Create a dictionary to store every quarter prediction result
    res_dict = {}
    
    def train(data, start, label, res_dict):
        training_start = years_delta(years=-10, from_date = start)
        training_end = months_delta(months=-3, from_date = start)
        testing_start = start   
        ts = pd.DataFrame([data[label].loc[training_start:training_end].values]*3).T
        ts.index = pd.date_range(start = training_start, end=training_end, freq = 'QS')
        ts.columns = ['Linear Regression', 'Lasso Regression', 'Support Vector Regression']
    
        from sklearn.preprocessing import MinMaxScaler
        global x_scaler
        global y_scaler
        x_scaler = MinMaxScaler(feature_range=(0, 1))
        y_scaler = MinMaxScaler(feature_range=(0, 1))
        y_train, y_test = pd.DataFrame(data[label].loc[training_start:training_end]),pd.DataFrame([data[label].loc[testing_start]])
        X_train, X_test = data.drop(label,1).loc[training_start:training_end], data.drop(label,1).loc[testing_start]    
        X_train_s=x_scaler.fit_transform(X_train)    
        X_test_s=x_scaler.transform([X_test])
        y_train_s=y_scaler.fit_transform(y_train)
        y_test_s=y_scaler.transform([y_test]) 

        global y_hat_lin
        y_hat_lin = linear_regression(X_train_s, y_train_s, X_test_s, y_test_s).round(decimals=1)
        
        global y_hat_lasso
        y_hat_lasso = lasso_regression(X_train_s, y_train_s, X_test_s, y_test_s).round(decimals=1)

        global y_hat_svr
        y_hat_svr = SVR(X_train_s, y_train_s, X_test_s, y_test_s).round(decimals=1)
        
        ts.loc[testing_start]={'Linear Regression':y_hat_lin[0][0], 'Lasso Regression':y_hat_lasso[0][0], 'Support Vector Regression':y_hat_svr[0][0]}
        arima_res = ts.apply(lambda x: arima(x,16))
        arima_res.index = pd.date_range(start = months_delta(1,start),periods = 16,freq = 'QS')
        arima_res = arima_res.apply(lambda x: x.round(decimals=1))
        ts = pd.concat([ts,arima_res])
        ts.loc['Error']= ts.apply(lambda x: (np.abs(x-data[label].loc[start:ts.index[-1]])/data[label].loc[start:ts.index[-1]]).mean())
        res_dict[start] = ts
    
    for time in pd.date_range(start = testing_start, end = months_delta(12,testing_start), freq = 'QS'):
        if time in data.index:
            train(data=data, start = time, label=label, res_dict=res_dict)
              
    return res_dict
      
def get_result_test(res_dict, date, data):
    """
    Get the prediction result, plot and error stored in the dictionary
    
    Parameters
    ----------
    res_dict: a dictionary to store the predictive result
    date: the date we want to predict
    data : raw data

    Returns
    -------
    prediction_result : DateFrame
        a pandas DateFrame containing result with specific date

    """    
    date = datetime.datetime.strptime(date, '%Y-%m-%d')
    svr = res_dict[date]['Support Vector Regression']
    start = svr.index[0]
    end = svr.index[-2]
    plt.figure(figsize = (12,6))
    plt.title('Rental Vacancy Prediction ('+ date.strftime('%Y-%m-%d') +'~'+end.strftime('%Y-%m-%d')+')', fontsize = 15)
    plt.xlabel('Time',fontsize = 15)
    plt.ylabel('Rental Vacancy Level',fontsize = 15)
    plt.plot(data.RV.loc[start:date],color = 'black', label='Historical Rental Vacancy')
    if end in data.index:
        plt.plot(data.RV.loc[date:end],color = 'blue', label='Future Rental Vacancy (only for comparison)')

    plt.plot(svr.loc[date:end],'ro-',label = 'Support Vector Regression')
    plt.legend(loc=0)
    plt.xticks(rotation=45)
    plt.grid(True)
    plt.show()
    print('The prediction error is {:.2f}%'.format(svr.iloc[-1]*100))
    print(svr.loc[date:end].resample('YS',how='mean'))
    return svr

def get_result_use(res_dict, date, data):
    """
    Get the prediction result, plot and error stored in the dictionary
    
    Parameters
    ----------
    res_dict: a dictionary to store the predictive result
    date: the date we want to predict
    data : raw data

    Returns
    -------
    prediction_result : DateFrame
        a pandas DateFrame containing result with specific date

    """    
    date = datetime.datetime.strptime(date, '%Y-%m-%d')
    svr = res_dict[date]['Support Vector Regression']
    end = svr.index[-2]
    #print('The prediction error is {:.2f}%'.format(svr.iloc[-1]*100))
    tmp_ = svr.loc[date:end].resample('YS',how='mean')
    return list(tmp_.values)



def get_rental_vacancy_test(prediction_periods):
    """
    Get the prediction result, plot and error stored in the dictionary
    
    Parameters
    ----------
    date: the date we want to predict
    data : raw data

    Returns
    -------
    prediction_result : DateFrame
        a pandas DateFrame containing result with specific date

    """
    data = data_scrapping(symbol, var)
    date = str(data.index[-1])[:10]
    res_dict = train_model(data, start = date, prediction_periods = prediction_periods, label = 'RV') 
    prediction_result = get_result_test(res_dict,date,data)
    
    return prediction_result

def get_rental_vacancy_use(prediction_periods):
    """
    Get the prediction result, plot and error stored in the dictionary
    
    Parameters
    ----------
    date: the date we want to predict
    data : raw data

    Returns
    -------
    prediction_result : DateFrame
        a pandas DateFrame containing result with specific date

    """
    data = data_scrapping(symbol, var)
    date = str(data.index[-1])[:10]
    res_dict = train_model(data, start = date, prediction_periods = prediction_periods, label = 'RV') 
    prediction_result = get_result_use(res_dict,date,data)
    
    return prediction_result

# The latest date to input is 2019-01-01
#get_rental_vacancy_test(prediction_periods=4)
#vacancy_rates = get_rental_vacancy_use(prediction_periods=4)

    


