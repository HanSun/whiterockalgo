# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
def main(city='philadelphia',state='pa',start_page=1,end_page=5):
    import pandas as pd
    from bs4 import BeautifulSoup
    import urllib as ulb
    import random
    import time
    
    def get_link_content(url):
        ##Decorate the request with header and proxy
        my_headers=["Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)"]
        headers = [('Host','https://hotpads.com/'),('Connection', 'keep-alive'),('Cache-Control', 'max-age=0'),
               ('User-Agent',random.choice(my_headers))]
        ##Please note the the size of proxy IPs may affect the performance,if the IP pools are small, may raise 403 Error (Server Forbidden)
        proxy_list = '138.197.222.35,36.89.65.253,105.19.59.192,109.110.73.106,176.9.75.42,200.89.178.63,138.68.161.14,188.226.141.61,139.59.109.156'.split(',')
        ##Add timer to sleep for 1 sec,in order to avoid getting blocked
        t = 1
        time.sleep(t)
        ##Decorate the url request with Header and Proxy IP
        proxy = random.choice(proxy_list)

        urlhandle = ulb.request.ProxyHandler({'http': proxy})
        opener = ulb.request.build_opener(urlhandle)
        opener.addheaders = headers
        ulb.request.install_opener(opener)
        response = ulb.request.Request(url)
        
        fp = ulb.request.urlopen(response)
        html = fp.read()
        fp.close
        content = BeautifulSoup(html,"lxml")
        return content
    
    def get_content(url):
        content = get_link_content(url)
        ## Store all the listing information as a list type
        listing_detail = []
        ##Check if the listing is a apartment building or a single room
        if content.find('div',class_="ModelFloorplanItem-card") == None:
            address = content.find('h1',class_="Text HdpAddress-normal-weight-text Utils-text-overflow Text-sm Text-xlAndUp-md").get_text(" ")
            street = ' '.join(address.split(' ')[:-3])
            city = ''.join(address.split(' ')[-3]).strip(',')
            zipcode = address.split(' ')[-1]
            state = ''.join(address.split(' ')[-2]).strip(',')
            condo = content.find('h2',class_="Text PropertyTypeIcon-keyword Utils-accent-dark Text-sm Text-xlAndUp-md").get_text()
            lowprice,highprice = content.find('span',class_="Text Text-lg").get_text()
            layout = [item.get_text() for item in content.find_all('span',class_="Text BedsBathsSqft-text Text-sm Text-xlAndUp-md")]
            beds = layout[0].split(' ')[0]
            baths = layout[1].split(' ')[0]
            sqt = layout[2].split(' ')[0]
            listing_detail.append([street,city,state,zipcode,baths,beds,sqt,lowprice,highprice,condo])
        else:
            for info in content.find_all('div',class_="ModelFloorplanItem-card"):
                address = content.find('address').get_text(" ")
                street = ' '.join(address.split(' ')[:-3])
                city = ''.join(address.split(' ')[-3]).strip(',')
                zipcode = address.split(' ')[-1]
                state = ''.join(address.split(' ')[-2]).strip(',')
                condo = content.find('h2',class_="Text PropertyTypeIcon-keyword Utils-accent-dark Text-sm Text-xlAndUp-md").get_text()
                beds = info.find('span',class_="ModelFloorplanItem-detail").get_text().split(' ')[0]
                baths = [layout.get_text() for layout in info.find_all('span',class_="ModelFloorplanItem-bthsqft")][0].split(' ')[0]
                sqt = [layout.get_text() for layout in info.find_all('span',class_="ModelFloorplanItem-bthsqft")][1].split(' ')[0]
            
            ##Check if the apartment building type is empty or not (The empty type does not have uni number)
                if info.find('div',class_="ModelFloorplanItem-unit") != None:
                    for subinfo in info.find_all('div',class_="ModelFloorplanItem-unit"):
                        price = subinfo.find('div',class_="ModelFloorplanItem-unit-price").get_text().split()
                        ##Check if unit has low and high rent or single rent
                        if len(price) >= 3:
                            lowprice,highprice = price[0],price[-1]
                        else:
                            lowprice,highprice = price[0]
                        uninum = subinfo.find('div',class_="ModelFloorplanItem-unit-name").get_text()
                        listing_detail.append([street,city,state,zipcode,uninum,baths,beds,sqt,lowprice,highprice,condo])
                elif info.find('div',class_="ModelFloorplanItem-empty-unit") != None:
                    for subinfo in info.find_all('div',class_="ModelFloorplanItem-empty-unit"):
                        price = subinfo.find('div',class_="ModelFloorplanItem-empty-unit-price").get_text().split()
                        if len(price) >= 3:
                            lowprice,highprice = price[0],price[-1]
                        else:
                            lowprice,highprice = price[0]
                        listing_detail.append([street,city,state,zipcode,baths,beds,sqt,lowprice,highprice,condo])
        return listing_detail
    
    def get_df(content_list,save_to_excel=False):
        ##Store all the listing info to a dataframe, if choose to save as Excel Spread Sheet, pass True
        df = pd.DataFrame()
        for i in range(len(content_list)-1):
            temp_df = pd.concat([pd.DataFrame(content_list[i]),pd.DataFrame(content_list[i+1])],axis=0, ignore_index=True)
            df = pd.concat([df,temp_df],axis=0, ignore_index=True)
        df.rename(columns={0:'ADDRESS',1:'CITY',2:'STATE',3:'ZIP',4:'APT #',5:'BATHROOMS',6:'BEDROOMS',
                       7:'SF',8:'LOW RENT',9:'HIGH RENT',10:'BUILDING TYPE'},inplace=True)
        if not save_to_excel:
            df.to_excel("Phil_demo_data.xlsx")
        return df
    
    ##test the function
    url_list = ['https://hotpads.com/{}-{}/apartments-for-rent?page='.format(city,state)+str(i) for i in range(start_page,end_page+1)]
    listing_link = []
    for url in url_list:
        content = get_link_content(url)
        for listing in content.find_all('div',class_='ListingWrapper'):
            listing_link.append('https://hotpads.com'+listing.find('a')['href'])
    
    content_list = []
    for url in listing_link[:20]:
        content_list.append(get_content(url))

    df = get_df(content_list,save_to_excel = True)
    
    return df