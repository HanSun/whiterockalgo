# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 20:34:35 2019

@author: Quan
"""

#%%
# libraries
import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np

#%%
# filter links
def check_link(links):
    listing_link = []
    page_link = []
    for ele in links:
        if ele[:9] == '/listing/' :
            listing_link.append(ele)
        if ele[:10] == '/for-rent/' :
            page_link.append(ele)
    return listing_link, page_link

#%%
# create the request links
def new_link(list_):
    List = []
    for ele in list_:
        ele_ = "https://www.compass.com" + ele
        List.append(ele_)
    return List

#%%
# create the soup
def Soup(link):
    result = requests.get(link)
    source = result.content
    soup = BeautifulSoup(source,'lxml')
    return soup


#%%
# scrap features

# address with zipcode
def Address(soup):
    addr1 = soup.select('p.kFKTKg.summary__StyledAddress-e4c4ok-6.textIntent-title1')[0]
    address1 = addr1.text
    addr2 = soup.select('div.jrSEM.summary__StyledAddressCaption-e4c4ok-7.textIntent-caption2.summary__SummaryCaption-e4c4ok-5')[0]
    address2 = addr2.text
    address = address1 + ' , ' + address2
    return address

# year built
def Built_Year(soup):
    for i in range(len(soup.select('td.dXbawG.data-table__TableStyledTd-ibnf7p-1'))):
        if (soup.select('td.dXbawG.data-table__TableStyledTd-ibnf7p-1')[i]).text == 'Year Built' :
            cnt = int(i/2)
    try:
        year = soup.select('td.dXbawG.data-table__TableStyledTd-ibnf7p-1.textIntent-caption1--strong')[cnt]
        built_year = year.text
    except:
        built_year = np.nan
    return built_year

# county
def County(soup):
    cty = soup.select('td.dsmUMm.data-table__TableStyledTd-ibnf7p-1.textIntent-caption1--strong')[0]
    county = cty.text
    return county

# price
def Price(soup):
    pri = soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')[0]
    price = (pri.text).replace('Price','')
    return price

# bath
def Beds(soup):
    bds = soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')[1]
    beds = (bds.text).replace('Beds','')
    return beds

# baths
def Baths(soup):
    if len(soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')) == 1 :
        baths = 0
    elif len(soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')) == 2 :
        baths = 0
    else:
        bths = soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')[2]
        if 'Bath' in bths.text:
            baths = (bths.text).replace('Bath','')
        if 'Baths' in bths.text:
            baths = (bths.text).replace('Baths','')
    return baths

# half bath
def Half_Bath(soup):
    if len(soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')) == 1 :
        half_bath = 0
    elif len(soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')) == 2 :
        half_bath = 0
    elif len(soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')) == 3 :
        half_bath = 0
    else:
        hbth = soup.select('div.bgfBKu.summary__StyledSummaryDetailUnit-e4c4ok-13')[3]
        if '1/2 Bath' in hbth.text:
            half_bath = (hbth.text).replace('1/2 Bath','')
        if '1/2 Baths' in hbth.text:
            half_bath = (hbth.text).replace('1/2 Baths','')     
    return half_bath

# Sq. Ft
def SQFT(soup):
    area = soup.select('div.bAxgXF.custom-ranges-hide__CustomRangesHide-sc-19a3hp9-0.sc-bdVaJa')[0]
    sqft = (area.text).split('Sq.')[0]
    return sqft


#%%

# create one dict to store features
    
def Dict(soup):
    keys = ['address_with_zipcode','year_built','county','price','beds','baths','1/2_bath','sqft']
    address = Address(soup)
    year_built = Built_Year(soup)
    county = County(soup)
    price = Price(soup)
    beds = Beds(soup)
    baths = Baths(soup)
    half_bath = Half_Bath(soup)
    sqft = SQFT(soup)
    values = [address,year_built,county,price,beds,baths,half_bath,sqft]
    Dict = dict(zip(keys, values))
    return Dict


#%%

def get_compass():
    website_link = 'https://www.compass.com/for-rent/philadelphia-pa/'
    
    # create the dataframe
    df = pd.DataFrame(columns = ['address_with_zipcode','year_built','county','price','beds','baths','1/2_bath','sqft'])
    
    Web = website_link + 'start='
    for i in range(111):
        soup = Soup(Web + str(20*i))
        print(i)
        links = soup.find_all('a')
        links_ = []
        for e in links:
            links_.append(e.get('href'))
        listing_link, page_link = check_link(links_)
        Listing_link = new_link(listing_link)
        for ele in Listing_link:
            #print(ele)
            soup_ = Soup(ele)
            info = Dict(soup_)
            df= df.append(info , ignore_index=True)
    
    df.to_csv(r'D:/PHL/Compass.csv',index = None)
    
    return df




