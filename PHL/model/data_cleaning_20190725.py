# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 20:49:38 2019

@author: Administrator
"""

import numpy as np
import pandas as pd

path = 'C:/Users/Administrator/Desktop/data_need_clean/'
rent_master = pd.read_csv(path+'NMA-001 Resi_Rent_Master [bylocation_address].csv')
sale_master = pd.read_csv(path+'NMA-002 Resi_Sales_Master [bylocation;addresses].csv')
formatting = pd.read_excel(path+'Building_Categories_Format.xlsx')
formatting = formatting.set_index(list(formatting.columns[1:]))

#%% BUILDING CLASS CLEAN
rent_master['< 5 UNITS'] = 0
rent_master['6 - 10 UNITS'] = 0
rent_master['11 - 20 UNITS'] = 0
rent_master['21 - 50 UNITS'] = 0
rent_master['51 - 100 UNITS'] = 0
rent_master['> 100 UNITS'] = 0
rent_master['ELEVATOR'] = 0
rent_master['WALK UP'] = 0
rent_master['MIXED USE'] = 0
rent_master['BLDG CLASS'] = 0

for i in range(len(rent_master.index)):
    flag = 0
    if np.isnan(rent_master.loc[i, '# UNITS']):
        rent_master.loc[i,'< 5 UNITS'] = np.nan
        rent_master.loc[i,'6 - 10 UNITS'] = np.nan
        rent_master.loc[i,'11 - 20 UNITS'] = np.nan
        rent_master.loc[i,'21 - 50 UNITS'] = np.nan
        rent_master.loc[i,'51 - 100 UNITS'] = np.nan
        rent_master.loc[i,'> 100 UNITS'] = np.nan
        
        flag = 1
    
    elif rent_master.loc[i, '# UNITS'] < 6:
        rent_master.loc[i,'< 5 UNITS'] = 1
        rent_master.loc[i,'6 - 10 UNITS'] = 0
        rent_master.loc[i,'11 - 20 UNITS'] = 0
        rent_master.loc[i,'21 - 50 UNITS'] = 0
        rent_master.loc[i,'51 - 100 UNITS'] = 0
        rent_master.loc[i,'> 100 UNITS'] = 0
        
    elif rent_master.loc[i, '# UNITS'] < 11:
        rent_master.loc[i,'< 5 UNITS'] = 0
        rent_master.loc[i,'6 - 10 UNITS'] = 1
        rent_master.loc[i,'11 - 20 UNITS'] = 0
        rent_master.loc[i,'21 - 50 UNITS'] = 0
        rent_master.loc[i,'51 - 100 UNITS'] = 0
        rent_master.loc[i,'> 100 UNITS'] = 0
        
    elif rent_master.loc[i, '# UNITS'] < 21:
        rent_master.loc[i,'< 5 UNITS'] = 0
        rent_master.loc[i,'6 - 10 UNITS'] = 0
        rent_master.loc[i,'11 - 20 UNITS'] = 1
        rent_master.loc[i,'21 - 50 UNITS'] = 0
        rent_master.loc[i,'51 - 100 UNITS'] = 0
        rent_master.loc[i,'> 100 UNITS'] = 0
        
    elif rent_master.loc[i, '# UNITS'] < 51:
        rent_master.loc[i,'< 5 UNITS'] = 0
        rent_master.loc[i,'6 - 10 UNITS'] = 0
        rent_master.loc[i,'11 - 20 UNITS'] = 0
        rent_master.loc[i,'21 - 50 UNITS'] = 1
        rent_master.loc[i,'51 - 100 UNITS'] = 0
        rent_master.loc[i,'> 100 UNITS'] = 0
        
    elif rent_master.loc[i, '# UNITS'] < 101:
        rent_master.loc[i,'< 5 UNITS'] = 0
        rent_master.loc[i,'6 - 10 UNITS'] = 0
        rent_master.loc[i,'11 - 20 UNITS'] = 0
        rent_master.loc[i,'21 - 50 UNITS'] = 0
        rent_master.loc[i,'51 - 100 UNITS'] = 1
        rent_master.loc[i,'> 100 UNITS'] = 0
    else:
        rent_master.loc[i,'< 5 UNITS'] = 0
        rent_master.loc[i,'6 - 10 UNITS'] = 0
        rent_master.loc[i,'11 - 20 UNITS'] = 0
        rent_master.loc[i,'21 - 50 UNITS'] = 0
        rent_master.loc[i,'51 - 100 UNITS'] = 0
        rent_master.loc[i,'> 100 UNITS'] = 1
    
    if np.isnan(rent_master.loc[i, 'ELEVATOR (1=yes)']):
        rent_master.loc[i,'ELEVATOR'] = np.nan
        rent_master.loc[i,'WALK UP'] = np.nan
        
        flag = 1
    elif rent_master.loc[i, 'ELEVATOR (1=yes)'] == 1:
        rent_master.loc[i,'ELEVATOR'] = 1
        rent_master.loc[i,'WALK UP'] = 0
    else:
        rent_master.loc[i,'ELEVATOR'] = 0
        rent_master.loc[i,'WALK UP'] = 1
        
    if np.isnan(rent_master.loc[i, 'COMM (1=yes)']):
        rent_master.loc[i,'MIXED USE'] = np.nan
        
        flag = 1
    elif rent_master.loc[i, 'COMM (1=yes)'] == 1:
        rent_master.loc[i,'MIXED USE'] = 1
    else:
        rent_master.loc[i,'MIXED USE'] = 0
        
    if flag == 1:
        rent_master.loc[i,'BLDG CLASS'] = np.nan
    else:
        index_ = []
        for j in ['< 5 UNITS','6 - 10 UNITS','11 - 20 UNITS','21 - 50 UNITS','51 - 100 UNITS','> 100 UNITS','ELEVATOR','WALK UP','MIXED USE']:
            index_.append(rent_master.loc[i,j])
        index_ = tuple(index_)
        class_ = 'W'+str(formatting.loc[index_,:].values[0])
        rent_master.loc[i,'BLDG CLASS'] = class_
    
rent_master = rent_master[['ADDRESS', 'APT#', 'BATHROOMS', 'BEDROOMS', 'NEIGHBORHOOD', 'RENT',
       'RENT PER BED', 'RENT PSF', 'SF', 'GSF', '# UNITS', '# FLOORS',
       'YEAR BUILT', 'ZIP', 'LAND SF', 'BOROUGH', 'BLDG CLASS NOW',
       'RETAIL SF', 'CODE', 'DEFINITION', '< 5 UNITS', '6 - 10 UNITS',
       '11 - 20 UNITS', '21 - 50 UNITS', '51 - 100 UNITS', '> 100 UNITS',
       'ELEVATOR', 'WALK UP', 'MIXED USE', 'BLDG CLASS']]

    
    
#%% SALE CLEAN
def building_class(row):
    index_ = []
    print(row)
    for j in ['< 5 UNITS','6 - 10 UNITS','11 - 20 UNITS','21 - 50 UNITS','51 - 100 UNITS','> 100 UNITS','ELEVATOR','WALK UP','MIXED USE']:
        index_.append(row.loc[j])
    index_ = tuple(index_)
    try:
        class_ = 'W'+str(formatting.loc[index_,:].values[0])
        return class_
    except:
        return np.nan
    
sale_master['BLDG CLASS'] = sale_master.apply(building_class, axis=1)

sale_master = sale_master.dropna(subset=['ADDRESS'])

#%%
import lycleaner

sale_master[['ADDRESS']] = sale_master[['ADDRESS']].astype(str)
cleaner = lycleaner.Address_cleaner()
#cleaner.strip_space(sale_master['ADDRESS'])
sale_master['ADDRESS'] = cleaner.easy_clean(sale_master['ADDRESS'])
    
#%%
def try_float(x):
    try:
        float(x)
        return True
    except:
        return False

sale_master = sale_master.loc[sale_master['SALE PRICE'].apply(try_float)]
sale_master[['SALE PRICE']] = sale_master[['SALE PRICE']].astype(float)

#%%
sale_master[['SALE DATE']] = sale_master[['SALE DATE']].astype(str)
def date_process(x):
    xlist = x.split('/')
    return xlist[2]+'-'+xlist[0]+'-'+xlist[1]

sale_master['SALE DATE'] = sale_master['SALE DATE'].apply(date_process)
sale_master = sale_master.dropna(subset=['YEAR BUILT','GSF','LAND SF'])
sale_master[['YEAR BUILT']] = sale_master[['YEAR BUILT']].astype(int)
sale_master[['GSF']] = sale_master[['GSF']].astype(int)
sale_master[['LAND SF']] = sale_master[['LAND SF']].astype(int)

sale_master = sale_master[sale_master['YEAR BUILT'] > 0]
sale_master = sale_master[sale_master['LAND SF'] > 0]
sale_master = sale_master[sale_master['GSF'] > 0]

sale_master = sale_master.dropna(subset=['ZIP CODE','# UNITS'])
sale_master[['# UNITS']] = sale_master[['# UNITS']].astype(int)
sale_master = sale_master[sale_master['# UNITS'] > 0]

#%%
rent_master.to_csv(path+'new_rent.csv')
sale_master.to_csv(path+'new_sale.csv')

#%% clean pluto and coross with pluto
pluto = pd.read_csv(path+'new_pluto.csv')
sale_master = pd.merge(sale_master,pluto[['ADDRESS','# FLOORS']],on='ADDRESS', how='left')
sale_master = sale_master[['ADDRESS', 'BOROUGH', 'NEIGHBORHOOD', 'TAX CLASS', 'BLOCK', 'LOT',
       'ZIP CODE', 'RESI UNITS', 'COMM UNITS', '# UNITS', '# FLOORS', 'LAND SF', 'GSF',
       'YEAR BUILT', 'TAX CLASS SALE', 'CITY CLASS', 'CLASS SALE',
       'CLASS CHANGE', 'SALE PRICE', 'SALE DATE', 'COMM', 'ELEVATOR',
       'WALK UP', '< 5 UNITS', '6 - 10 UNITS', '11 - 20 UNITS',
       '21 - 50 UNITS', '51 - 100 UNITS', '> 100 UNITS', 'MIXED USE',
       'BLDG CLASS']]

sale_master.to_csv(path+'new_new_sale.csv')

#%%
sale_master = pd.read_csv(path+'new_new_sale.csv', index_col=0)
rent_master = pd.read_csv(path+'new_rent.csv',index_col=0)

rent_master = rent_master.dropna(subset=['ZIP'])
rent_master[['ZIP']] = rent_master[['ZIP']].astype(int)
rent_master = rent_master[rent_master['ZIP'] != '0']

sale_master.columns = ['ADDRESS', 'BOROUGH', 'NEIGHBORHOOD', 'TAX CLASS', 'BLOCK', 'LOT',
       'ZIP', 'RESI UNITS', 'COMM UNITS', '# UNITS', '# FLOORS',
       'LAND SF', 'GSF', 'YEAR BUILT', 'TAX CLASS SALE', 'CITY CLASS',
       'CLASS SALE', 'CLASS CHANGE', 'SALE PRICE', 'SALE DATE', 'COMM',
       'ELEVATOR', 'WALK UP', '< 5 UNITS', '6 - 10 UNITS', '11 - 20 UNITS',
       '21 - 50 UNITS', '51 - 100 UNITS', '> 100 UNITS', 'MIXED USE',
       'BLDG CLASS']

#%% pluto - stab
stab = pd.read_csv(path+'N-102 Stabilized_Buildings [bylocation;address] DHCR.csv')
stab['STAB'] = 1
#stab[['STAB']] = stab[['BLOCK']].astype(int)

df = pd.merge(pluto,stab[['BLOCK','LOT','STAB']],on=['BLOCK','LOT'], how='left')
rent_master = pd.merge(rent_master,stab[['ADDRESS','STAB']],on='ADDRESS', how='left')
rent_master['STAB'] = np.nan_to_num(rent_master['STAB'].values)

sale_master = pd.merge(sale_master,stab[['ADDRESS','STAB']],on='ADDRESS', how='left')
sale_master['STAB'] = np.nan_to_num(sale_master['STAB'].values)

rent_master.to_csv(path+'new_rent_stab.csv')
sale_master.to_csv(path+'new_sale_stab.csv')

#%%
sale_master = pd.read_csv(path+'new_sale_stab.csv', index_col=0)
rent_master = pd.read_csv(path+'new_rent_stab.csv',index_col=0)
pluto = pd.read_csv(path+'new_pluto.csv')

geo_data = pluto[['ADDRESS','LONGITUDE','LATITUDE','ZIP','bank', 'bus_station', 'gym', 'hospital', 'park',
       'restaurant', 'school', 'shopping_mall', 'subway_station']]

rent_master = pd.merge(rent_master,geo_data[['ADDRESS','ZIP','LATITUDE','bank', 'bus_station', 'gym', 'hospital', 'park',
       'restaurant', 'school', 'shopping_mall', 'subway_station']],on=['ADDRESS','ZIP'], how='left')

sale_master = pd.merge(sale_master,geo_data[['ADDRESS','ZIP','LATITUDE','bank', 'bus_station', 'gym', 'hospital', 'park',
       'restaurant', 'school', 'shopping_mall', 'subway_station']],on=['ADDRESS','ZIP'], how='left')

rent_master.to_csv(path+'new_rent_stab_geo.csv')
sale_master.to_csv(path+'new_sale_stab_geo.csv')

used_columns = ['ZIP','LAND SF','GSF','# UNITS','# FLOORS','YEAR BUILT',
                '< 5 UNITS', '6 - 10 UNITS','11 - 20 UNITS','21 - 50 UNITS',
                '51 - 100 UNITS', '> 100 UNITS','ELEVATOR','WALK UP','MIXED USE','STAB',
                'bank', 'bus_station', 'gym', 'hospital', 'park', 'restaurant', 'school', 'shopping_mall', 'subway_station','RENT PSF']
df = df[used_columns]
df = df.dropna()
df.index = range(0,len(df.index))


#%%
super_df = pd.read_csv(path+'NY_super_file.csv',index_col = 0)

#%%
pluto = pd.read_csv(path+'new_pluto_stab_geo.csv', index_col = 0)

def cal_tax(x):
    if x['# UNITS'] <= 3:
        rate = 0.20919
    else:
        rate = 0.12612
    
    return rate*(x['assesstot']-x['exempttot'])

pluto['TAX'] = pluto.apply(cal_tax, axis=1)

#%%
rent_master_1 = pd.merge(rent_master,geo_data[['ADDRESS','ZIP','LATITUDE','LONGITUDE']],on=['ADDRESS','ZIP','LATITUDE'], how='left')
rent_master_1.to_csv(path+'new_rent_stab_geo.csv')

#%%
super_data = pd.read_csv(path+'NY_super_file 2019-08-02.csv', index_col = 0)

#%%
rent_master_unique = pd.read_csv(path+'NMA-001 Rent_Master_Unique [bylocation;addresses].csv', index_col = 0)
rent_master        = pd.read_csv(path+'NMA-001 Rent_Master [bylocation;addresses].csv', index_col = 0)

















