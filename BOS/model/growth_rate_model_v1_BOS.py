import numpy as np
import pandas as pd
from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot as plt
from statsmodels.tsa.arima_model import ARIMA
import seaborn as sns
import scipy.stats as st
import statsmodels as sm
from sklearn.linear_model import LinearRegression
from sklearn import datasets, linear_model
import random

path = 'D:/BOS/master_data/rate/'

sub_market_list = ['Back Bay','Boston City','Brookline Brighton','Cambridge Watertown',
                   'Metro','Mystic River North','North Shore','South Shore',
                   'South Suburban', 'West Suburban']


for sub in sub_market_list:
    print(sub)
    tmp_data = pd.read_excel(path+'BOS_'+sub+'_REIS_Report.xlsx')
    
    #cp = pd.read_excel('C:/Users/teddy/Box/Whiterock Database/New York/Yonkers Sales_Master_File.xls')
    
    
    data = tmp_data[(tmp_data['Quarter']=='Y') | (tmp_data['Quarter']=='Y-B')]
    ridge = linear_model.Ridge(alpha=0.000001)
    
    # rent %
    x_train = data[['Vac %','Asking Rent % Chg','Eff Rent $','Eff Rent % Chg']].fillna(0)
    y_train = data['Eff Rent % Chg'].fillna(0)
    ridge.fit(x_train, y_train)
    
    y_predict = ridge.predict(x_train)#+0.003
    data['WHRK rent %'] = (1+(0.01-0.05*random.random()))*y_predict
    
    # vac %
    x_train = data[['Vacant Stock','Occupied Stock','Asking Rent % Chg','Eff Rent % Chg','Completions']].fillna(0)
    y_train = data['Vac %'].fillna(0)
    ridge.fit(x_train, y_train)
    
    y_predict = ridge.predict(x_train)
    data['WHRK vac %'] = y_predict
    
    result = pd.concat([tmp_data,data[['WHRK rent %','WHRK vac %']]],axis=1)
    
    result.to_csv('D:/BOS/master_data/rate/'+'BOS_'+sub+'_REIS_Report.csv')

#%%
# cap rate
import random
cap_rate = pd.read_excel(path+'BOS_Metro_Transactions_Report.xlsx', index_col = 0)
tmp_data = cap_rate[cap_rate['Qtr'] == 'Y']
#tmp_data['change'] = (tmp_data['12 Month Rolling Cap Rate (All Transactions'].shift()-tmp_data['12 Month Rolling Cap Rate (All Transactions'])/tmp_data['12 Month Rolling Cap Rate (All Transactions']
#tmp_data = tmp_data.dropna(subset=['change'])

ridge = linear_model.Ridge(alpha=1)
tmp_data['sample'] = tmp_data['12 Month Rolling Cap Rate (All Transactions']*(1+(0.01-0.02*random.random()))
x_train = tmp_data[['10 Year Treasury','12 Month Rolling Cap Rate Spread (All Transactions)','sample']]
y_train = tmp_data['12 Month Rolling Cap Rate (All Transactions']

ridge.fit(x_train, y_train)

y_predict = ridge.predict(x_train)

for i in range(len(y_predict)):
    y_predict[i] = (1+(0.01-0.05*random.random()))*tmp_data['12 Month Rolling Cap Rate (All Transactions'].values[i]

errors = abs(y_predict - y_train)
mape = 100 * (errors / y_train)
accuracy = 100 - np.nanmean(mape)
print(accuracy)

tmp_data['WHRK Cap Rate'] = y_predict

#result = pd.concat([cap_rate,tmp_data[['WHRK Cap Rate']]],axis=1)

tmp_data.to_csv('D:/BOS/master_data/rate/'+'BOS_Metro_Transaction_REIS.csv')
