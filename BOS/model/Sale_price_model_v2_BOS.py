# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 22:08:08 2019

@author: teddy
"""

import numpy as np
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor,AdaBoostRegressor,BaggingRegressor,GradientBoostingRegressor
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import ElasticNet
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

import statsmodels.api as sm
from scipy import stats
import pickle
import copy

import warnings
warnings.filterwarnings("ignore")

#%%
def year_process(j):
    if j < 1900:
        return 1
    elif 1900<=j <1910:
        return 2
    elif 1910<=j<1920:
        return 3
    elif 1920<=j <1930:
        return 4
    elif 1930<=j<1940:
        return 5
    elif 1960<=j <1970:
        return 8
    elif 1970<=j<1980:
        return 9
    elif 1980<=j <1990:
        return 10
    elif 1990<=j<2000:
        return 11
    elif 2000<=j<2010:
        return 12
    else:
        return 13

def data_processing(data):
    #data_copy = copy.deepcopy(data)

    # process with date and yaer
    #data[['ZIP']] = data[['ZIP']].astype(int)
    data[['GSF']] = data[['GSF']].astype(int)
    data[['LAND SF']] = data[['LAND SF']].astype(int)
    #data[['# UNITS']] = data[['# UNITS']].astype(int)
    data[['# FLOORS']] = data[['# FLOORS']].astype(int)
    
    #data = data[data['ZIP'] > 10000]
    data = data[data['GSF'] > 0]
    data = data[data['LAND SF'] > 0]
    #data = data[data['# UNITS'] > 0]
    data = data[data['# FLOORS'] > 0]
    
    #data[['ZIP']] = data[['ZIP']].astype(str)
    
    bins = [-np.inf,1900, 1910, 1920, 1930, 1940, 1950, 1960, 1970, 1980, 1990, 2000, 2010, np.inf]
    names = ['1', '2', '3', '4', '5','6','7','8','9','10','11','12','13']
    
    try:
        data['YEAR BUILT'] = pd.cut(data['YEAR BUILT'], bins, labels=names)
    except:
        pass
    '''
    upper_y = np.mean(data['RENT PSF']) + 3 * np.std(data['RENT PSF'])
    lower_y = max(0, np.mean(data['RENT PSF']) - 3 * np.std(data['RENT PSF']))
    #upper_sf = np.mean(data['SF']) + 3 * np.std(data['SF'])
    #lower_sf = max(0, np.mean(data['SF']) - 3 * np.std(data['SF']))
    upper_gsf = np.mean(data['GSF']) + 3 * np.std(data['GSF'])
    lower_gsf = max(0, np.mean(data['GSF']) - 3 * np.std(data['GSF']))
    upper_units = np.mean(data['# UNITS']) + 3 * np.std(data['# UNITS'])
    lower_units = max(0, np.mean(data['# UNITS']) - 3 * np.std(data['# UNITS']))
    upper_land = np.mean(data['LAND SF']) + 3 * np.std(data['LAND SF'])
    lower_land = max(0, np.mean(data['LAND SF']) - 3 * np.std(data['LAND SF']))
    
    data = data[data['RENT PSF']<=upper_y]
    data = data[data['RENT PSF']>=lower_y]
    #data = data[data['SF']<=upper_sf]
    #data = data[data['SF']>=lower_sf]
    data = data[data['GSF']<=upper_gsf]
    data = data[data['GSF']>=lower_gsf]
    data = data[data['# UNITS']<=upper_units]
    data = data[data['# UNITS']>=lower_units]
    data = data[data['LAND SF']<=upper_land]
    data = data[data['LAND SF']>=lower_land]
    
    data[['< 5 UNITS']] = data[['< 5 UNITS']].astype(int)
    data[['6 - 10 UNITS']] = data[['6 - 10 UNITS']].astype(int)
    data[['11 - 20 UNITS']] = data[['11 - 20 UNITS']].astype(int)
    data[['21 - 50 UNITS']] = data[['21 - 50 UNITS']].astype(int)
    data[['51 - 100 UNITS']] = data[['51 - 100 UNITS']].astype(int)
    data[['> 100 UNITS']] = data[['> 100 UNITS']].astype(int)
    data[['ELEVATOR']] = data[['ELEVATOR']].astype(int)
    #data[['WALK UP']] = data[['WALK UP']].astype(int)
    data[['MIXED USE']] = data[['MIXED USE']].astype(int)
    data[['STAB']] = data[['STAB']].astype(int)
    data[['bank']] = data[['bank']].astype(int)
    data[['bus_station']] = data[['bus_station']].astype(int)
    data[['gym']] = data[['gym']].astype(int)
    data[['hospital']] = data[['hospital']].astype(int)
    data[['park']] = data[['park']].astype(int)
    data[['restaurant']] = data[['restaurant']].astype(int)
    data[['school']] = data[['school']].astype(int)
    data[['shopping_mall']] = data[['shopping_mall']].astype(int)
    data[['subway_station']] = data[['subway_station']].astype(int)
    '''
    data[['# FIREPLACE']] = data[['# FIREPLACE']].astype(int)
    data[['LOT FRONTAGE']] = data[['LOT FRONTAGE']].astype(float)
    data[['GARAGE']] = data[['GARAGE']].astype(int)
    data[['TOT ASSD $']] = data[['TOT ASSD $']].astype(int)
    data[['# BATH']] = data[['# BATH']].astype(int)
    data[['# BED']] = data[['# BED']].astype(int)
    #data[['VIEW']] = data[['VIEW']].astype(str)
    data[['SALE PRICE']] = data[['SALE PRICE']].astype(float)
    
    return data

def random_forest(x_train, y_train):
    print('random forest started!')
    rf = RandomForestRegressor(n_estimators = 1000, random_state = 42)
    rf.fit(x_train, y_train)
    
    print('    random forest done!')
    
    return rf

def ridge_regression(x_train, y_train):
    print('ridge regression started!')
    ridge = linear_model.Ridge(alpha=0.5)
    ridge.fit(x_train, y_train)
    
    print('    ridge regression done!')
    
    return ridge

def ada_boosting(x_train, y_train):
    print('adaboosting regression started!')
    ada = AdaBoostRegressor(n_estimators = 1000, random_state = 42)
    ada.fit(x_train, y_train)
    
    print('    adaboosting regression done!')
    
    return ada

def bagging(x_train, y_train):
    print('bagging regression started!')
    bag = BaggingRegressor(n_estimators = 1000, random_state = 42)
    bag.fit(x_train, y_train)
    
    print('    bagging regression done!')
    
    return bag

def gradient_boosting(x_train, y_train):
    print('gradient boosting regression started!')
    gradient = GradientBoostingRegressor(n_estimators = 1000, random_state = 42)
    gradient.fit(x_train, y_train)
    
    print('    gradient boosting regression done!')
    
    return gradient

def elastic_net(x_train, y_train):
    print('elastic net regression started!')
    enet = ElasticNet(alpha = 0.1, random_state=10, positive=True)
    enet.fit(x_train, y_train)
    
    print('    elastic net regression done!')
    
    return enet

def lasso_regression(x_train, y_train):
    print('lasso regression started!')
    lasso = linear_model.Lasso(alpha=0.5)
    lasso.fit(x_train, y_train)
    
    print('    lasso regression done!')
    
    return lasso

def ensemble_learning(data, methods, state, decorred_columns, if_test=False):
    sample_data = data[decorred_columns[:-1]]
    
    print(sample_data.columns)
    
    x_data = pd.get_dummies(sample_data)
    
    y_data = data['SALE PRICE']
    
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size = 1, random_state = state)
    
    rf_model = random_forest(x_train, y_train)
    #ridge_model = ridge_regression(x_train, y_train)
    #ada_model = ada_boosting(x_train, y_train)
    bagging_model = bagging(x_train, y_train)
    #gradient_model = gradient_boosting(x_train, y_train)
    #enet_model = elastic_net(x_train, y_train)
    
    #y_train_ensemble = pd.DataFrame(index=range(len(y_train)),columns=methods)
    y_train_rf = rf_model.predict(x_train)
    errors = abs(y_train_rf - y_train)
    print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')
    #y_train_ada = ada_model.predict(x_train)
    y_train_bagging = bagging_model.predict(x_train)
    errors = abs(y_train_bagging - y_train)
    print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')
    #y_train_gradient = gradient_model.predict(x_train)
    #errors = abs(y_train_gradient - y_train)
    #print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')
    #y_train_enet = enet_model.predict(x_train)
    #errors = abs(y_train_enet - y_train)
    #print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')
    #y_train_ridge = ridge_model.predict(x_train)
    #errors = abs(y_train_ridge - y_train)
    #print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')
    
    #x_train_ensemble = np.vstack((y_train_rf,y_train_bagging,y_train_gradient,y_train_enet,y_train_ridge)).transpose()
    x_train_ensemble = np.vstack((y_train_rf,y_train_bagging)).transpose()
    
    y_test_rf = rf_model.predict(x_test)
    #y_test_ada = ada_model.predict(x_test)
    y_test_bagging = bagging_model.predict(x_test)
    #y_test_gradient = gradient_model.predict(x_test)
    #y_test_enet = enet_model.predict(x_test)
    #y_test_ridge = ridge_model.predict(x_test)
    
    #x_test_ensemble = np.vstack((y_test_rf,y_test_bagging,y_test_gradient,y_test_enet,y_test_ridge)).transpose()
    x_test_ensemble = np.vstack((y_test_rf,y_test_bagging)).transpose()
    
    # stacking by lasso
    lasso_model = lasso_regression(x_train_ensemble, y_train)
    y_predict = lasso_model.predict(x_test_ensemble)
    y_predict_train = lasso_model.predict(x_train_ensemble)
    
    # evaluation
    errors = abs(y_predict - y_test)
    errors_insample = abs(y_predict_train - y_train)
    print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')

    mape = 100 * (errors / y_test)
    mape_insample = 100 * (errors_insample / y_train)
    accuracy = 100 - np.mean(mape)
    accuracy_insample = 100 - np.mean(mape_insample)
    print('Accuracy in-sample:', round(accuracy_insample, 2), '%.')
    print('Accuracy out-sample:', round(accuracy, 2), '%.')
    
    if if_test:
        while True:
            data_input = []
            for i in sample_data.columns:
                if i == 'NEIGHBORHOOD':
                    data_input.append(input(i+': '))
                elif i == 'YEAR BUILT':
                    data_input.append(str(year_process(input(i+': '))))
                else:
                    data_input.append(int(input(i+': ')))
                    
            tmp = pd.DataFrame(data_input, index=['test'], columns=sample_data.columns)
            tmp = pd.concat([sample_data,tmp],axis=0)
            test_tmp = pd.get_dummies(tmp)
            test_tmp = test_tmp.loc['test',:].values
            
            test_rf = rf_model.predict(test_tmp)
            #test_ada = ada_model.predict(test_tmp)
            test_bagging = bagging_model.predict(test_tmp)
            #test_gradient = gradient_model.predict(test_tmp)
            #test_stack = lasso_model.predict([test_rf,test_ada,test_bagging,test_gradient])
            
            #print('Estimated rent is: ' + str(test_stack*data_input[3]))
            
            #if_continue = input('More to test? (Y/N): ')
            #print('===================================')
            #if if_continue == False:
            #    break
    
    return y_predict, y_test, mape, mape_insample

def ensemble_learning_output(data, methods, state, decorred_columns):
    sample_data = data[decorred_columns[:-1]]
    
    x_data = pd.get_dummies(sample_data)
    
    y_data = data['SALE PRICE']
    
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size = 1, random_state = state)
    
    rf_model = random_forest(x_train, y_train)
    #ridge_model = ridge_regression(x_train, y_train)
    #ada_model = ada_boosting(x_train, y_train)
    bagging_model = bagging(x_train, y_train)
    #gradient_model = gradient_boosting(x_train, y_train)
    #enet_model = elastic_net(x_train, y_train)
    
    #y_train_ensemble = pd.DataFrame(index=range(len(y_train)),columns=methods)
    y_train_rf = rf_model.predict(x_train)
    #y_train_ada = ada_model.predict(x_train)
    y_train_bagging = bagging_model.predict(x_train)
    #y_train_gradient = gradient_model.predict(x_train)
    #y_train_enet = enet_model.predict(x_train)
    #y_train_ridge = ridge_model.predict(x_train)
    
    x_train_ensemble = np.vstack((y_train_rf,y_train_bagging)).transpose()
    
    y_test_rf = rf_model.predict(x_test)
    #y_test_ada = ada_model.predict(x_test)
    y_test_bagging = bagging_model.predict(x_test)
    #y_test_gradient = gradient_model.predict(x_test)
    #y_test_enet = enet_model.predict(x_test)
    #y_test_ridge = ridge_model.predict(x_test)
    
    x_test_ensemble = np.vstack((y_test_rf,y_test_bagging)).transpose()
    
    # stacking by lasso
    lasso_model = lasso_regression(x_train_ensemble, y_train)
    y_predict = lasso_model.predict(x_test_ensemble)
    y_predict_train = lasso_model.predict(x_train_ensemble)
    #y_predict = lasso_model.predict(x_test_ensemble)
    
    # evaluation
    #errors = abs(y_predict - y_test)
    #print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')

    #mape = 100 * (errors / y_test)
    #accuracy = 100 - np.mean(mape)
    #print('Accuracy:', round(accuracy, 2), '%.')
    
    return rf_model, bagging_model, lasso_model, sample_data

def k_means_cluster(data, k=15, rate = 0.5):
    x_data = pd.get_dummies(data[[ 'LAND SF','GSF','# FLOORS','YEAR BUILT','# FIREPLACE', 'LOT FRONTAGE', 'GARAGE', 'TOT ASSD $', '# BATH','# BED']])
    
    kmeans=KMeans(n_clusters=k)
    kmeans.fit(x_data)
    
    data_copy = copy.deepcopy(data)
    data_copy = pd.concat([data_copy,pd.DataFrame(kmeans.labels_,columns=['class'],index=range(0,len(data.index)))],axis=1)
    
    # mean and std for each group
    means = {}
    std = {}
    medians = {}
    '''
    for i in range(k):
        means[i] = data_copy.groupby(['class']).mean().loc[i,'SALE PRICE']
        std[i] = data_copy.groupby(['class']).std().loc[i,'SALE PRICE']
        medians[i] = data_copy.groupby(['class']).median().loc[i,'SALE PRICE']
    
    upper = {}
    lower = {}
    for i in range(k):
        upper[i] = min(means[i]+rate*std[i],means[i]+rate*medians[i])
        lower[i] = max(means[i]-rate*std[i],means[i]-rate*medians[i])
    '''
    keep_index = []
    # filter out the outpliers by MAD and sigma
    for i in range(0,len(data_copy.index)):
        class_ = data_copy.loc[i,'class']
        #if data_copy.loc[i,'SALE PRICE'] >= lower[class_] and data_copy.loc[i,'SALE PRICE'] <= upper[class_]:
        keep_index.append(i)
            
    new_data = data_copy.loc[keep_index, :]
    
    return new_data

def decorr(data, num_features = 10):
    sample_data = data[['LAND SF','GSF','# FLOORS','# FIREPLACE', 'LOT FRONTAGE', 'GARAGE', 'TOT ASSD $', '# BATH','# BED']]
    
    x_data = pd.get_dummies(sample_data)
    x_data = sample_data
    y_data = data['SALE PRICE'].astype('int')
    
    bestfeatures = SelectKBest(score_func=chi2, k=num_features)
    fit = bestfeatures.fit(x_data,y_data)
    dfscores = pd.DataFrame(fit.scores_)
    dfcolumns = pd.DataFrame(x_data.columns)
    
    featureScores = pd.concat([dfcolumns,dfscores],axis=1)
    
    featureScores.columns = ['Specs','Score']
    
    decorred_columns = list(featureScores.nlargest(num_features,'Score')['Specs'].values)+['SALE PRICE']
    
    return decorred_columns

#%% Import data
def model_test():
    methods = ['rf','ridge']
    
    path = 'C:/Users/Administrator/Box/Whiterock Database/NYC/Master Files - NMA/'
    path = 'D:/BOS/master_data/'
    data = pd.read_csv(path+'BOSPL-001 All_Properties (bylocation;address) PLUTO.csv')
    
    data = data[data['BLDG'] == 1]
    data = data.dropna(subset=['SALE DATE','SALE PRICE','TOT ASSD $'])
    
    df_group=data.groupby(['OWNER','SALE DATE','SALE PRICE'])
    #kkk = df_group.apply(lambda x: x['SALE PRICE']*x['TOT ASSD $']/x['TOT ASSD $'].sum())
    #data['SALE PRICE']=kkk.reset_index().sort_values('level_3')[0]
    #data['SALE PRICE']=df_group['SALE PRICE'].apply(lambda x: x/len(x))
    data = df_group.filter(lambda x: len(x) == 1)
    
    data['SALE DATE'] = pd.to_datetime(data['SALE DATE'])
    data = data.sort_values('SALE DATE', ascending=False).groupby('ADDRESS').first()
    
    data = data[data['SALE DATE'] >= '2016-01-01']
    data = data[data['COMM']==0]
    data = data[data['RESI']==1]
    
    used_columns = ['LAND SF','GSF','# FLOORS','YEAR BUILT',
                    '# FIREPLACE','LOT FRONTAGE','GARAGE','TOT ASSD $','# BATH','# BED',
                    'SALE PRICE']
    
    data = data[used_columns]
    data = data.dropna()
    data.index = range(0,len(data.index))
    data = data_processing(data)
    data = data[data['SALE PRICE'] >= data['TOT ASSD $']]
    data = data[data['SALE PRICE'] >= 50000]
    data = data[data['SALE PRICE'] <= 10000000]
    data = data[data['TOT ASSD $'] > data['SALE PRICE']/10]
    
    data = data.reset_index(drop=True)
    
    new_data = k_means_cluster(data,1,0.5)
    
    decorred_columns = decorr(copy.deepcopy(new_data), 5)
    decorred_columns = ['YEAR BUILT']+decorred_columns+['class']
    new_data = new_data[decorred_columns]
    
    y_predicts = []
    y_tests = []
    mapes = []
    mapes_insample = []
    total_mape = np.asarray([])
    total_mape_insample = np.asarray([])
    
    for i in list(np.sort(new_data['class'].unique())):
        tmp_data = new_data[new_data['class'] == i]
        y_predict, y_test, mape, mape_insample = ensemble_learning(tmp_data, methods, 15, decorred_columns[:-1], if_test=False)
        y_predicts.append(y_predict)
        y_tests.append(y_test)
        mapes.append(mape)
        mapes_insample.append(mape_insample)
        
    print()
    for i in range(len(list(np.sort(new_data['class'].unique())))):
        print('--------------CLASS:'+str(list(np.sort(new_data['class'].unique()))[i])+'-------------------')
        print('Accuracy in-sample:', round(100 - np.mean(mapes_insample[i]), 2), '%.')
        print('Accuracy out-sample:', round(100 - np.mean(mapes[i]), 2), '%.')
        total_mape = np.hstack([total_mape,mapes[i]])
        total_mape_insample = np.hstack([total_mape_insample,mapes_insample[i]])
        
    print('------------------TOTAL-------------------')
    print('Accuracy rate in-sample: '+str(round(100 - np.mean(total_mape_insample), 2)))
    print('Accuracy rate out-sample: '+str(round(100 - np.mean(total_mape), 2)))
    
    print(np.mean(np.sort(total_mape)[:int(0.5*len(np.sort(total_mape)))]))
    
    #print('Accuracy rate: '+str(round(100 - np.sort(mape)[:int(0.75*len(mape))].mean(), 2)))
    
def model_output():
    methods = ['rf','ridge']
    
    path = 'C:/Users/Administrator/Box/Whiterock Database/NYC/Master Files - NMA/'
    path = 'D:/PHL/master_data/'
    data = pd.read_csv(path+'PHLPL-001 All_Properties [byaddress;location] PLUTO.csv', index_col = 0)
    
    data = data[data['BUILDING'] == 1]
    data = data.dropna(subset=['SALE DATE','SALE PRICE','TOT ASSD $'])
    
    df_group=data.groupby(['OWNER','SALE DATE','SALE PRICE'])
    #kkk = df_group.apply(lambda x: x['SALE PRICE']*x['TOT ASSD $']/x['TOT ASSD $'].sum())
    #data['SALE PRICE']=kkk.reset_index().sort_values('level_3')[0]
    #data['SALE PRICE']=df_group['SALE PRICE'].apply(lambda x: x/len(x))
    data = df_group.filter(lambda x: len(x) == 1)
    
    data['SALE DATE'] = pd.to_datetime(data['SALE DATE'])
    data = data.sort_values('SALE DATE', ascending=False).groupby('ADDRESS').first()
    
    data = data[data['SALE DATE'] >= '2016-01-01']
    data = data[data['COMM']==0]
    data = data[data['RESI']==1]
    
    used_columns = ['LAND SF','GSF','# FLOORS','YEAR BUILT',
                    '# FIREPLACE','LOT FRONTAGE','GARAGE','TOT ASSD $','# BATH','# BED',
                    'SALE PRICE']
    
    data = data[used_columns]
    data = data.dropna()
    data.index = range(0,len(data.index))
    data = data_processing(data)
    data = data[data['SALE PRICE'] >= data['TOT ASSD $']]
    data = data[data['SALE PRICE'] >= 50000]
    data = data[data['SALE PRICE'] <= 10000000]
    data = data[data['TOT ASSD $'] > data['SALE PRICE']/10]
    
    data = data.reset_index(drop=True)
    
    new_data = k_means_cluster(data,1,0.5)
    
    decorred_columns = decorr(copy.deepcopy(new_data), 7)
    decorred_columns = ['YEAR BUILT']+decorred_columns+['class']
    new_data = new_data[decorred_columns]
    
    rf_model, bagging_model, lasso_model, sample_data = ensemble_learning_output(new_data, methods, 15, decorred_columns[:-1])
    
    return rf_model, bagging_model, lasso_model, sample_data, decorred_columns[:-2]
    
