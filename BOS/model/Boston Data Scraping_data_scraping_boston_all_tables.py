#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 30 14:50:51 2019

@author: zhangzhenhong
"""
################################### Get Apartment Parcel ID ###################################
import urllib.request
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
import time
import re
import os
import datetime
import sys
#import boto3
#import s3peat
import warnings
if not sys.warnoptions:
    warnings.simplefilter("ignore")


def unique(items):
    """
    Get unique items from the input
    ----------
    item : list
        Building Parcel ID
    Returns
    -------
    keep : list
        a list of unique Parcel ID
    """    
    found = set([])
    keep = []

    for item in items:
        if item not in found:
            if len(str(item)) == 9:
                item = '0' + str(item)
            else:
                item = str(item)
            found.add(item)
            keep.append(item)

    return keep

def get_all_PID(PID_list_new):  
    """
    Get unit Parcel ID with its master Parcel ID
    ----------
    PID_list_new : list
        Master Parcel ID
    Returns
    -------
    PID_table : list
        a list of unit Parcel ID
    """    
    PID_table = pd.DataFrame(columns = ['Parcel ID', 'Address', 'Owner', 'Value'])
    for i,link in enumerate(PID_list_new):
        try:
            print(i, link)
            url = 'https://www.cityofboston.gov/assessing/search/?parcel='+link+'streetnumber=&streetname=&streetsuffix=&unitnumber=&owner='
            res = urllib.request.urlopen(url)
            html = res.read().decode('utf-8')
            
            PID_pattern = re.compile(r'>[0-9]{10}<')
            Value_pattern = re.compile(r'\$\ ?[+-]?[0-9]{1,3}(?:,?[0-9])*(?:\.[0-9]{1,2})?')
            PID_start_is = [x.start()+1 for x in PID_pattern.finditer(html)]
            Value_end_is = [x.end() for x in Value_pattern.finditer(html)]
            if PID_start_is != []:
                for start, end in zip(PID_start_is, Value_end_is):
                    Info = html[start:end]
                    for r in ['<td>','</td>','\n','\r']:
                        if r == '</td>':
                            Info = Info.replace(r,';')
                        else:
                            Info = Info.replace(r,'')
                    
                    PID_table.loc[PID_table.shape[0]] = Info.split(';')
        
        except:
            Error_PID.append(link)
            pass
            
        if i%5000 == 0:
            time.sleep(1)
            
    PID_table.to_csv('All Parcel ID {}.csv'.format(str(datetime.date.today())))        
    return PID_table


######## Get Apartment Basic Information Table and Value History Table ##########
global error
error = []

def get_tables(PID_unique,a,b):
    """
    Get Get Apartment Basic Information Table, Attributes Table and Value History Table
    ----------
    PID_unique : list
        Unique Unit Parcel ID
    Returns
    -------
    t1 : dataframe
        an Apartment Basic Information Table
    t2 : dataframe
        an Attributes Table
    t3 : dataframe
        a Value History Table       
    """    

    
    for i, link in enumerate(PID_unique):
        try:
            if i == 0:
                print(i,link)
                if len(str(link)) != 10: 
                    link = '0'+str(link)
                
                
                #link = '1501588006'
                url = r'https://www.cityofboston.gov/assessing/search/?pid={}'.format(link)
                res = urllib.request.urlopen(url)
                html = res.read().decode('utf-8')
                
                
                page_content = BeautifulSoup(html)
                tables = page_content.select('table')
                df_list = []
                
                for table in tables:
                    df_list.append(pd.read_html(table.prettify()))
                    
                if len(df_list) <= 2:
                    error.append(link)
                    continue
                
                # get Info Table    
                if df_list[0][2].iloc[2,0] == 'Parcel ID:':
                    t1 = df_list[0][2].iloc[2:-1,:].T
                else:
                    error.append(link)
                    continue           
                
                # get Attribute Table
                if max([sum([bool(re.search('Attributes', str(d))) for d in df.columns]) for df in df_list[0]])!=0:
                    loc = np.argmax([sum([bool(re.search('Attributes', str(d))) for d in df.columns]) for df in df_list[0]])
                    df_list[0][loc].iloc[:,0] = df_list[0][loc].iloc[:,0].apply(lambda x: 'Building '+str(1)+' '+str(x))
                    
                    pattern =  re.compile(r'BUILDING [0-9]+')
                    buildings_table = [sum([bool(re.match(pattern, str(d))) for d in df.columns]) for df in df_list[0]]
                    buildings_table_index = [i for i in range(len(buildings_table)) if buildings_table[i] != 0]
                    
                    if buildings_table_index != []:
                        for i,j in enumerate(buildings_table_index):
                            df_list[0][j].iloc[:,0] = df_list[0][j].iloc[:,0].apply(lambda x: 'Building '+str(i+1)+' '+str(x))
                          
                    t2 = []
                    [t2.extend(df.values.tolist()) for df in df_list[0][loc:-2]]
                    t2 = pd.DataFrame(t2)
                    t2.columns = ['0',link]
                    col = []
                    if t2[t2.iloc[:,0]=='Type:'].index.tolist() != []:
    
                        loc1 = t2[t2['0']=='Type:'].index[0]
                        for i in range(t2.iloc[loc1:,0].shape[0]//2):
                            Type_ = 'EXTRA FEATURES TYPE '+str(i)
                            Size_ = 'EXTRA FEATURES SIZE '+str(i)
                            col.extend([Type_,Size_])
                            
                        t2.iloc[loc1:,0] = col
    
                else:
                    t2 = pd.DataFrame()
    
                # get Value History Table
                if 'Value History' in df_list[0][-1].columns:
                    t3 = df_list[0][-1]
                    
                    Property_Type = t3.iloc[0,1]
                    t3 = t3.iloc[:,[0,2]]
                    t3.columns = ['Fiscal Year',link]
                    t3.loc[t3.shape[0]] = ['Property Type', Property_Type]
                else:
                    error.append(link)
                    continue                

            else:
                print(i,link)
                if len(str(link)) != 10: 
                    link = '0'+str(link)
                url = r'https://www.cityofboston.gov/assessing/search/?pid={}'.format(link)
                res = urllib.request.urlopen(url)
                html = res.read().decode('utf-8')
                
                
                page_content = BeautifulSoup(html)
                tables = page_content.select('table')
                df_list = []
                
                # get Info Table    
                for table in tables:
                    df_list.append(pd.read_html(table.prettify()))
                    
                if len(df_list) <= 2:
                    error.append(link)
                    continue
                    
                if df_list[0][2].iloc[2,0] == 'Parcel ID:':
                    temp1 = df_list[0][2].iloc[2:-1,:]
                else:
                    error.append(link)
                    continue
                 # get Attribute Table
                if max([sum([bool(re.search('Attributes', str(d))) for d in df.columns]) for df in df_list[0]])!=0:
                    loc = np.argmax([sum([bool(re.search('Attributes', str(d))) for d in df.columns]) for df in df_list[0]])
                    df_list[0][loc].iloc[:,0] = df_list[0][loc].iloc[:,0].apply(lambda x: 'Building '+str(1)+' '+str(x))
                    
                    pattern =  re.compile(r'BUILDING [0-9]+')
                    buildings_table = [sum([bool(re.match(pattern, str(d))) for d in df.columns]) for df in df_list[0]]
                    buildings_table_index = [i for i in range(len(buildings_table)) if buildings_table[i] != 0]

                    if buildings_table_index != []:
                        for i,j in enumerate(buildings_table_index):
                            df_list[0][j].iloc[:,0] = df_list[0][j].iloc[:,0].apply(lambda x: 'Building '+str(i+1)+' '+str(x))
                    if t2.empty != 0:
                        t2 = []
                        [t2.extend(df.values.tolist()) for df in df_list[0][loc:-2]]
                        t2 = pd.DataFrame(t2) 
                        t2.columns = ['0',link]
                        col = []
                        if t2[t2.iloc[:,0]=='Type:'].index.tolist() != []:
        
                            loc1 = t2[t2.iloc[:,0]=='Type:'].index[0]
                            for i in range(t2.iloc[loc1:,0].shape[0]//2):
                                Type_ = 'EXTRA FEATURES TYPE '+str(i)
                                Size_ = 'EXTRA FEATURES SIZE '+str(i)
                                col.extend([Type_,Size_])
                                
                            t2.iloc[loc1:,0] = col
                        
                    else:
                        temp2 = []
                        [temp2.extend(df.values.tolist()) for df in df_list[0][loc:-2]]
                        temp2 = pd.DataFrame(temp2) 
                        temp2.columns = ['0',link]
                        col = []
                        if temp2[temp2.iloc[:,0]=='Type:'].index.tolist() != []:
                            loc1 = temp2[temp2.iloc[:,0]=='Type:'].index[0]
                            for i in range(temp2.iloc[loc1:,0].shape[0]//2):
                                Type_ = 'EXTRA FEATURES TYPE '+str(i)
                                Size_ = 'EXTRA FEATURES SIZE '+str(i)
                                col.extend([Type_,Size_])
                            temp2.iloc[loc1:,0] = col
    
                        t2 = pd.merge(t2, temp2, on = '0', how = 'outer',sort=False)
                        
                # get Value History Table
                if 'Value History' in df_list[0][-1].columns:
                    temp3 = df_list[0][-1]
                    Property_Type = temp3.iloc[0,1]
                    temp3 = temp3.iloc[:,[0,2]]
                    temp3.columns = ['Fiscal Year',link]
                    temp3.loc[temp3.shape[0]] = ['Property Type', Property_Type]
                else:
                    error.append(link)
                    continue

                t1.loc[t1.shape[0]] = temp1.iloc[:,-1].values.tolist()
                    
                t3 = pd.merge(t3, temp3, on = 'Fiscal Year', how = 'outer')
    
                
        except:
            print('An exception occurred: {}'.format(error))
            error.append(link)
            pass 
        
    t1 = t1.T.set_index(0).T
    t1.to_csv('table1 {}.csv'.format(str(datetime.date.today())+str(a)))
    
    t2 = t2.set_index('0').T
    t2.to_csv('table2 {}.csv'.format(str(datetime.date.today())+str(a)))
    
    t3 = t3.set_index('Fiscal Year').T
    #rearrange columns
    temp_col = t3.columns.tolist()
    temp_col.remove('Property Type')
    temp_col.append('Property Type')
    
    t3 = t3[temp_col]
    t3.to_csv('table3 {}.csv'.format(str(datetime.date.today())+str(a)))
    
    return t1, t2, t3

     
###################-----------------------------------------------------######################

################## Extract Master Parcel ID from existing file ####################
        
#################################################################################
os.chdir("/home/ubuntu/Area/BOS/Data_Scraping")
# read Bos file
#################################################################################


s3_client = boto3.client('s3')
tmp = s3_client.get_object(Bucket='gsinvest', Key='Boston/Master_Files/BOS-001 Street_Address_Management [bylocation;address] BOS.csv')
PID = pd.read_csv(tmp['Body'],usecols=[26])



#PID = pd.read_csv("BOS-001 Street_Address_Management [bylocation;address] BOS.csv",usecols=[26])
PID['PARCEL'] = PID['PARCEL'].apply(lambda x: '0'+str(x)[:-2] if len(str(x))!=12 else str(x)[:-2])
PID_list = PID.values.reshape((1,-1))[0].tolist()
Error_PID = []

PID_list_new = unique(PID_list)


################## Use Master Parcel ID to get all Apartment Parcel ID ####################

################################################################################
# add a folder
if not os.path.exists('/home/ubuntu/Area/BOS/Data_Scraping/Parcel ID'):
        os.makedirs('/home/ubuntu/Area/BOS/Data_Scraping/Parcel ID')

os.chdir("/home/ubuntu/Area/BOS/Data_Scraping/Parcel ID")        
# local dir
################################################################################

for times in range(100):
    try:
        PID_table = get_all_PID(PID_list_new)
        file_chdir = os.getcwd()
        filecsv_list = []
        for root,dirs,files in os.walk(file_chdir):
            for file in files:
                if os.path.splitext(file)[1] == '.csv':
                    filecsv_list.append(file)
        #break
        if 'All Parcel ID {}.csv'.format(str(datetime.date.today())) in filecsv_list:
            print('We have got all Parcel ID and start to scrape the tables!')
            break
        
    except:
        time.sleep(7) 
        print('We have tried {} times'.format(times + 1))

All_PID = PID_table['Parcel ID'].values.flatten().tolist()
PID_unique = unique(All_PID)

# Use Parcel ID to get Apartment Basic Information Table, Attributes Table and Value History Table #

################################################################################
# add a local dir
if not os.path.exists('/home/ubuntu/Area/BOS/Data_Scraping/Boston'):
        os.makedirs('/home/ubuntu/Area/BOS/Data_Scraping/Boston')
os.chdir("/home/ubuntu/Area/BOS/Data_Scraping/Boston")
# local
################################################################################
length = len(PID_unique)
l = []
for i in range(length//5000+1):
    l.append((i*5000, (i+1)*5000))
    
     
for a,b in l:
    print('Start to scrape table {}!'.format(a+1))
    for times in range(50):
        try:
            t1, t2, t3 = get_tables(PID_unique[a:b],a,b)
            file_chdir = os.getcwd()
            filecsv_list = []
            for root,dirs,files in os.walk(file_chdir):
                for file in files:
                    if os.path.splitext(file)[1] == '.csv':
                        filecsv_list.append(file)
            #break
            if 'table1 {}.csv'.format(str(datetime.date.today())+str(a)) in filecsv_list:
                print('We have got table {}.'.format(a+1))
                break

        except BaseException as error:
            print('An exception occurred: {}'.format(error))
            print('We have tried {} times'.format(times + 1))
            time.sleep(7)

bucket1 = s3peat.S3Bucket('gsinvest','AKIAVOO3BVERZQ4JZ27W','S2gjyUTjNH2W8R8TU4L8azhoE+VXo0IUU+H68984')
f1=s3peat.sync_to_s3(directory='/home/ubuntu/Area/BOS/Data_Scraping/Boston', prefix='Boston/Data_Scraping_Result',bucket=bucket1, concurrency=50)
f2=s3peat.sync_to_s3(directory='/home/ubuntu/Area/BOS/Data_Scraping/Parcel ID', prefix='Boston/Data_Scraping_Result',bucket=bucket1, concurrency=50)

############################################## table cleaning ##############################################


os.chdir("r/home/ubuntu/Area/BOS/Data_Scraping/Boston/Data_Scraping_Result")

#******************************************************
s3_resource = boto3.resource('s3')
bucket = s3_resource.Bucket('gsinvest')
filecsv_table1_list = []
filecsv_table2_list = []
filecsv_table3_list = []

for obj in bucket.objects.filter(Prefix='Boston/Data_Scraping_Result'):
    if 'table1' in obj.key:
        filecsv_table1_list.append(obj.key)
    if 'table2' in obj.key:
        filecsv_table2_list.append(obj.key)
    if 'table3' in obj.key:
        filecsv_table3_list.append(obj.key)
        

def merge_table(file_list):
    for i, file in enumerate(file_list):
        if i == 0:
            tmp = s3_client.get_object(Bucket='gsinvest', Key=file)
            df = pd.read_csv(tmp['Body'])
        else:
            tmp = s3_client.get_object(Bucket='gsinvest', Key=file)
            temp = pd.read_csv(tmp['Body'])
            df=df.append(temp,sort=False)
            
    return df

def cleaner(table1, table2, table3):
    table1 = merge_table(filecsv_table1_list)
    table1 = table1.iloc[:,1:]
    table1.set_index('Parcel ID:',inplace = True)
    
    table2 = merge_table(filecsv_table2_list)
    table2.set_index('Unnamed: 0',inplace = True)
    
    table3 = merge_table(filecsv_table3_list)
    table3.set_index('Unnamed: 0',inplace = True)
    
    master_table = table1.merge(table3, right_index = True, left_index = True, how = 'inner')
    master_table = master_table.merge(table2, right_index = True, left_index = True, how = 'inner')
    
    master_table['City:'] = master_table['Address:'].apply(lambda x: re.findall(r'BOSTON',x)[0])
    master_table['State:'] = master_table['Address:'].apply(lambda x: re.findall(r'MA',x)[0])
    master_table['Zip:'] = master_table['Address:'].apply(lambda x: re.findall(r'MA [0-90]+',x)[0][3:])
    master_table['Address:'].replace(r'BOSTON MA [0-9]+','', regex=True,inplace = True)
    master_table['Unit:'] = master_table['Address:'].apply(lambda x: re.findall(r'# .+',x)[0] if re.findall(r'# [0-9]+',x) != [] else np.nan)
    master_table['Unit:'].replace(regex={'# ':''},inplace = True)
    master_table['Address:'].replace(r' # .+','', regex=True,inplace = True)
    master_table['Address:'] = master_table['Address:'].apply(lambda x: x.lower())
    master_table['Address:'].replace(regex={' st[ ]{0,1}$': ' street', 
                ' rd[ ]{0,1}$': ' road', 
                ' av[ ]{0,1}$': ' avenue', 
                ' ct[ ]{0,1}$': ' court', 
                ' pl[ ]{0,1}$': ' plaza',
                ' te[ ]{0,1}$': ' terrace', 
                ' dr[ ]{0,1}$': ' drive', 
                ' ro[ ]{0,1}$': ' row',
                ' pa[ ]{0,1}$': ' park',
                ' pk[ ]{0,1}$': ' park',
                ' bl[ ]{0,1}$': ' boulevard',
                ' cc[ ]{0,1}$': ' circuit',
                ' ci[ ]{0,1}$': ' circle',
                ' cr[ ]{0,1}$': ' crescent',
                ' ct[ ]{0,1}$': ' court',
                ' cw[ ]{0,1}$': ' crossway',
                ' hw[ ]{0,1}$': ' highway',
                ' la[ ]{0,1}$': ' lane',
                ' ln[ ]{0,1}$': ' lane',
                ' pw[ ]{0,1}$': ' parkway',
                ' sq[ ]{0,1}$': ' square',
                ' wa[ ]{0,1}$': ' way',
                ' wh[ ]{0,1}$': ' wharf',
                ' wy[ ]{0,1}$': ' way'}, inplace = True)
    
    master_table['Address:'].replace(regex={' e ': ' east ',' w ': ' west ', ' n ': ' north ', ' s ': ' south '}, inplace = True)    
    
    master_table.replace(regex = {'[,( sq ft)$':''}, inplace=True)
    master_table[['2019','2018','2017','2016','2015','2014','2013','2012',
     '2011','2010','2009','2008','2007','2006','2005','2004','2003','2002',
     '2001','2000','1999','1998','1997','1996','1995','1994','1993','1992',
     '1991','1990','1989','1988','1987','1986','1985']] = master_table[['2019','2018','2017','2016','2015','2014','2013','2012',
     '2011','2010','2009','2008','2007','2006','2005','2004','2003','2002',
     '2001','2000','1999','1998','1997','1996','1995','1994','1993','1992',
     '1991','1990','1989','1988','1987','1986','1985']].replace('[\$,]', '', regex=True)
    
    master_table = pd.read_csv('Boston Master Table.csv',index_col = 0)
    master_table[['Lot Size:', 'Living Area:']] = master_table[['Lot Size:', 'Living Area:']].replace(regex={' sq ft':'',',':''})
    master_table.to_csv(r'C:\Users\jorda\Downloads\Boston Residential Master Table.csv')
    return master_table


# Scraping tables
table1, table2, table3 = t1, t2, t3
# Cleaning tables
master_table = cleaner(table1, table2, table3)
all_parcel_id = pd.read_csv(r'C:\Users\jorda\Downloads\Boston\All Parcel ID 2019-07-08.csv',usecols=[1])['Parcel ID'].to_list()
unscraped_id = set(all_parcel_id)-set(master_table.reset_index()['index'].to_list())