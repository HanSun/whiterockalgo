# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 10:36:48 2019

@author: teddy
"""

import urllib
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.linear_model import LinearRegression
import os
import glob
import random
from tensorflow import keras
import tensorflow as tf
from PIL import Image
import copy
import math
from sklearn.externals import joblib
#import prettytable as pt
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.writer.excel import ExcelWriter
from datetime import date
import datetime
import Rent_model_NYC_v2 as rent_model
import Sale_price_model_NYC_v2 as sales_model
#import Expense_Elec_model_v2 as elec_model
import Macroeconomic_model_v1 as macro_model
import Vacancy_model_v2 as vacancy_model
from Underwritting_generator_NYC import *
from PIL import Image, ImageEnhance
from IPython.display import Image as Im
from IPython.display import display

from os import walk

import warnings
warnings.filterwarnings("ignore")


#%% SUPPORT FUNCTION
class PicDownloader:
    def __init__(self):
        self.key  = "AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE"
        
    def download(self, url, name):
        urllib.request.urlretrieve(url.replace(" ", "%20"),"%s.jpg" % name)

    def gen_url(self, geom, fov=100, heading=0, pitch=30, size=(500, 500)):
        # fov控制镜头缩进，数值越小图片越大，最大为120
        # heading控制朝向，0为北，90为东，180为南
        # pitch控制向上的仰视的角度
        x, y = size
        lat, lng = geom
        return "https://maps.googleapis.com/maps/api/streetview?size=%sx%s&location=%s,%s&fov=%s&heading=%s&pitch=%s&key=AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE" % (x, y, lat, lng, fov, heading, pitch)

    def gen_url_by_string(self, address, fov=100, pitch=30, size=(400, 400)):
        x, y = size
        return "https://maps.googleapis.com/maps/api/streetview?size=%sx%s&location=%s&fov=%s&pitch=%s&key=AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE" % (x, y, address, fov, pitch)

PD = PicDownloader()

def com_with_dis(img, material, criterion=100, dis=10):
    L, W = img.size
    length = int(680*L/W)
    img = img.resize((length,680))
    img_array = np.array(img)
    
    sampling = lambda: (random.randint(0,55), random.randint(0,39))
    i = 0
    score = []
    ii = 0
    while i < 1000:
        sample_1 = sampling()
        sample_2 = sampling()
        if np.linalg.norm(np.array(sample_1)-np.array(sample_2)) >= dis:
            arr_1 = img_array[sample_1[0]*12:(sample_1[0]+1)*12, sample_1[1]*12:(sample_1[1]+1)*12, :].astype(np.int64)
            arr_2 = img_array[sample_2[0]*12:(sample_2[0]+1)*12, sample_2[1]*12:(sample_2[1]+1)*12, :].astype(np.int64)
            if arr_1.shape == (12,12,3) and arr_2.shape == (12,12,3):
                if np.linalg.norm(arr_1-np.array(material),axis=2).mean() <= criterion:
                    if np.linalg.norm(arr_2-np.array(material),axis=2).mean() <= criterion:
                        score.append(np.linalg.norm(arr_1.mean(axis=(0,1)) - arr_2.mean(axis=(0,1))))
                        i += 1
                        
        ii += 1
        if ii > 1000000 and i < 100:
            break;
    return np.array(score).mean()

def crop(img, left, right, top, bottom):
    x, y = img.size
    area = (int(x*left), int(y*top), int(x*right), int(y*bottom))
    return img.crop(area)

def transparency(img, material):    
    L, W = img.size
    length = int(300*L/W)
    img = img.resize((length,300))
    win = 0
    img_array = np.array(img)
    for i in range(300//9):
        array = img_array[i*9:(i+1)*9, :]
        array_dis = np.linalg.norm(array-np.array(material),axis=2)
        dummy_array_dis = np.where(array_dis<100,0,1)
        array_mean = array.mean(axis=0)
        mean_dis = np.linalg.norm(array_mean-np.array(material),axis=1)
        dummy_mean_dis = np.where(mean_dis<100,0,1)
        dis_count = dummy_array_dis * dummy_mean_dis[np.newaxis,:]
        win += dis_count.sum()
    return win/(300*length)

def multi_crop(img, material):
    res = {}
    for i in range(100,301,2):
        i /= 1000
        for j in range(100,301,2):
            j /= 1000
            cropped = crop(img,i,1-i,j,1-j)
            score = transparency(cropped, material)
            res.setdefault((i,j),score)
    return res

def compute(dic,method='mean'):
    scores = list(dic.values())
    if method == 'mean':
        return np.array(scores).mean()
    if method == 'median':
        return np.median(np.array(scores))
    raise ValueError('Indistinguishable method {}'.format(method))

def prediction(model, cropped_img, shape: tuple):
	"""
	Before prediction, the test image is recommended to be cropped. Use the function crop in part one
	params:
	cropped_image: raw image data, PIL.Image.Image object
	shape: tuple, (length, width)
	"""
	L, W = shape
	img = np.array(cropped_img.resize(shape))
	img = img/255
	img = img.reshape(1,L,W,3)
	pred = model.predict(img).argmax()
	if pred == 0:
		return 'Brick'
	if pred == 1:
		return 'Glass'
	if pred == 2:
		return 'Limestone'

def addmonths(date,months):
    targetmonth=months+date.month
    try:
        date = date.replace(year=date.year+int(targetmonth/12),month=(targetmonth%12))
    except:
        date = date.replace(year=date.year+int((targetmonth+1)/12),month=((targetmonth+1)%12),day=1)
        date+=datetime.timedelta(days=-1)
        
    return date

def year_process(j):
    if j < 1900:
        return 1
    elif 1900<=j <1910:
        return 2
    elif 1910<=j<1920:
        return 3
    elif 1920<=j <1930:
        return 4
    elif 1930<=j<1940:
        return 5
    elif 1960<=j <1970:
        return 8
    elif 1970<=j<1980:
        return 9
    elif 1980<=j <1990:
        return 10
    elif 1990<=j<2000:
        return 11
    elif 2000<=j<2010:
        return 12
    else:
        return 13

def distance_calculator(x,lon1,lat1):
    lon2 = x['LONGITUDE']
    lat2 = x['LATITUDE']
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = math.sin(dlat/2)**2 + math.cos(np.pi*lat1/180) * math.cos(np.pi*lat2/180) * math.sin(dlon/2)**2
    c = 2 * math.atan2(math.sqrt(a),math.sqrt(1-a)) 
    r = 3956
    return c * r / 35

def cluster_label(x,model):
    cols = []
    cols.append(x['GSF'])
    cols.append(x['LAND SF'])
    cols.append(x['# UNITS'])
    cols.append(x['# FLOORS'])
    cols.append(x['ELEVATOR'])
    cols.append(x['TRANSPARENCY'])
    cols.append(x['DIRTINESS'])
    
    try:
        label = model.predict(np.asarray(cols).reshape(1,-1))[0]
        return int(label)
    except:
        return -1

def df_process(data, features):
    for feature in features:
        if feature == 'ZIP' or feature == 'YEAR BUILT':
            data[[feature]]=data[[feature]].astype(str)
        elif feature == 'DIRTINESS':
            data[[feature]]=data[[feature]].astype(float)
        else:
            data[[feature]]=data[[feature]].astype(int)
    
    return data

def check_from_pluto(pluto, address):
    if len(pluto[ pluto['ADDRESS'] == address ].values) == 0:
        return 0
    else:
        return 1
        
def check_zip_pluto(pluto, zip_code):
    if len(pluto[ pluto['ZIP'] == zip_code ].values) == 0:
        return 0
    else:
        return 1

def searching_from_pluto(pluto, address, target):
    return pluto[pluto['ADDRESS'] == address][target].values[0]
    
def get_elevator(pluto, address):
    bldg_class = pluto[pluto['ADDRESS'] == address]['BLDG CLASS NOW'].values[0]
    num_floors = pluto[pluto['ADDRESS'] == address]['# FLOORS'].values[0]
    
    if num_floors >= 5:
        return 1
    else:
        return 0
        
def get_tax_class(pluto, address):
    num_units = pluto[pluto['ADDRESS'] == address]['# UNITS'].values[0]
    
    if num_units > 3:
        return 2
    else:
        return 1

def get_tax(pluto, address):
    units_num = int(searching_from_pluto(pluto, address, '# UNITS'))
    assest_total = int(searching_from_pluto(pluto, address, 'assesstot'))
    exempt_total = int(searching_from_pluto(pluto, address, 'exempttot'))
    
    if units_num <= 3:
        tax_rate = 0.20919
    else:
        tax_rate = 0.12612
        
    return tax_rate * (assest_total - exempt_total)
        
def generate_whrk_class(property_features, formatting):
    whrk_class_dict = {}
    
    if property_features['TOTAL UNITS'] < 6:
        whrk_class_dict['< 5 UNITS'] = 1
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif property_features['TOTAL UNITS'] < 11:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 1
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif property_features['TOTAL UNITS'] < 21:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 1
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif property_features['TOTAL UNITS'] < 51:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 1
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 0
    elif property_features['TOTAL UNITS'] < 101:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 1
        whrk_class_dict['> 100 UNITS'] = 0
    else:
        whrk_class_dict['< 5 UNITS'] = 0
        whrk_class_dict['6 - 10 UNITS'] = 0
        whrk_class_dict['11 - 20 UNITS'] = 0
        whrk_class_dict['21 - 50 UNITS'] = 0
        whrk_class_dict['51 - 100 UNITS'] = 0
        whrk_class_dict['> 100 UNITS'] = 1
        
    if property_features['ELEVATOR'] == 1:
        whrk_class_dict['ELEVATOR'] = 1
        whrk_class_dict['WALK UP'] = 0
    else:
        whrk_class_dict['ELEVATOR'] = 0
        whrk_class_dict['WALK UP'] = 1
    
    if property_features['COMM GSF'] > 0:
        whrk_class_dict['MIXED USE'] = 1
    else:
        whrk_class_dict['MIXED USE'] = 0
        
    index_ = []
    for j in ['< 5 UNITS','6 - 10 UNITS','11 - 20 UNITS','21 - 50 UNITS','51 - 100 UNITS','> 100 UNITS','ELEVATOR','WALK UP','MIXED USE']:
        index_.append(whrk_class_dict[j])
    index_ = tuple(index_)
    class_ = 'W'+str(formatting.loc[index_,:].values[0])
    whrk_class_dict['WHRK CLASS'] = class_
    
    return whrk_class_dict

def rent_input_generate(neibor_features, whrk_class_dict, property_features, key, sample_zip, material_label, transparency_rate, clean_score):
    if key in ['< 5 UNITS','6 - 10 UNITS','11 - 20 UNITS','21 - 50 UNITS','51 - 100 UNITS','> 100 UNITS','ELEVATOR','WALK UP','MIXED USE']:
        return int(whrk_class_dict[key])
    elif key in ['bank', 'bus_station', 'gym', 'hospital', 'park', 'restaurant', 'school', 'shopping_mall', 'subway_station']:
        return int(neibor_features[key])
    elif key == 'STAB':
        return 0
    elif key == 'GSF':
        return int(property_features['BLDG GSF'])
    elif key == '# UNITS':
        return int(property_features['TOTAL UNITS'])
    elif key == 'ZIP':
        sy_zip = []
        for i in sample_zip:
            sy_zip.append(int(i))
        sy_zip = sorted(sy_zip)
        tmp_zip = property_features[key]
        pre_zip = 0
        if not tmp_zip in sy_zip:
            for i in sy_zip:
                if tmp_zip < i:
                    if (tmp_zip - pre_zip) < (i - tmp_zip):
                        tmp_zip = pre_zip
                    else:
                        tmp_zip = i
                    break
                pre_zip = i
        return str(tmp_zip)
    elif key == 'YEAR BUILT':
        return str(year_process(property_features[key]))
    elif key == 'MATERIAL':
        return material_label
    elif key == 'TRANSPARENCY':
        return transparency_rate
    elif key == 'DIRTINESS':
        return clean_score
    else:
        #print(key)
        return int(property_features[key])
    
def sale_input_generate(neibor_features, whrk_class_dict, property_features, key, sample_zip):
    if key in ['< 5 UNITS','6 - 10 UNITS','11 - 20 UNITS','21 - 50 UNITS','51 - 100 UNITS','> 100 UNITS','ELEVATOR','WALK UP','MIXED USE']:
        return int(whrk_class_dict[key])
    elif key in ['bank', 'bus_station', 'gym', 'hospital', 'park', 'restaurant', 'school', 'shopping_mall', 'subway_station']:
        return int(neibor_features[key])
    elif key == 'STAB':
        return 0
    elif key == 'GSF':
        return int(property_features['BLDG GSF'])
    elif key == '# UNITS':
        return int(property_features['TOTAL UNITS'])
    elif key == 'ZIP':
        sy_zip = []
        for i in sample_zip:
            sy_zip.append(int(i))
        sy_zip = sorted(sy_zip)
        tmp_zip = property_features[key]
        pre_zip = 0
        if not tmp_zip in sy_zip:
            for i in sy_zip:
                if tmp_zip < i:
                    if (tmp_zip - pre_zip) < (i - tmp_zip):
                        tmp_zip = pre_zip
                    else:
                        tmp_zip = i
                    break
                pre_zip = i
        return str(tmp_zip)
    elif key == 'YEAR BUILT':
        return str(year_process(property_features[key]))
    else:
        #print(key)
        return int(property_features[key])
    
#%% GLOBAL 
holding_period = 3
cap_rate = 0.045
water = 1
#rent_growth_rates = list(np.asarray(macro_model.get_rent_growth_use(holding_period-1))/100)
#vacancy_rates = list(np.asarray(vacancy_model.get_rental_vacancy_use(holding_period-1))/100)

#%% MODEL OUTPUT
path = 'D:/NYC/master_data/'
output_path = 'D:/NYC/estimation_pluto/'

rf_rent, bagging_rent, gradient_rent, enet_rent, ridge_rent, lasso_rent, rent_sample, rent_features, cluster_model_rent = rent_model.model_output()
#rf_sale, bagging_sale, gradient_sale, enet_sale, ridge_sale, lasso_sale, sale_sample, sale_features = sales_model.model_output()

rent_growth_rate_dict = {}
vac_growth_rate_dict = {}
growth_rate_keys =  ['Bronx County','Kings County','Metro','Midtown West','Morningside Washington',
                     'Queens County','Stuyvesant Turtle Bay','Upper East Side','Upper West Side', 'W Village Downtown']

rent_rate_periods = list(range(2020,2029))
vac_rate_periods = list(range(2020,2029))
cap_rate_periods = list(range(2020,2024))

cap_rate_data = pd.read_csv(path+'rate/NYC_Metro_Transaction_REIS.csv', index_col = 0)
#cap_rate_data = cap_rate_data[cap_rate_data['Qtr'] == 'Y']
#cap_rate_data = cap_rate_data.set_index('Year')
cap_rate_list = list(cap_rate_data.loc[cap_rate_periods,'WHRK'].values)    

for key in growth_rate_keys:
    tmp_data = pd.read_csv(path+'rate/NYC_'+key+'_REIS_Report.csv', index_col = 0)
    tmp_data = tmp_data[tmp_data['Quarter'] == 'Y']
    tmp_data = tmp_data.set_index('Year')
    tmp_rent = list(tmp_data.loc[rent_rate_periods,'WHRK rent %'].values)
    tmp_rent.append(tmp_data.loc[2018,'WHRK rent %'])
    tmp_vac  = list(tmp_data.loc[vac_rate_periods,'WHRK vac %'].values)
    
    rent_growth_rate_dict[key] = tmp_rent
    vac_growth_rate_dict[key] = tmp_vac
    
elec = 0.27 # assumption just for now

amenity_weights = [0.04878099, 0.11831073, 0.56815079, 0.01231, 0.71678136, 0.13852546, 0.20806348, 0.48968476, 0.68982312]

#%% READ FEATURES OF PROPERTIES
#path = '/home/hansun/WHRK/'
import traceback
testing_database = pd.read_csv(path+'property_to_estimate_NY_20190928.csv', index_col = 0)
testing_database = testing_database.dropna(subset=['ASKING PRICE'])
#testing_database[['ZIP']] = testing_database[['ZIP']].astype(int)
testing_database = testing_database.reset_index(drop = True)
pluto = pd.read_csv(path+'NPL-001 All_Properties [bylocation;address] PLUTO.csv')
rent_master = pd.read_csv(path+'NMA-001 Rent_Master [bylocation;addresses].csv',index_col = 0)
rent_master = rent_master.dropna(subset=['RENT PSF'])
rent_master_unique = pd.read_csv(path+'NMA-001 Rent_Master_Unique [bylocation;addresses].csv',index_col = 0)
stab_units = pd.read_csv(path+'stab_units.csv',index_col=0)
stab_list = list(stab_units['Address'])
formatting = pd.read_excel(path+'Building_Categories_Format.xlsx')
formatting = formatting.set_index(list(formatting.columns[1:]))
census = pd.read_csv(path+'Census_Bureau_5digits.csv')
census['ZIP'] = census['City'].apply(lambda x: x[-5:])
#testing_database = testing_database.reset_index()
num = 0
add_list = []
cnn_model = keras.models.load_model(path+'cnn/Model_July_2.h5')

with open(path+'cnn/features_from_brick.txt','r') as f:
    line = f.readline()
Brick = [float(x) for x in line.split()]

with open(path+'cnn/features_from_lime.txt','r') as f:
    line = f.readline()
Lime = [float(x) for x in line.split()]

nnn = 0

for i in range(len(rent_master_unique.index)):
#for i in range(2): 
    # info collection
    address = rent_master_unique.loc[i, 'ADDRESS']
    if check_from_pluto(pluto, address) == 0:
        add_list.append(address)
        num+=1
        pass
    elif searching_from_pluto(pluto, address, 'ZIP') is np.nan or searching_from_pluto(pluto, address, 'ZIP') == 0:
        pass
    elif pluto[pluto['ADDRESS'] == address]['STAB'].values[0] == 1:
        pass
    elif searching_from_pluto(pluto, address, 'RETAIL SF') > 10:
        pass
    elif address in stab_list:
        pass
    elif searching_from_pluto(pluto, address, 'CONDO') == 1:
        pass
    else:
        try:
            property_features = {}
            property_features['Address'] = address
            property_features['ZIP'] = int(searching_from_pluto(pluto, address, 'ZIP'))
            property_features['CITY'] = 'New York'
            property_features['STATE'] = 'NY'
            property_features['BLOCK'] = searching_from_pluto(pluto, address, 'BLOCK')
            property_features['LOT'] = searching_from_pluto(pluto, address, 'LOT')
            property_features['LATITUDE'] = searching_from_pluto(pluto, address, 'LATITUDE')
            property_features['LONGITUDE'] = searching_from_pluto(pluto, address, 'LONGITUDE')
            property_features['ID'] = str(i)
            property_features['OWNER'] = searching_from_pluto(pluto, address, 'OWNER')
            property_features['FIPS'] = 'Nan'
            property_features['REGION'] = 'Northeast'
            
            property_features['# BUILDINGS'] = searching_from_pluto(pluto, address, '# BLDG')
            if np.isnan(property_features['# BUILDINGS']):
                property_features['# BUILDINGS'] = 0

            property_features['# FLOORS'] = int(searching_from_pluto(pluto, address, '# FLOORS'))
            property_features['BUILDING SITUATION'] = searching_from_pluto(pluto, address, 'LOT TYPE')
            property_features['LAND SF'] = int(searching_from_pluto(pluto, address, 'LAND SF'))
            property_features['YEAR BUILT'] = int(searching_from_pluto(pluto, address, 'YEAR BUILT'))
            property_features['LOT DEPTH'] = searching_from_pluto(pluto, address, 'LOT DEPTH')
            if np.isnan(property_features['LOT DEPTH']):
                property_features['LOT DEPTH'] = 0
            property_features['LOT FRONT'] = searching_from_pluto(pluto, address, 'LOT FRONT')
            if np.isnan(property_features['LOT FRONT']):
                property_features['LOT FRONT'] = 0
            property_features['SCHOOL DISTRICT'] = searching_from_pluto(pluto, address, 'SCHOOL DIS')
            if np.isnan(property_features['SCHOOL DISTRICT']):
                property_features['SCHOOL DISTRICT'] = 0
            property_features['ELEVATOR'] = get_elevator(pluto, address) # modify it
            property_features['MATERIAL'] = 'Brick' # Add CNN
            property_features['VIEWS'] = 0
            if np.isnan(searching_from_pluto(pluto, address, 'pfirm15_flag')):
                property_features['FEMA'] = 0
            else:
                property_features['FEMA'] = 1
            property_features['DESIGNATION'] = 0
            
            #try:
            #    property_features['BLDG GSF'] = int(testing_database.loc[i, 'GSF'])
            #except:
            property_features['BLDG GSF'] = int(searching_from_pluto(pluto, address, 'GSF'))
            property_features['RESI GSF'] = int(searching_from_pluto(pluto, address, 'RESI SF'))
            property_features['COMM GSF'] = 0 #testing_database.loc[i, 'COMM']
            property_features['RESI NSF'] = property_features['RESI GSF'] * 0.85
            property_features['COMM RSF'] = property_features['COMM GSF'] * 0.85
            property_features['TOTAL UNITS'] = int(searching_from_pluto(pluto, address, '# UNITS'))
            
            property_features['RESI UNITS'] = property_features['TOTAL UNITS']
            property_features['COMM UNITS'] = property_features['TOTAL UNITS'] - property_features['RESI UNITS']
            property_features['STAB UNITS'] = 0
            property_features['AVG SF/RESI UNITS'] = property_features['RESI NSF'] / property_features['TOTAL UNITS']
            if property_features['COMM RSF'] == 0:
                property_features['AVG SF/COMM UNITS'] = 0.0
            else:
                property_features['AVG SF/COMM UNITS'] = property_features['COMM RSF'] / property_features['COMM UNITS']
            #property_features['TOTAL ASSESED VALUE'] = testing_database.loc[i, 'ASKING PRICE']
            property_features['TAX CLASS'] = get_tax_class(pluto, address)
            try:
                property_features['ACTUAL RENT'] = np.nan # TO DO
            except:
                property_features['ACTUAL RENT'] = np.nan
                
            property_features['EVALUATE ASSET'] = searching_from_pluto(pluto, address, 'assesstot')
            
            whrk_class_dict = generate_whrk_class(property_features, formatting)
            
            property_features['WHRK CLASS'] = whrk_class_dict['WHRK CLASS']
            property_features['RENT PSF'] = float(rent_master_unique.loc[i, 'RENT PSF'])
            
            neibor_features = {}
            for ammenity in ['bank', 'bus_station', 'gym', 'hospital', 'park', 'restaurant', 'school', 'shopping_mall', 'subway_station']:
                neibor_features[ammenity] = searching_from_pluto(pluto, address, ammenity)
            
            # cnn prediciton
            city = 'NYC'
            new_addr = '_'.join(address.split('/')) + ', ' + city
            
            material_label = ''
            pic = ''
            pic_path = ''
            if os.path.exists("D:/NYC/pics/Glass/"+new_addr+".png"):
                material_label = 'Glass'
                pic = Image.open("D:/NYC/pics/Glass/"+new_addr+".png")
                pic_path = "D:/NYC/pics/Glass/"+new_addr+".png"
            elif os.path.exists("D:/NYC/pics/Brick/"+new_addr+".png"):
                material_label = 'Brick'
                pic = Image.open("D:/NYC/pics/Brick/"+new_addr+".png")
                pic_path = "D:/NYC/pics/Brick/"+new_addr+".png"
            elif os.path.exists("D:/NYC/pics/Limestone/"+new_addr+".png"):
                material_label = 'Limestone'
                pic = Image.open("D:/NYC/pics/Limestone/"+new_addr+".png")
                pic_path = "D:/NYC/pics/Limestone/"+new_addr+".png"
            elif os.path.exists("D:/NYC/pics/Other/"+new_addr+".png"):
                material_label = 'Other'
                pic = Image.open("D:/NYC/pics/Other/"+new_addr+".png")
                pic_path = "D:/NYC/pics/Other/"+new_addr+".png"
            elif os.path.exists("D:/NYC/pics/Wood Panels/"+new_addr+".png"):
                material_label = 'Wood'
                pic = Image.open("D:/NYC/pics/Wood Panels/"+new_addr+".png")
                pic_path = "D:/NYC/pics/Wood Panels/"+new_addr+".png"
                
            #print(material_label)
            property_features['MATERIAL'] = material_label
            
            if material_label == 'Brick':
                res_1 = multi_crop(pic, Brick)
                transparency_rate = compute(res_1,'mean')
                pic_1 = pic.convert("RGB")
                #display(Im(filename=image))
                contrast = ImageEnhance.Contrast(pic_1)
                contrast = contrast.enhance(1.0)
                clean_score = com_with_dis(contrast, Brick, criterion=80)
            elif material_label == 'Limestone' or material_label == 'Wood':
                res_1 = multi_crop(pic, Lime)
                transparency_rate = compute(res_1,'mean')
                pic_1 = pic.convert("RGB")
                #display(Im(filename=image))
                contrast = ImageEnhance.Contrast(pic_1)
                contrast = contrast.enhance(1.0)
                clean_score = com_with_dis(contrast, Lime, criterion=80)
            elif material_label == 'Other':
                transparency_rate = 0
                clean_score = 0
            else:
                transparency_rate = 1
                clean_score = 0
                
            #print(transparency_rate)
            #print(clean_score)
            
            # closing buildings
            closing_bldf_dict = {}
            closing_df = copy.deepcopy(rent_master)
            closing_df['dis'] = rent_master.apply(distance_calculator, args=(property_features['LONGITUDE'], property_features['LATITUDE'], ), axis=1)
            closing_df['label'] = rent_master.apply(cluster_label, args=(cluster_model_rent, ), axis=1)
            target_label = cluster_model_rent.predict(np.asarray([property_features['BLDG GSF'],property_features['LAND SF'],property_features['TOTAL UNITS'],
                                                     property_features['# FLOORS'],property_features['ELEVATOR'],transparency_rate,clean_score]).reshape(1,-1))[0]
            closing_df = closing_df.sort_values(by=['dis'])
            closing_df = closing_df[closing_df['label'] == target_label]
            closing_df = closing_df[closing_df['dis']>0]
            closing_df = closing_df.reset_index(drop=True)
            
            for ii in range(100):
                try:
                    closing_bldf_dict[closing_df.loc[ii,'ADDRESS']] = [closing_df.loc[ii,'GSF'], closing_df.loc[ii,'dis'], closing_df.loc[ii,'RENT PSF']]
                except:
                    pass
            
            # rent and sale input
            rent_input = []
            sale_input = []
            for feature in rent_features:
                rent_input.append(rent_input_generate(neibor_features, whrk_class_dict, property_features, feature, list(rent_sample['ZIP'].unique()),material_label,transparency_rate,clean_score))
            #for feature in sale_features:
            #    sale_input.append(sale_input_generate(neibor_features, whrk_class_dict, property_features, feature, list(sale_sample['ZIP'].unique())))    
            rent_input = np.asarray(rent_input)
            #sale_input = np.asarray(sale_input)
                
            
            # rent model calibration
            tmp = pd.DataFrame(rent_input.reshape(1,len(rent_input)), index=['test'], columns=rent_sample.columns)
            tmp = df_process(tmp, rent_features)
            tmp = pd.concat([rent_sample,tmp],axis=0)
            test_tmp = pd.get_dummies(tmp)
            test_tmp = test_tmp.loc['test',:].values.reshape(1,len(test_tmp.columns))
            
            test_rf = rf_rent.predict(test_tmp)
            #test_ada = ada_rent.predict(test_tmp)
            test_bagging = bagging_rent.predict(test_tmp)
            test_gradient = gradient_rent.predict(test_tmp)
            test_enet = enet_rent.predict(test_tmp)
            test_ridge = ridge_rent.predict(test_tmp)
            test_stack = lasso_rent.predict(np.asarray([test_rf[0],test_bagging[0],test_gradient[0],test_enet[0],test_ridge[0]]).reshape(1,5))[0]
            #test_stack = test_ada[0]
            
            # sale model calibration
            #tmp_sale = pd.DataFrame(sale_input.reshape(1,len(sale_input)), index=['test'], columns=sale_sample.columns)
            #tmp_sale = df_process(tmp_sale, sale_features)
            #tmp_sale = pd.concat([sale_sample,tmp_sale],axis=0)
            #test_tmp_sale = pd.get_dummies(tmp_sale)
            #test_tmp_sale = test_tmp_sale.loc['test',:].values.reshape(1,len(test_tmp_sale.columns))
            
            #test_rf_sale = rf_sale.predict(test_tmp_sale)
            #test_ada_sale = ada_sale.predict(test_tmp_sale)
            #test_bagging_sale = bagging_sale.predict(test_tmp_sale)
            #test_gradient_sale = gradient_sale.predict(test_tmp_sale)
            #test_enet_sale = enet_sale.predict(test_tmp_sale)
            #test_ridge_sale = ridge_sale.predict(test_tmp_sale)
            #test_stack_sale = lasso_sale.predict(np.asarray([test_rf_sale[0],test_bagging_sale[0],test_gradient_sale[0],test_enet_sale[0],test_ridge_sale[0]]).reshape(1,5))[0]
            #test_stack_sale = test_enet_sale[0]
            
            property_features['ASKING PRICE'] = 0
            
            # projection info
            sub_market = searching_from_pluto(pluto, address, 'REIS SUBMARKET')
            if sub_market == 'Staten Island':
                sub_market = 'Metro'
            elif sub_market == 'Morningside/Washington':
                sub_market = 'Morningside Washington'
            elif sub_market == 'West village/Downtown':
                sub_market = 'W Village Downtown'
            elif sub_market == 'West Village/Downtown':
                sub_market = 'W Village Downtown'
            elif sub_market == 'Stuyvesant/Turtle Bay':
                sub_market = 'Stuyvesant Turtle Bay'
            
            projection_dict = {}
            projection_dict['rent'] = test_stack*(1+0.05)
            projection_dict['sale'] = 0
            projection_dict['rent growth'] = rent_growth_rate_dict[sub_market]
            projection_dict['vacancy rate'] = vac_growth_rate_dict[sub_market]
            projection_dict['cap rate'] = cap_rate
            projection_dict['water'] = water
            projection_dict['elec'] = elec
            projection_dict['tax'] = get_tax(pluto, address)
            
            # amenity
            amenity = {}
            amenity['bank'] = int(searching_from_pluto(pluto, address, 'bank'))
            amenity['bus_station'] = int(searching_from_pluto(pluto, address, 'bus_station'))
            amenity['gym'] = int(searching_from_pluto(pluto, address, 'gym'))
            amenity['hospital'] = int(searching_from_pluto(pluto, address, 'hospital'))
            amenity['park'] = max(int(searching_from_pluto(pluto, address, 'park')),6)
            amenity['restaurant'] = int(searching_from_pluto(pluto, address, 'restaurant'))
            amenity['school'] = int(searching_from_pluto(pluto, address, 'school'))
            amenity['shopping_mall'] = int(searching_from_pluto(pluto, address, 'shopping_mall'))
            amenity['subway_station'] = int(searching_from_pluto(pluto, address, 'subway_station'))
            
            #property_features['TOTAL ASSESED VALUE'] = float(first_year_NOI(path, output_path, property_features, whrk_class_dict, neibor_features, projection_dict, closing_bldf_dict, holding_period, cap_rate_list))*int(property_features['RESI NSF'])/cap_rate_list[0]
            #print(property_features['TOTAL ASSESED VALUE'])
            # census
            census_dict = {}
            try:
                census_dict['population'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Population estimates, July 1, 2018, (V2018)'].values[0])
            except:
                census_dict['population'] = 0
                
            try:
                census_dict['female_population'] = int(float(census[census['ZIP'] == str(property_features['ZIP'])]['Female persons, percent'].values[0].split('%')[0])/100*census_dict['population'])
            except:
                census_dict['female_population'] = 0
                
            try:
                census_dict['male_population'] = census_dict['population'] - census_dict['female_population']
            except:
                census_dict['male_population'] = 0
                
            try:
                census_dict['per_capita_income'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Per capita income in past 12 months (in 2017 dollars), 2013-2017'].values[0])
            except:
                census_dict['per_capita_income'] = 0
            
            try:
                census_dict['median_income'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Median household income (in 2017 dollars), 2013-2017'].values[0])
            except:
                census_dict['median_income'] = 0
            
            try:
                census_dict['avg_income'] = census_dict['median_income']
            except:
                census_dict['avg_income'] = 0
                
            try:
                census_dict['persons_poverty'] = int(float(census[census['ZIP'] == str(property_features['ZIP'])]['Persons in poverty, percent'].values[0].split('%')[0])/100*census_dict['population'])
            except:
                census_dict['persons_poverty'] = 0
                
            try:
                census_dict['housing_units'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Housing units, July 1, 2018, (V2018)'].values[0])
            except:
                census_dict['housing_units'] = 0
                
            try:
                census_dict['owner_occupied_units'] = float(census[census['ZIP'] == str(property_features['ZIP'])]['Owner-occupied housing unit rate, 2013-2017'].values[0].split('%')[0])/100
            except:
                census_dict['owner_occupied_units'] = 0
                
            try:
                census_dict['median_values'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Median value of owner-occupied housing units, 2013-2017'].values[0])
            except:
                census_dict['median_values'] = 0
                
            try:
                census_dict['median_rent'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Median gross rent, 2013-2017'].values[0])
            except:
                census_dict['median_rent'] = 0
                
            try:
                census_dict['permits'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Building permits, 2018'].values[0])
            except:
                census_dict['permits'] = 0
                
            try:
                census_dict['households'] = int(census[census['ZIP'] == str(property_features['ZIP'])]['Households, 2013-2017'].values[0])
            except:
                census_dict['households'] = 0
                
            try:
                census_dict['persons_households'] = float(census[census['ZIP'] == str(property_features['ZIP'])]['Persons per household, 2013-2017'].values[0])
            except:
                census_dict['persons_households'] = 0
            
            # number of firms
            try:
                census_dict['persons_sp'] = float(census[census['ZIP'] == str(property_features['ZIP'])]['All firms, 2012'].values[0])
            except:
                census_dict['persons_sp'] = 1
                
            try:
                census_dict['employment'] = float(census[census['ZIP'] == str(property_features['ZIP'])]['High school graduate or higher, percent of persons age 25 years+, 2013-2017'].values[0].split('%')[0])/100
            except:
                census_dict['employment'] = 0.8
                
            try:
                property_features['FIPS'] = str(census[census['ZIP'] == str(property_features['ZIP'])]['FIPS Code'].values[0])
            except:
                property_features['FIPS'] = '0'
                
            property_features['TOTAL ASSESED VALUE'] = float(first_year_NOI(path, property_features, projection_dict))/cap_rate_list[0]
            print(property_features['TOTAL ASSESED VALUE'])
            
            if property_features['RESI NSF'] < 500 or property_features['TOTAL ASSESED VALUE'] < 2000000:
                cap_rate_list = list(np.asarray(cap_rate_list)*0.06849453546285089/cap_rate_list[0])
            
            #print(111)
            # sensitive return
            exit_sensitive = []
            for ii in range(6):
                exit_sensitive.append(exit_sensitive_resi(path, property_features, whrk_class_dict, neibor_features, projection_dict, closing_bldf_dict, ii+2, cap_rate_list))
            
            sale_price_sensitive = {} #sale_price_sensitive_resi
            asking_price_tmp = property_features['TOTAL ASSESED VALUE']
            for ii in range(6):
                sale_price_sensitive[asking_price_tmp] = sale_price_sensitive_resi(path, copy.deepcopy(property_features), whrk_class_dict, neibor_features, 
                                    projection_dict, closing_bldf_dict, holding_period, asking_price_tmp, cap_rate_list)
                asking_price_tmp = (1-0.05) * asking_price_tmp
            
            generate_report_resi(path, output_path, property_features, whrk_class_dict, neibor_features, projection_dict, closing_bldf_dict, holding_period,
                                 exit_sensitive, sale_price_sensitive, amenity, census_dict, cap_rate_list, amenity_weights, pic_path)
            
            generate_report_resi(path, 'C:/Users/jorda/Box/User Interface/Financial Analysis/NYC/', property_features, whrk_class_dict, neibor_features, projection_dict, closing_bldf_dict, holding_period,
                     exit_sensitive, sale_price_sensitive, amenity, census_dict, cap_rate_list, amenity_weights, pic_path)
   
            
        except Exception as e:
            traceback.print_exc()
        
    print('\rUnderwritting for NY：{0}{1}%'.format('▉'*int(10*i/len(rent_master_unique.index)),round((i/len(rent_master_unique.index)*100),2)), end='')
    
    nnn += 1
    
    try:
        if (nnn>=100):
            mypath = 'C:/Users/jorda/Box/User Interface/Financial Analysis/NYC/'
        
            f = []
            for (dirpath, dirnames, filenames) in walk(mypath):
                f.extend(filenames)
                break
            
            deal_summary = pd.read_csv('D:/NYC/Deal Summary NYC.csv',index_col=0)
            #deal_summary = pd.DataFrame(columns=['ADDRESS','ZIP CODE','CITY','STATE','BUILDING GSF','# UNITS','PROJECTED NOI','UNLEVERED IRR','LEVERED IRR','RETURN MULTIPLE','PROJECTED RENT PSF','CONDO'])
            
            i = 0
            
            for file in f:    
                workbook_ = load_workbook(mypath+file)
                sheetnames =workbook_.get_sheet_names() #获得表单名字
                sheet = workbook_.get_sheet_by_name(sheetnames[0])
                
                addr = sheet['D6'].value
                
                comment = np.nan
                if addr in list(deal_summary['ADDRESS'].values):
                    comment = deal_summary[deal_summary['ADDRESS'] == addr]['COMMENT'].values[0]
                
                if (sheet['V14'].value>-0.1):
                    deal_summary.loc[i,'ADDRESS'] = sheet['D6'].value
                    deal_summary.loc[i,'ZIP CODE'] = sheet['D7'].value
                    deal_summary.loc[i,'CITY'] = 'NYC'
                    deal_summary.loc[i,'CITY NAME'] = 'New York'
                    deal_summary.loc[i,'STATE'] = sheet['D9'].value
                    deal_summary.loc[i,'BUILDING GSF'] = sheet['G15'].value
                    deal_summary.loc[i,'# UNITS'] = sheet['G20'].value
                    deal_summary.loc[i,'PROJECTED NOI'] = sheet['I91'].value
                    deal_summary.loc[i,'UNLEVERED IRR '] = sheet['S14'].value
                    deal_summary.loc[i,'LEVERED IRR '] = sheet['V14'].value
                    deal_summary.loc[i,'RETURN MULTIPLE'] = sheet['S13'].value
                    deal_summary.loc[i,'PROJECTED RENT PSF'] = sheet['V29'].value
                    deal_summary.loc[i,'CONDO'] = searching_from_pluto(pluto, sheet['D6'].value, 'CONDO')
                    deal_summary.loc[i,'COMMENT'] = comment
                    
                    i += 1
                    
            deal_summary.to_csv('D:/NYC/Deal Summary NYC.csv')
            
            nnn = 0
            
            overall_summary = pd.DataFrame()
            cities_list = ['NYC','PHL','BOS','ABE','CAM','HFD','PIT','RDG','SYR']
            for city in cities_list:
                tmp = pd.read_csv('C:/Users/jorda/Box/User Interface/Deals/Deal Summary '+city+'.csv',index_col=0)
                overall_summary = pd.concat([overall_summary, tmp], axis=0)
            
            overall_summary.to_csv('C:/Users/jorda/Box/User Interface/Deal Summary.csv')
    
    except:
        nnn = 0
        pass
    
print(num)