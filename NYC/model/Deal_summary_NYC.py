# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 14:47:11 2019

@author: jorda
"""

from os import walk
import pandas as pd
import openpyxl
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.writer.excel import ExcelWriter

mypath = 'C:/Users/jorda/Box/User Interface/NYC/Analysis/'

f = []
for (dirpath, dirnames, filenames) in walk(mypath):
    f.extend(filenames)
    break

deal_summary = pd.read_csv('C:/Users/jorda/Box/User Interface/NYC/Deal Summary.csv')

i = 0

for file in f:    
    workbook_ = load_workbook(mypath+file)
    sheetnames =workbook_.get_sheet_names() #获得表单名字
    sheet = workbook_.get_sheet_by_name(sheetnames[0])
    
    if (sheet['V14'].value>-0.1):
        deal_summary.loc[i,'ADDRESS'] = sheet['D6'].value
        deal_summary.loc[i,'ZIP CODE'] = sheet['D7'].value
        deal_summary.loc[i,'CITY'] = sheet['D8'].value
        deal_summary.loc[i,'STATE'] = sheet['D9'].value
        deal_summary.loc[i,'BUILDING GSF'] = sheet['G15'].value
        deal_summary.loc[i,'# UNITS'] = sheet['G20'].value
        deal_summary.loc[i,'PROJECTED NOI'] = sheet['I91'].value
        deal_summary.loc[i,'UNLEVERED IRR '] = sheet['S14'].value
        deal_summary.loc[i,'LEVERED IRR '] = sheet['V14'].value
        deal_summary.loc[i,'RETURN MULTIPLE'] = sheet['S13'].value
        deal_summary.loc[i,'PROJECTED RENT PSF'] = sheet['V29'].value
        
        i += 1
        
deal_summary.to_csv('C:/Users/jorda/Box/User Interface/NYC/Deal Summary.csv')