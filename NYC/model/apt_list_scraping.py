# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 10:59:41 2019

@author: Quinntang
"""

import pandas as pd
from bs4 import BeautifulSoup
import urllib as ulb
import random
import time
from selenium import webdriver
from selenium.webdriver import ActionChains

def main(city='philadelphia',state='pa',start_page=1,end_page=2):
    
    def get_link_content(url):
        ##Decorate the request with header and proxy
        my_headers=["Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)"]
        headers = [('Host','https://www.apartmentlist.com/'),('Connection', 'keep-alive'),('Cache-Control', 'max-age=0'),
               ('User-Agent',random.choice(my_headers))]
        ##Please note the the size of proxy IPs may affect the performance,if the IP pools are small, may raise 403 Error (Server Forbidden)
        proxy_list = '138.197.222.35,36.89.65.253,105.19.59.192,109.110.73.106,176.9.75.42,200.89.178.63,138.68.161.14,188.226.141.61,139.59.109.156'.split(',')
        ##Add timer to sleep for 1 sec,in order to avoid getting blocked
        t = 1
        time.sleep(t)
        ##Decorate the url request with Header and Proxy IP
        proxy = random.choice(proxy_list)

        urlhandle = ulb.request.ProxyHandler({'http': proxy})
        opener = ulb.request.build_opener(urlhandle)
        opener.addheaders = headers
        ulb.request.install_opener(opener)
        response = ulb.request.Request(url)
        
        fp = ulb.request.urlopen(response)
        html = fp.read()
        fp.close
        content = BeautifulSoup(html,"lxml")
        return content
    
    def get_content(url):

        content = get_link_content(url)
        address = content.find('div',class_="css-16t5sqs eow5xa21").find('span').get_text().split(',')
        street = address[0]
        city = address[1]
        state = address[2].strip().split(' ')[0]
        zip_code = address[2].strip().split(' ')[1]
    
        listing_detail = []
        if content.find('div',class_="css-5yy5uv e1gq47vd2") == None:
            if len(content.find_all('div',class_="ListingCardUnitDetails__section__3Y9Bd")) == 1:
                apt_num = 'N/A'
                bedrooms = content.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[0].strip().split(' ')[1]
                bathrooms = content.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[1].strip()
                try:
                    sqt = content.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[2].strip()
                except:
                    sqt = 'N/A'
                price = content.find('div',class_="ListingCardUnitDetails__price__1So3E").get_text()
                listing_type = 'House'
                listing_detail.append([street,city,state,zip_code,apt_num,bathrooms,bedrooms,sqt,price,listing_type])
            else:
                for element in content.find_all('div',class_="ListingCardUnitDetails__section__3Y9Bd"):
                    apt_num = element.find('p',class_="ListingCardUnitDetails__unitTitle__3ULBF").get_text().split('·')[0].strip().split(' ')[1]
                    bedrooms = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[0].strip()
                    bathrooms = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[1].strip()
                    try:
                        sqt = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[2].strip().split(' ')[0]
                    except:
                        sqt = 'N/A'
                    price = element.find('div',class_="ListingCardUnitDetails__price__1So3E").get_text()
                    listing_type = 'Apartment'
                    listing_detail.append([street,city,state,zip_code,apt_num,bathrooms,bedrooms,sqt,price,listing_type])
            
        else:
        ##Initialize a browser object
        ## Please make sure to put chromedriver.exe and this python file together under the same folder path
        ## And change to your local path when testing
            try:
                chromedriver_path = 'C:/Users/Quinntang/Desktop/whiterock AI/chromedriver.exe'
                driver = webdriver.Chrome(executable_path=chromedriver_path)
                driver.get(url)
                ##Create ActionChains to realize the 'click' event in order to expand the folded listing info
                element = driver.find_element_by_css_selector(".e1gq47vd2")
                actions = ActionChains(driver)
                actions.move_to_element(element).click(element).perform()
                ActionChains(driver).move_to_element(element).click(element).perform()
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                driver.quit()
                for element in soup.find_all('div',class_="ListingCardUnitDetails__section__3Y9Bd"):
                    apt_num = element.find('p',class_="ListingCardUnitDetails__unitTitle__3ULBF").get_text().split('·')[0].strip().split(' ')[1]
                    bedrooms = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[0].strip()
                    bathrooms = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[1].strip()
                    try:
                        sqt = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[2].strip().split(' ')[0]
                    except:
                        sqt = 'N/A'
                    price = element.find('div',class_="ListingCardUnitDetails__price__1So3E").get_text()
                    listing_type = 'Apartment'
                    listing_detail.append([street,city,state,zip_code,apt_num,bathrooms,bedrooms,sqt,price,listing_type])
            except:
                for element in content.find_all('div',class_="ListingCardUnitDetails__section__3Y9Bd"):
                    apt_num = element.find('p',class_="ListingCardUnitDetails__unitTitle__3ULBF").get_text().split('·')[0].strip().split(' ')[1]
                    bedrooms = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[0].strip()
                    bathrooms = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[1].strip()
                    sqt = element.find('p',class_="ListingCardUnitDetails__unitSubTitle__3Hnu1").get_text().split('·')[2].strip().split(' ')[0]
                    price = element.find('div',class_="ListingCardUnitDetails__price__1So3E").get_text()
                    listing_type = 'Apartment'
                    listing_detail.append([street,city,state,zip_code,apt_num,bathrooms,bedrooms,sqt,price,listing_type])
                
        return listing_detail
    
    def get_df(content_list):
    ##Store all the listing info to a dataframe, if choose to save as Excel Spread Sheet, pass True
        df = pd.DataFrame()
        for i in range(len(content_list)-1):
            temp_df = pd.concat([pd.DataFrame(content_list[i]),pd.DataFrame(content_list[i+1])],axis=0, ignore_index=True)
            df = pd.concat([df,temp_df],axis=0, ignore_index=True)
        df.rename(columns={0:'ADDRESS',1:'CITY',2:'STATE',3:'ZIP',4:'APT #',5:'BATHROOMS',6:'BEDROOMS',
                       7:'SF',8:'RENT',9:'BUILDING TYPE'},inplace=True)
        return df
    
    ##test the function
    url_list = ['https://www.apartmentlist.com/{}/{}/page-{}'.format(state,city,str(i)) for i in range(start_page,end_page+1)]
    listing_link = []
    for url in url_list:
        content = get_link_content(url)
        for link in content.find_all('div',class_='ListingCard'):
            listing_link.append('https://www.apartmentlist.com'+ link.find('a')['href'])
    content_list = []
    for url in listing_link:
        content_list.append(get_content(url))
    
    df = get_df(content_list)
    df.drop_duplicates(inplace=True)
    df.reset_index(drop=True,inplace=True)
    
    return df