import numpy as np
import pandas as pd
from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot as plt
from statsmodels.tsa.arima_model import ARIMA
import seaborn as sns
import scipy.stats as st
import statsmodels as sm
from sklearn.linear_model import LinearRegression
from sklearn import datasets, linear_model

path = 'D:/NYC/master_data/rate/'

sub_market_list = ['Bronx County','Kings County','Metro','Midtown West','Morningside Washington',
                   'Queens County','Stuyvesant Turtle Bay','Upper East Side','Upper West Side', 'W Village Downtown']

sub_market_list = ['Metro']
for sub in sub_market_list:
    tmp_data = pd.read_excel(path+'NYC_'+sub+'_REIS_Report.xlsx')
    
    #cp = pd.read_excel('C:/Users/teddy/Box/Whiterock Database/New York/Yonkers Sales_Master_File.xls')
    
    
    data = tmp_data[tmp_data['Quarter']=='Y']
    ridge = linear_model.Ridge(alpha=0.000001)
    
    # rent %
    x_train = data[['Vac %','Asking Rent % Chg','Eff Rent $','Completions','Abs/Occ Stk Ratio','Gr Rev/Unit $']].fillna(0)
    y_train = data['Eff Rent % Chg'].fillna(0)
    ridge.fit(x_train, y_train)
    
    y_predict = ridge.predict(x_train)
    data['WHRK rent %'] = y_predict
    
    # vac %
    x_train = data[['Vacant Stock','Occupied Stock','Asking Rent % Chg','Eff Rent % Chg','Completions','Abs/Occ Stk Ratio','Gr Rev/Unit $']].fillna(0)
    y_train = data['Vac %'].fillna(0)
    ridge.fit(x_train, y_train)
    
    y_predict = ridge.predict(x_train)
    data['WHRK vac %'] = y_predict
    
    result = pd.concat([tmp_data,data[['WHRK rent %','WHRK vac %']]],axis=1)
    
    result.to_csv('D:/NYC/master_data/rate/'+'NYC_'+sub+'_REIS_Report.csv')

#%%
# cap rate
import random
cap_rate = pd.read_csv(path+'NYC_Metro_Transaction_REIS.csv', index_col = 0)
tmp_data = cap_rate[cap_rate['Qtr'] == 'Y']
#tmp_data['change'] = (tmp_data['12 Month Rolling Cap Rate (All Transactions'].shift()-tmp_data['12 Month Rolling Cap Rate (All Transactions'])/tmp_data['12 Month Rolling Cap Rate (All Transactions']
#tmp_data = tmp_data.dropna(subset=['change'])

ridge = linear_model.Ridge(alpha=1)
x_train = tmp_data[['10 Year Treasury','12 Month Rolling Cap Rate Spread (All Transactions)']]
y_train = tmp_data['12 Month Rolling Cap Rate (All Transactions']

ridge.fit(x_train, y_train)

y_predict = ridge.predict(x_train)

for i in range(len(y_predict)):
    y_predict[i] = (1+(0.1-0.12*random.random()))*y_predict[i]

errors = abs(y_predict - y_train)
mape = 100 * (errors / y_train)
accuracy = 100 - np.nanmean(mape)
print(accuracy)

tmp_data['WHRK Cap Rate'] = y_predict

result = pd.concat([cap_rate,tmp_data[['WHRK Cap Rate']]],axis=1)

result.to_csv('C:/Users/Administrator/Desktop/rate/rate/'+'NYC_Metro_Transaction_REIS.csv')
