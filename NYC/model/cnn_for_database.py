# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 23:54:57 2019

@author: Administrator
"""

import boto3
import pandas as pd
import urllib
import numpy as np
from sklearn import datasets, linear_model
from sklearn.linear_model import LinearRegression
import os
import glob
import random
from tensorflow import keras
import tensorflow as tf
from PIL import Image
import copy
import math
from sklearn.externals import joblib
#import prettytable as pt
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.writer.excel import ExcelWriter
from datetime import date
import datetime
import Rent_model_v2 as rent_model
import Sale_price_model_v2 as sales_model
#import Expense_Elec_model_v2 as elec_model
import Macroeconomic_model_v1 as macro_model
import Vacancy_model_v2 as vacancy_model
from Underwritting_generator import *
from PIL import Image, ImageEnhance
from IPython.display import Image as Im
from IPython.display import display

import warnings
warnings.filterwarnings("ignore")

BUCKET_NAME = 'gsinvest'
#KEY = 'NYC/External_Picture/picture_name.jpg'
s3_resource = boto3.resource('s3')
s3_client = boto3.client('s3')

path = 'D:/NYC/master_data/'
rent_master_unique = pd.read_csv(path+'NMA-001 Rent_Master_Unique [bylocation;addresses].csv', index_col = 0)

def searching_from_pluto(pluto, address, target):
    return pluto[pluto['ADDRESS'] == address][target].values[0]

class PicDownloader:
    def __init__(self):
        self.key  = "AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE"
        
    def download(self, url, name):
        urllib.request.urlretrieve(url.replace(" ", "%20"),"%s.png" % name)

    def gen_url(self, geom, fov=100, heading=0, pitch=30, size=(500, 500)):
        # fov控制镜头缩进，数值越小图片越大，最大为120
        # heading控制朝向，0为北，90为东，180为南
        # pitch控制向上的仰视的角度
        x, y = size
        lat, lng = geom
        return "https://maps.googleapis.com/maps/api/streetview?size=%sx%s&location=%s,%s&fov=%s&heading=%s&pitch=%s&key=AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE" % (x, y, lat, lng, fov, heading, pitch)

    def gen_url_by_string(self, address, fov=80, pitch=43, size=(400, 400)):
        x, y = size
        return "https://maps.googleapis.com/maps/api/streetview?size=%sx%s&location=%s&fov=%s&pitch=%s&key=AIzaSyBMsupEpnbssPowczxp3ow0QPPW01TE-fE" % (x, y, address, fov, pitch)

PD = PicDownloader()

def com_with_dis(img, material, criterion=100, dis=10):
    L, W = img.size
    length = int(680*L/W)
    img = img.resize((length,680))
    img_array = np.array(img)
    
    sampling = lambda: (random.randint(0,55), random.randint(0,39))
    i = 0
    score = []
    ii = 0
    while i < 100:
        sample_1 = sampling()
        sample_2 = sampling()
        if np.linalg.norm(np.array(sample_1)-np.array(sample_2)) >= dis:
            arr_1 = img_array[sample_1[0]*12:(sample_1[0]+1)*12, sample_1[1]*12:(sample_1[1]+1)*12, :].astype(np.int64)
            arr_2 = img_array[sample_2[0]*12:(sample_2[0]+1)*12, sample_2[1]*12:(sample_2[1]+1)*12, :].astype(np.int64)
            if arr_1.shape == (12,12,3) and arr_2.shape == (12,12,3):
                if np.linalg.norm(arr_1-np.array(material),axis=2).mean() <= criterion:
                    if np.linalg.norm(arr_2-np.array(material),axis=2).mean() <= criterion:
                        score.append(np.linalg.norm(arr_1.mean(axis=(0,1)) - arr_2.mean(axis=(0,1))))
                        i += 1
                        
        ii += 1
        
        if ii > 1000000 and i < 100:
            break;
    return np.array(score).mean()

def crop(img, left, right, top, bottom):
    x, y = img.size
    area = (int(x*left), int(y*top), int(x*right), int(y*bottom))
    return img.crop(area)

def transparency(img, material):    
    L, W = img.size
    length = int(300*L/W)
    img = img.resize((length,300))
    win = 0
    img_array = np.array(img)
    for i in range(300//9):
        array = img_array[i*9:(i+1)*9, :]
        array_dis = np.linalg.norm(array-np.array(material),axis=2)
        dummy_array_dis = np.where(array_dis<100,0,1)
        array_mean = array.mean(axis=0)
        mean_dis = np.linalg.norm(array_mean-np.array(material),axis=1)
        dummy_mean_dis = np.where(mean_dis<100,0,1)
        dis_count = dummy_array_dis * dummy_mean_dis[np.newaxis,:]
        win += dis_count.sum()
    return win/(300*length)

def multi_crop(img, material):
    res = {}
    for i in range(100,301,5):
        i /= 1000
        for j in range(100,301,5):
            j /= 1000
            cropped = crop(img,i,1-i,j,1-j)
            score = transparency(cropped, material)
            res.setdefault((i,j),score)
    return res

def compute(dic,method='mean'):
    scores = list(dic.values())
    if method == 'mean':
        return np.array(scores).mean()
    if method == 'median':
        return np.median(np.array(scores))
    raise ValueError('Indistinguishable method {}'.format(method))

def prediction(model, cropped_img, shape: tuple):
	"""
	Before prediction, the test image is recommended to be cropped. Use the function crop in part one
	params:
	cropped_image: raw image data, PIL.Image.Image object
	shape: tuple, (length, width)
	"""
	L, W = shape
	img = np.array(cropped_img.resize(shape))
	img = img/255
	img = img.reshape(1,L,W,3)
	pred = model.predict(img).argmax()
	if pred == 0:
		return 'Brick'
	if pred == 1:
		return 'Glass'
	if pred == 2:
		return 'Limestone'
    
def getSize(fileobject):
    fileobject.seek(0,2) # move the cursor to the end of the file
    size = fileobject.tell()
    return size
    
#o_path = 'C:/Users/Administrator/Desktop/data_need_clean/'
cnn_model = keras.models.load_model(path+'cnn/Model_July_2.h5')

with open(path+'cnn/features_from_brick.txt','r') as f:
    line = f.readline()
Brick = [float(x) for x in line.split()]

with open(path+'cnn/features_from_lime.txt','r') as f:
    line = f.readline()
Lime = [float(x) for x in line.split()]

#rent_master_unique['MATERIAL'] = np.nan
#rent_master_unique['TRANSPARENCY'] = np.nan
#rent_master_unique['DIRTINESS'] = np.nan

city = 'NYC'
pluto = pd.read_csv(path+'NPL-001 All_Properties [bylocation;address] PLUTO.csv')

for i in range(0,len(rent_master_unique.index)):
    print(i)
    if np.isnan(rent_master_unique.loc[i,'DIRTINESS']):
        try:
            address = rent_master_unique.loc[i,'ADDRESS']
            
            new_addr = '_'.join(address.split('/')) + ', ' + city
            
            if os.path.exists("D:/NYC/pics/Brick/"+new_addr+".png"):
                pic = Image.open("D:/NYC/pics/Brick/"+new_addr+".png")
                print('  fetch pic')
                material_label = 'Brick'
                print('  '+material_label)
                if os.path.getsize("D:/NYC/pics/Brick/"+new_addr+".png") > 5120:
                    res_1 = multi_crop(pic, Brick)
                    transparency_rate = compute(res_1,'mean')
                    print('  '+str(transparency_rate))
                    pic_1 = pic.convert("RGB")
                    #display(Im(filename=image))
                    contrast = ImageEnhance.Contrast(pic_1)
                    contrast = contrast.enhance(1.0)
                    clean_score = com_with_dis(contrast, Brick, criterion=80)
                    print('  '+str(clean_score))
                    
                    rent_master_unique.loc[i,'MATERIAL'] = material_label
                    rent_master_unique.loc[i,'TRANSPARENCY'] = transparency_rate
                    rent_master_unique.loc[i,'DIRTINESS'] = clean_score
            
            elif os.path.exists("D:/NYC/pics/Glass/"+new_addr+".png"):
                pic = Image.open("D:/NYC/pics/Glass/"+new_addr+".png")
                print('  fetch pic')
                material_label = 'Glass'
                print('  '+material_label)
                if os.path.getsize("D:/NYC/pics/Glass/"+new_addr+".png") > 5120:
                    transparency_rate = 1
                    clean_score = 0
                    
                    rent_master_unique.loc[i,'MATERIAL'] = material_label
                    rent_master_unique.loc[i,'TRANSPARENCY'] = transparency_rate
                    rent_master_unique.loc[i,'DIRTINESS'] = clean_score
                    
            elif os.path.exists("D:/NYC/pics/Limestone/"+new_addr+".png"):
                pic = Image.open("D:/NYC/pics/Limestone/"+new_addr+".png")
                print('  fetch pic')
                material_label = 'Limestone'
                print('  '+material_label)
                if os.path.getsize("D:/NYC/pics/Limestone/"+new_addr+".png") > 5120:
                    res_1 = multi_crop(pic, Brick)
                    transparency_rate = compute(res_1,'mean')
                    print('  '+str(transparency_rate))
                    pic_1 = pic.convert("RGB")
                    #display(Im(filename=image))
                    contrast = ImageEnhance.Contrast(pic_1)
                    contrast = contrast.enhance(1.0)
                    clean_score = com_with_dis(contrast, Brick, criterion=80)
                    print('  '+str(clean_score))
                    
                    rent_master_unique.loc[i,'MATERIAL'] = material_label
                    rent_master_unique.loc[i,'TRANSPARENCY'] = transparency_rate
                    rent_master_unique.loc[i,'DIRTINESS'] = clean_score
                    
            elif os.path.exists("D:/NYC/pics/Wood Panels/"+new_addr+".png"):
                pic = Image.open("D:/NYC/pics/Wood Panels/"+new_addr+".png")
                print('  fetch pic')
                material_label = 'Wood Panels'
                print('  '+material_label)
                if os.path.getsize("D:/NYC/pics/Wood Panels/"+new_addr+".png") > 5120:
                    res_1 = multi_crop(pic, Brick)
                    transparency_rate = compute(res_1,'mean')
                    print('  '+str(transparency_rate))
                    pic_1 = pic.convert("RGB")
                    #display(Im(filename=image))
                    contrast = ImageEnhance.Contrast(pic_1)
                    contrast = contrast.enhance(1.0)
                    clean_score = com_with_dis(contrast, Brick, criterion=80)
                    print('  '+str(clean_score))
                    
                    rent_master_unique.loc[i,'MATERIAL'] = material_label
                    rent_master_unique.loc[i,'TRANSPARENCY'] = transparency_rate
                    rent_master_unique.loc[i,'DIRTINESS'] = clean_score
                    
            elif os.path.exists("D:/NYC/pics/Other/"+new_addr+".png"):
                pic = Image.open("D:/NYC/pics/Other/"+new_addr+".png")
                print('  fetch pic')
                material_label = 'Other'
                print('  '+material_label)
                if os.path.getsize("D:/NYC/pics/Other/"+new_addr+".png") > 5120:
                    transparency_rate = 0
                    clean_score = 0
                    
                    rent_master_unique.loc[i,'MATERIAL'] = material_label
                    rent_master_unique.loc[i,'TRANSPARENCY'] = transparency_rate
                    rent_master_unique.loc[i,'DIRTINESS'] = clean_score
                    
            else:
                print('  scrap pic')
                new_addr_pic = '_'.join(address.split('/')) + ', ' + str(searching_from_pluto(pluto,address,'ZIP'))
                new_addr = '_'.join(address.split('/')) + ', ' + city
                url = PD.gen_url_by_string(new_addr_pic)
                saving_path = "D:/NYC/pics/"+new_addr
                PD.download(url, saving_path)
                
        except Exception as e:
            print(e)


rent_master = pd.read_csv(path+'NMA-001 Rent_Master [bylocation;addresses].csv', index_col = 0)

rent_master = pd.merge(rent_master,rent_master_unique[['ADDRESS','MATERIAL','TRANSPARENCY','DIRTINESS']],on='ADDRESS', how='left')
rent_master.to_csv(path+'NMA-001 Rent_Master [bylocation;addresses].csv')
rent_master_unique.to_csv(path+'NMA-001 Rent_Master_Unique [bylocation;addresses].csv')
